import core from './core'
export const apiServer = 's1.aamadeo.me'

const urls = {
	params: `https://${apiServer}/v2/users/params`,
	avatar: `https://${apiServer}/v2/users/avatar`,
	/*Village*/
	village:`https://${apiServer}/v2/villages`,
	villageTroops:`https://${apiServer}/v2/villages/troops`,
	moveWorker: `https://${apiServer}/v2/villages/moveWorker`,
	research: `https://${apiServer}/v2/villages/research`,
	upgradeOccupation: `https://${apiServer}/v2/villages/upgradeOccupation`,
	spawn: `https://${apiServer}/v2/villages/spawn`,
	spawnArmy: `https://${apiServer}/v2/villages/spawnArmy`,
	build: `https://${apiServer}/v2/villages/build`,
	upgradeBuilding: `https://${apiServer}/v2/villages/upgrade`,
	sell:  `https://${apiServer}/v2/villages/sell`,
	tech: `https://${apiServer}/v2/villages/tech`,
	/*Map*/
	getCells: `https://${apiServer}/v2/cells/`,
	/*Mission*/
	mission: `https://${apiServer}/v2/missions/`,
	reports: `https://${apiServer}/v2/missions/reports`,
	toggleLoop: `https://${apiServer}/v2/missions/toggleLoop`,
	abort: `https://${apiServer}/v2/missions/abort`,
	/*Tasks*/
	tasks:  `https://${apiServer}/v2/tasks`,
	/*Auth*/
	login: `https://${apiServer}/v2/users/login`,
	logout: `https://${apiServer}/v2/users/logout`,
	freeUser: `https://${apiServer}/v2/users/free`,
	users: `https://${apiServer}/v2/users/`,
	/*Diplomacy*/
	surrender: `https://${apiServer}/v2/diplomacy/surrender`,
	diplomacy: `https://${apiServer}/v2/diplomacy`,
	troopsCalling: `https://${apiServer}/v2/diplomacy/troopsCalling`
}

export const errorHandlers = new Map()

const get = url => fetch(url, { 
	credentials: 'include',
	cache: false,
	dataType: 'json',
}).then( r => r.json())//.then ( r => { console.log({r}) ; return r})

const dataOp = (method) => (url, data) => fetch(url, { 
	method,
	credentials: 'include',
	headers : {
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	},
	body: JSON.stringify(data)
}).then( r => r.json()).then ( r => { console.log({r}) ; return r})

const toFormData = (name, data) => {
	const formData = new FormData()
	formData.append(name, data)

	return formData
}

const sendFile = (url, file) => fetch(url, {
	method: 'POST',
	credentials: 'include',
	headers: { 'Accept': 'application/json' },
	body: toFormData('file', file)
}).then( r => r.json()).then ( r => { console.log({r}) ; return r})

const put = dataOp("PUT")
const post = dataOp("POST")

/*****************************************
 * Diplomacy
 */

export const surrender = terms => post(urls.surrender, terms)
export const troopClaim = (fph, subject) => post(urls.troopsCalling, { fph, subject })
export const updateTerms = terms => put(urls.surrender, terms)
export const loadDiplomacy = () => get(urls.diplomacy)

/*****************************************
 * Users
 */

export const login = (user, pass) => post(urls.login, { user, pass })
export const logout = () => post( urls.logout )
export const freeUserName = (name) => get(urls.freeUser + "?user=" + name)
export const createUser = (userInfo) => post(urls.users, userInfo)
export const loadRanking = () => get(urls.users)
export const loadUserInfo = user => get(urls.users + "/" + user + "/" )
export const updateAvatar = file => sendFile(urls.avatar, file)

/*****************************************
 * Village
 */

const setT0 = list => {
	const t0 = new Date().getTime()
	for ( let r in list ) {
		list[r].t0 = t0
	}
}

export const updateSummary = summary => {
	if ( ! summary ) return null

	if ( summary.resources !== null ) setT0(summary.resources)

	let total = 0

	for( let o in summary.occupations ){
		total += summary.occupations[o].workers
	}

	for( let o in summary.occupations ){
		summary.occupations[o].total = total
	}
	
	return summary
}



export const loadParams = () => get(urls.params)

export const loadVillage = () => {
	return get(urls.village).then( updateSummary )
}

export const loadVillageTroops = () => {
	return get(urls.villageTroops).then( updateSummary )
}

export const upgradeBuilding = item => {
	item.village = core.village

	return post( urls.upgradeBuilding, item ).then( updateSummary )
}

export const upgradeOccupation = ({occupation}) => {
	
	var data = { 
		occupation,
		slot: 12
	}
	
	return post( urls.upgradeOccupation , data ).then( updateSummary )
}

export const build = (job) => {
	return post( urls.build, job ).then( updateSummary )
}

export const spawn = data => {
	return post( urls.spawn, data ).then( updateSummary )
}

export const spawnArmy = data => {
	return post( urls.spawnArmy, data ).then( updateSummary )
}

export const moveWorker = (occupation, workers) => {
	var data = { occupation, workers }
	
	return put ( urls.moveWorker , data ).then( updateSummary )
}

export const research = ({ slot, name, icon }) => post( urls.research, {slot, technology: name, icon }).then( updateSummary )

export const sell = (sellSpec) => {
	return post(urls.sell, sellSpec).then( updateSummary )
}

/*****************************************
 * Map
 */
export const loadMap = (center) => {
	let url = urls.getCells
	
	if ( center ) url = `${url}?x=${center.x}&y=${center.y}`

	return get(url)
}

export const loadMarks = () => {
	let marks = localStorage.getItem("marks")
	marks = marks ? JSON.parse(marks) : {}

	return new Promise( (res, rej) => res(marks) )
}

export const markLocation = location => {
	let marks = localStorage.getItem("marks")
	marks = marks ? JSON.parse(marks) : {}
	
	marks[core.p2Str(location)] = location
	
	localStorage.setItem("marks", JSON.stringify(marks))

	return  new Promise( (res, rej) => res(location) )
}

export const unmarkLocation = location => {
	let marks = localStorage.getItem("marks")
	marks = marks ? JSON.parse(marks) : {}
	
	delete marks[core.p2Str(location)]
	
	localStorage.setItem("marks", JSON.stringify(marks))

	return new Promise( (res, rej) => res(location) )
}

/*****************************************
 * Mission
 */
export const startMission = data => {
	var empty = true
	
	for (let troop in data.troops) {
		if (data.troops[troop].count === 0) continue
		empty = false
		break
	}
	
	if (empty) {
		return new Promise( (a,b) => undefined)
	}
	
	//console.log("StartMission.data: ", data)
	
	return post(urls.mission, data)
}

export const toggleLoop = missionId => put( urls.toggleLoop, { missionId } )
export const abort = missionId => put( urls.abort, { missionId } )

export const loadMissions = () => get(urls.mission).then(
	missions => {
		const t0 = new Date().getTime() /1000
		return missions.map( mission => Object.assign(mission, {t0}) )
	}
)

export const loadReports = () => get(urls.reports).then( reports => reports.map( r => Object.assign(r, { id: r.mission })))

export const loadTasks = () => {
	return get(urls.tasks).then(
		slots => slots.map( 
			slot => 
				slot !== null && 
				slot.map(task => Object.assign(task, {t0: new Date().getTime()}))
		)
	)
}

