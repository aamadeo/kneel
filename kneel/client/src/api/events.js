import * as actions from '../components/ActionTypes.js'
import * as api from './api.js'

const updateMapSectionHandler = (socket, dispatcher) => {
    socket.on('update-map-section', e => dispatcher.updateSection(e))
}

const updateVillageHandler = (socket, dispatcher) => {
    socket.on('update-village', e => {
        dispatcher.updateVillage(api.updateSummary(e.summary))
    })

    socket.on('update-resources', e => {
        dispatcher.updateResources(api.updateSummary(e.resources))
    })
}

const reportEventHandler = (socket, dispatcher) => {
    socket.on('new-report', e => {
        e.report.id = e.report.mission
        dispatcher.addReport(e.report)
    })

    socket.on('update-report', e => {
        e.report.id = e.report.mission
        dispatcher.addReport(e.report)
    })
}

const missionEventHandler = (socket, dispatcher) => {
    socket.on('new-mission', e => {
        const t0 = new Date().getTime() / 1000
        Object.assign(e.mission, {t0})

        dispatcher.addMission(e.mission)
    })

    socket.on('update-mission', e => {
        const t0 = new Date().getTime() / 1000
        Object.assign(e.mission, {t0})

        dispatcher.updateMission(e.mission)
    })

    socket.on('end-mission', e => {
        dispatcher.deleteMission(e.mission)
    })
}

const taskEventHandler = (socket, dispatcher) => {
    socket.on('task-start', e => {
        e.task.t0 = new Date().getTime()
        dispatcher.addTask(e.task)
    })

    socket.on('task-end', e => {
        dispatcher.deleteTask(e.task)
    })
}

const diplomacyEventHandler = (socket, dispatcher) => {
    socket.on('allegiance-request', e => {
        console.log('Allegiance Request by event', e.term)

        if ( e.term.isKing ){
            dispatcher.addSubjectRequest(e)
        } else {
            dispatcher.surrenderRequest(e)
        }
    })

    socket.on('allegiance-update', e => {
        console.log('Allegiance Update by event', e)

        if ( e.term.isKing ){
            dispatcher.updateSubjectRequest(e.term)
        } else {
            dispatcher.updateSurrenderRequest(e.term)
        }
    })

    socket.on('allegiance-accepted', e => {
        console.log('Allegiance Accepted by event', e)

        if ( e.term.isKing ){
            dispatcher.subjectAccepted(e.term.subject)
        } else {
            dispatcher.acceptSurrenderRequest(e.term)
        }
    })

    socket.on('allegiance-denied', e => {
        console.log('Allegiance Denied by event', e)

        if ( e.term.isKing ){
            dispatcher.subjectRejected(e.term.subject)
        } else {
            dispatcher.denySurrenderRequest(e.term)
        }
    })
}

export const subscribe = (socket, subscription) => socket.emit('subscribe', subscription)
export const unsubscribe = (socket, subscription) => socket.emit('unsubscribe', subscription)
export const removeEvent = (socket, eventId) => socket.emit('remove-event', eventId)

export const init = (socket, dispatch) => {
    const dispatcher = {}

    for ( let action in actions ){
        dispatcher[action] = object => dispatch(actions[action](object))
    }

    updateMapSectionHandler(socket, dispatcher)
    updateVillageHandler(socket, dispatcher)
    reportEventHandler(socket, dispatcher)
    taskEventHandler(socket, dispatcher)
    missionEventHandler(socket, dispatcher)
    diplomacyEventHandler(socket, dispatcher)
}
