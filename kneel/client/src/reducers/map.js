import { combineReducers } from 'redux'
import core from '../api/core'

const location = (state = {x: 0, y:0}, action) => {
	if ( action.type === "UPDATE_LOCATION" ){
		return Object.assign({}, action.location)
	} else if ( action.type === "LOGIN" ){
		return Object.assign({}, action.userInfo.location)
	}

	return state
}

const withinSection = (center, cell, radius ) => Math.abs( cell.x - center.x) <= radius &&
												 Math.abs( cell.y - center.y) <= radius

const centerOfSection = ({x,y}, radius) => ({
	x: x * 2 * radius + (x !== 0 ? x / Math.abs(x) : 0),
	y: y * 2 * radius + (y !== 0 ? y / Math.abs(y) : 0),
})

const cells = (state = [], action) => {
	if ( action.type === "UPDATE_MAP" ){
		return [...action.cells]
	}

	if ( action.type === 'UPDATE_SECTION' ){
		const setCells = new Set()
		const center = centerOfSection(action.point, action.radius)

		const filteredCells = state.filter( cell => {
			if ( withinSection(center, cell, action.radius) ) return false
			setCells.add(`${cell.x}:${cell.y}`)

			return true
		})

		const newState = [...filteredCells]
		
		action.cells.forEach( cell => {
			if (setCells.has(`${cell.x}:${cell.y}`)) return
			newState.push(cell)
		})

		return newState
	}

	return state
}

const marks = ( state = {}, action) => {
	if( action.type === "LOAD_MARKS") {
		return Object.assign({}, action.marks)
	} else if (action.type === "ADD_MARK"){
		return Object.assign({}, state, { [core.p2Str(action.mark)] : action.mark })
	} else if (action.type === "REMOVE_MARK"){
		//egghead example
	} else if (action.type === "CLEAR_MARKS"){
		return []
	}

	return state
}

export default combineReducers({
	cells,
	location,
	marks
})