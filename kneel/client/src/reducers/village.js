import { combineReducers } from 'redux'

const nullResource = {
	r0: 0,
	t0: new Date().getTime(),
	capacity: 0,
	rps: 0
}

const initialResources = {
	food: nullResource,
	iron: nullResource,
	wood: nullResource,
	stone: nullResource
}

const nullOccupation = { workers: 0, level: 0 }
const initialOccupations = {
	food: nullOccupation,
	iron: nullOccupation,
	wood: nullOccupation,
	stone: nullOccupation,
	builders: nullOccupation,
	unoccupied: nullOccupation
}

const shortage = (state = {}, action) => {
	if ( action.type === "SHOW_SHORTAGE" ){
		let shortage = {}
		
		for( let r in action.shortage ){
			if (action.shortage[r] < 0 ) shortage[r] = action.shortage[r]
		}

		return shortage
	} else {
		return {}
	}
}

const buildings = (state = [], action ) => {
	
	if ( action.type === "UPDATE_BUILDINGS" ){
		return [...action.buildings]
	} else if ( action.type === "UPDATE_VILLAGE"){
		return [...action.village.buildings]
	} else if ( action.type === "LOGOUT" ){
		return []
	}

	return state
}

const updater = {
	resources: (original, updated) => {
		const newResources = Object.assign({}, original)

		for( let r in original ){
			newResources[r] = Object.assign({}, original[r], updated[r])
		}

		return newResources
	},

	occupations: (original, updated) => {
		const newOccupations = Object.assign({}, original)

		for( let o in original ){
			newOccupations[o] = Object.assign({}, original[o], updated[o])
		}

		return newOccupations
	},

	troops: (original, updated) => Object.assign({}, original, updated)
}

const itemUpdateReducer = (initialState, item) => (state = initialState, action) => {
	let actionType = "UPDATE_" + item.toUpperCase()

	if ( action.type === actionType ) {
		return updater[item](state, action[item])
	} else if( action.type === "UPDATE_VILLAGE" ){	
		return Object.assign({}, state, action.village[item])
	} else if( action.type === "LOGOUT") {
		return Object.assign({}, initialState)
	} else {
		return state
	}
}

const resources = itemUpdateReducer(initialResources, "resources")
const occupations = itemUpdateReducer(initialOccupations, "occupations")
const troops = itemUpdateReducer({}, "troops")

export default combineReducers({
	shortage,
	resources, 
	occupations, 
	troops, 
	buildings
})