import { combineReducers } from 'redux'

const initialTab = { view: "village", changed: false }
const loggedOut = { loggedIn: false }

const builders = (state = 1, action) => {
	if ( action.type === "UPDATE_BUILDERS" ){
		return action.builders
	}
	return state
}

const lastAction = (state = "no-action", action ) => action.type
const login = (state = loggedOut, action ) => {
	if ( action.type === "LOGIN" ){
		delete action.userInfo.authenticated
		delete action.userInfo.token
		delete action.userInfo.location
		
		const loginState = Object.assign({ loggedIn: true }, action.userInfo)
		return loginState
	} else if ( action.type === "LOGOUT" ) {
		return loggedOut
	} else if (action.type === "LOGIN_ERROR"){
		return { loggedIn: false, error: true }
	}

	return state
}

const tab = (state = initialTab, action) => {
	if ( action.type === "CHANGE_TAB" ){
		return { view: action.view, changed: true, lastChanged: new Date().getTime() }
	} else if ( action.type === "PREPARE_MISSION" ) {
		return { view: "mission", changed: true, lastChanged: new Date().getTime() }
	} else if( action.type === "LOGOUT" ) {
		return { view: "village", changed: true, lastChanged: new Date().getTime() }
	} else {
		return Object.assign({}, state, { changed: false } )
	}
}

const configuration = ( state = {}, action ) => {

	if ( action.type === "PARAMS_RECEIVED" ){
		const { technologies, userTech } = action.params
		let newState = Object.assign({}, action.params )
		delete newState.userTech
		delete newState.technologies
		newState.tech = {
			available: userTech,
			all: technologies
		}

		return newState
	}

	return state
}

const loaded = ( state = {}, action ) => {
	let newState = Object.assign( {}, state )

	if ( action.type === "PARAMS_RECEIVED" ){
		 newState = Object.assign(newState, {params: true} )
	}

	if ( action.type === "UPDATE_VILLAGE" ){
		newState = Object.assign(newState, { village: true} )
	}

	if ( action.type === "UPDATE_DIPLOMACY" ){
		 newState = Object.assign(newState, {diplomacy: true} )
	}

	if ( action.type === "UPDATE_USER_INFO" ){
		 newState = Object.assign(newState, {userInfo: true} )
	}

	if ( action.type === "LOGOUT" ){
		return {}
	}

	return newState
}

const socket = ( state = {}, action ) => {
	if ( action.type === 'SET_SOCKET' ) return action.socket

	return state
}

export default combineReducers({
	socket,
	configuration,
	loaded,
	login,
	lastAction,
	builders,
	tab
})