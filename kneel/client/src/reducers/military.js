import { combineReducers } from 'redux'

const arrayUpdateReducer = type => (state = [], action ) => {

	const item = type.replace(/s$/,"")
	const addActionType = 'ADD_' + item.toUpperCase()
	const updateListActionType = 'UPDATE_' + type.toUpperCase()
	const updateItemActionType = 'UPDATE_' + item.toUpperCase()
	const deleteItemActionType = 'DELETE_' + item.toUpperCase()

	if ( action.type === updateListActionType ){
		return [...action[type]]
	} else if ( action.type === addActionType ){
		return [ action[item], ...state]
	} else if ( action.type === updateItemActionType ){
		return state.map( element => element.id !== action[item].id ? element : action[item])
	} else if ( action.type === deleteItemActionType ){
		return state.filter( element => element.id !== action[item].id )
	}

	return state
}

const missions = arrayUpdateReducer("missions")
const reports = arrayUpdateReducer("reports")

const emptyMissionConfig = { 
	objective: {
		action: "",
		cell: { x: "", y: "", name: "", owner: "" }
	}
}

const mission = (state = emptyMissionConfig, action ) => {
	if ( action.type === 'PREPARE_MISSION' ){
		return { objective: action.objective }
	} else if ( action.type === 'CLEAR_MISSION_CONFIG' ){
		return emptyMissionConfig
	}

	return state
}

export default combineReducers({
	mission,
	missions,
	reports
})