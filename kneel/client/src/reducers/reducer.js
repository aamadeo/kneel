import { combineReducers } from 'redux'
import village from './village'
import map from'./map'
import military from './military'
import control from './control'

const arrayUpdateReducer = type => (state = [], action ) => {

	const item = type.replace(/s$/,"")
	const addActionType = 'ADD_' + item.toUpperCase()
	const updateListActionType = 'UPDATE_' + type.toUpperCase()
	const updateItemActionType = 'UPDATE_' + item.toUpperCase()
	const deleteItemActionType = 'DELETE_' + item.toUpperCase()

	if ( action.type === updateListActionType ){
		return [...action[type]]
	} else if ( action.type === addActionType ){
		return [ action[item], ...state]
	} else if ( action.type === updateItemActionType ){
		return state.map( element => element.id !== action[item].id ? element : action[item])
	} else if ( action.type === deleteItemActionType ){
		console.log({ [item] : action[item] })
		return state.filter( element => element.id !== action[item].id )
	}

	return state
}

const tasks = (state = [], action ) => {

	if ( action.type === 'UPDATE_TASKS' ){

		return [...action.tasks]
	} else if ( action.type === 'ADD_TASK' ){
		const slot = action.task.params.slot
		const prevList = state[slot] || []
		const slotTasks = [...prevList, action.task]
		
		action.task.index = prevList.length

		return state.map( (taskList, s) => s === slot ? slotTasks : taskList )
	} else if ( action.type === 'DELETE_TASK' ){
		const { slot, id } = action.task
		const prevList = state[slot]

		if ( ! prevList ) return state

		const slotTasks = prevList.filter( t => t.id !== id )

		return state.map( (taskList, s) => s === slot ? slotTasks : taskList )
	}

	return state
}



const ranking = arrayUpdateReducer("ranking")
const userInfo = ( state = {}, action ) =>{
	if ( action.type === "UPDATE_USER_INFO" ){
		return Object.assign({}, action.userInfo)
	}

	return state
}

export const surrenderRequest = term => ({ type: 'SURRENDER_REQUEST', term })
export const acceptedSurrenderRequest = term => ({ type: 'ACCEPTED_SURRENDER_REQUEST', term })
export const rejectedSurrenderRequest = term => ({ type: 'REJECTED_SURRENDER_REQUEST', term })

const diplomacy = (state = {}, action ) => {
	if ( action.type === "UPDATE_DIPLOMACY" ){
		return Object.assign({}, action.diplomacy)

	} else if (action.type === "SUBJECT_ACCEPTED" ){
		const terms = state.terms.map( 
			t => t.subject === action.subject ? Object.assign({}, t, { status: 'accepted'}) : t
		)

		return Object.assign({}, state, { terms })
	} else if ( action.type === "SUBJECT_REJECTED" ){
		const allies = state.allies.filter(s => s !== action.subject)
		const terms = state.terms.filter(s => s.subject !== action.subject)

		return Object.assign({}, state, { terms, allies })
	} else if ( action.type === "ADD_SUBJECT_REQUEST" ){
		const terms = [...state.terms, action.term]

		return Object.assign({}, state, { terms })
	} else if ( action.type === "UPDATE_SUBJECT_REQUEST" ){
		const terms = state.terms.map( t => t.subject === action.term.subject ? action.term : t )

		return Object.assign({}, state, { terms })
	} else if ( action.type === "SURRENDER_REQUEST" ){
		return {
			user: action.term.subject,
			terms: [ action.term ],
		}
	} else if ( action.type === "ACCEPTED_SURRENDER_REQUEST" ){
		const term = Object({}, state.terms[0], { status: 'accepted' })

		return Object.assign(state, { terms: [term] })
	} else if ( action.type === "REJECTED_SURRENDER_REQUEST" ){
		return Object.assign(state, { terms: [] })
	}

	if ( action.type === "LOGOUT" ){
		return {}
	}

	return state
}

const main = combineReducers({
	village,
	map,
	military,
	control,
	tasks,
	ranking,
	userInfo,
	diplomacy
})

export default (state, action ) => {
	 let newState = main(state, action)
	 console.log({ before: state, action, after: newState })
	 return newState
 }