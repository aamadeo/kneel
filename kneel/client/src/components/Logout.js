import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import auth from './Auth'
import { logout } from './ActionTypes'

export default withRouter( connect()( React.createClass({
  componentDidMount() {
    auth.logout(this.redirect)
  },
  redirect(){
    console.log("Redirecting")
    this.props.dispatch( logout )
    this.props.router.push("/")
  },
  render() {
    console.log("Haupei")
    return <p>You are now logged out</p>
  }
})))