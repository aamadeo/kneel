const itemAction = actionType => item => data => {
	const action = { type: actionType + '_' + item.toUpperCase()}
	action[item] = data

	return action
}

const updateItem = itemAction('UPDATE')
const addItem = itemAction('ADD')
const deleteItem = itemAction('DELETE')

export const setSocket = socket => ({ type: 'SET_SOCKET', socket })
export const prepareMission = (objective) => ({ type: 'PREPARE_MISSION',  objective })
export const clearMissionConfig = () => ({ type: 'CLEAR_MISSION_CONFIG' })

export const addSubjectRequest = term => ({ type: 'ADD_SUBJECT_REQUEST', term })
export const updateSubjectRequest = term => ({ type: 'UPDATE_SUBJECT_REQUEST', term })
export const subjectRejected = subject => ({ type: 'SUBJECT_REJECTED', subject })
export const subjectAccepted = subject => ({ type: 'SUBJECT_ACCEPTED', subject })

export const surrenderRequest = term => ({ type: 'SURRENDER_REQUEST', term })
export const updateSurrenderRequest = term => ({ type: 'UPDATE_SURRENDER_REQUEST', term })
export const acceptedSurrenderRequest = term => ({ type: 'ACCEPTED_SURRENDER_REQUEST', term })
export const rejectedSurrenderRequest = term => ({ type: 'REJECTED_SURRENDER_REQUEST', term })

export const updateDiplomacy = updateItem('diplomacy')

export const updateResources = updateItem('resources')
export const updateOccupations = updateItem('occupations')
export const updateTroops = updateItem('troops')

export const updateMissions = updateItem('missions')
export const addMission = addItem('mission')
export const updateMission = updateItem('mission')
export const deleteMission = deleteItem('mission')

export const updateReports = updateItem('reports')
export const addReport = addItem('report')

export const updateTasks = updateItem('tasks')
export const addTask = addItem('task')
export const deleteTask = deleteItem('task')

export const updateBuildings = updateItem('buildings')
export const updateMap = cells => ({ type: 'UPDATE_MAP', cells })
export const updateSection = update => Object.assign({ type: 'UPDATE_SECTION' }, update)
export const updateLocation = location => ({ type: 'UPDATE_LOCATION', location })
export const mark = mark => ({ type: 'ADD_MARK', mark })
export const unmark = mark => ({ type: 'REMOVE_MARK', mark })
export const loadMarks = marks => ({ type: 'LOAD_MARKS', marks })


export const lacks = shortage => ({ type: 'SHOW_SHORTAGE', shortage })

export const changeTab = (key) => ({ type : 'CHANGE_TAB', view: key})

export const updateVillage = (village) => ({ type : 'UPDATE_VILLAGE', village })
export const paramsReceived = (params) => ({ type : 'PARAMS_RECEIVED', params })

export const requestLogin = { type : 'REQUEST_LOGIN' }
export const loginError = { type : 'LOGIN_ERROR' }
export const loginSuccess = userInfo => ({ type : 'LOGIN', userInfo })
export const logout = { type: 'LOGOUT' }
export const updateRanking = ranking => ({ type: 'UPDATE_RANKING', ranking })
export const updateUserInfo = userInfo => ({ type: 'UPDATE_USER_INFO', userInfo })
export const updateBuilders = builders => ({ type: 'UPDATE_BUILDERS', builders })