import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { PageHeader, Col, Table, Image } from 'react-bootstrap'

const mapStateToProps = state => ({ ranking: state.ranking })

export default withRouter( connect( mapStateToProps )(React.createClass({
	selectUser(e){
		console.log(e.target.id)
		this.props.router.push("/user/" + e.target.id)
	},
	render(){
		let position = 1

		const ranking = this.props.ranking.length > 0  ? this.props.ranking.map( u => {
			const { terms } = u
			let allegianceTo = null
			let subjects = null

			if ( terms[0] !== undefined ){
				if ( terms[0].king === u.user ){
					subjects = terms.length
				} else {
					allegianceTo = terms[0].king
				}
			}

			console.log({ user: u })

			const avatar = u.avatar ? <Image src={'/img/upload/'+u.avatar} className='ranking-avatar' thumbnail /> : ""

			return (<tr key={u.user}>
				<td>{ position++ }</td>
				<td style={{ fontSize: '2.5vw' }}>
					<a href="#" id={u.user} onClick={this.selectUser}>
					{avatar}
					<br />
					{u.user}
					</a>
				</td>
				<td>{subjects}</td>
				<td>{allegianceTo}</td>
				<td>{u.points}</td>
			</tr>)

		}) : "ranking"

		return ( 
			<div className="view">
				<PageHeader>Top Kings</PageHeader>
				<Col xs={12}>
					<Table striped bordered responsive hover>
						<thead  style={{ fontSize: '2.5vw' }}>
							<tr>
							<th style={{ width: "10%" }}>#</th>
							<th style={{ width: "30%" }}> User </th>
							<th style={{ width: "10%" }}> Subjects </th>
							<th style={{ width: "30%" }}> King </th>
							<th style={{ width: "10%" }}> Score </th>
							</tr>
						</thead>
						<tbody>
							{ranking}
						</tbody>
					</Table>
				</Col>
			</div>
		)
		
	}
})))