import React from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { Alert, Row, Col, Form, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

import auth from './Auth'

export default  withRouter( connect( ({ control: { login } }) => ({ login }) )(React.createClass({
	getInitialState() {
		return {
			 user: "",
			 password: ""
		}
	},
	
	onPasswdChange(e){
		this.setState({ password: e.target.value })
	},

	onUserChange(e){
		this.setState({ user: e.target.value })
	},

	submit(event) {
		auth.login(this.props.dispatch, this.state.user, this.state.password)
		event.preventDefault()
	},

	render() {
		const validationState = this.props.login.error ? "error" : undefined

		return (
			<Form horizontal onSubmit={this.submit} >
			   <FormGroup><Col sm={5} smOffset={3}><h2>Log In</h2></Col></FormGroup>
				{ this.props.login.error ? <FormGroup><Col sm={5} smOffset={2}><Alert bsStyle="danger">The log in information is incorrect.</Alert></Col></FormGroup> : ""}
				<Row>
					<FormGroup validationState={validationState}>
						<Col componentClass={ControlLabel} sm={2} smOffset={2}>
							Username :
						</Col>
						<Col componentClass={ControlLabel} sm={3}>
							<FormControl value={this.state.user} onChange={this.onUserChange} placeholder="JonSnow"/>
						</Col>
					</FormGroup>
					{' '}
					<FormGroup validationState={validationState}>
						<Col componentClass={ControlLabel} sm={2} smOffset={2}>
							Password
						</Col>
						<Col componentClass={ControlLabel} sm={3}>
							<FormControl	type="password" 
												value={this.state.password}
												onChange={this.onPasswdChange}
												placeholder="IKnowNothing"
							/>
						</Col>
					</FormGroup>
					<FormGroup>
						<Col componentClass={ControlLabel} sm={2}  smOffset={4}>
							<Button bsSize="large" bsStyle="info" block type="submit">Log In</Button>
						</Col>
					</FormGroup>
				</Row>
			</Form>

		)
	}
})))