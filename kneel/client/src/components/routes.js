import React from 'react'
import { Route } from 'react-router'
import { Col, Alert } from 'react-bootstrap'
import App from './App'
import User from './User'
import Profile from './account/Profile'

import Login from './Login'
import Register from './Register'
import Dashboard from './Dashboard'

const NotFound = React.createClass({
  render(){
    return (
      <Col xs={7} xsOffset={1}>
        <Alert bsStyle="danger">
          Who you trying to get crazy with, ése?
          <br/>
          Don't you know : <b>{this.props.location.pathname}</b> doesn't exist.
        </Alert>
      </Col>
    )
  }
})

const requireAuth = store => (nextState, replace) => {
  const { loggedIn } = store.getState().control.login

  if ( ! loggedIn ) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}

module.exports = (store) => {
  return (
	  <Route path="/" component={App} >
      <Route path="/login" component={Login} />
		  <Route path="/dashboard" onEnter={requireAuth(store)} component={Dashboard} />
      <Route path="/user/:user" onEnter={requireAuth(store)} component={User} />
      <Route path="/profile" onEnter={requireAuth(store)} component={Profile} />
      <Route path="/register" component={Register} />
      <Route path="/*" component={NotFound} />
	  </Route>
  )
}