import React from 'react'
import { connect } from 'react-redux'
import { PageHeader, Popover, OverlayTrigger, Alert, Image } from 'react-bootstrap'
import VillageActions from '../map/VillageActions'

import * as api from '../../api/api'
import * as actions from '../ActionTypes'

const mapStateToProps =  ({ 
	userInfo,
	control: { 
		loaded, login: { user, avatar } 
	}
}) => ({ userInfo, loaded, user, avatar })

export default connect (mapStateToProps)( React.createClass ({
	componentWillMount(){
		const dispatch = this.props.dispatch

		api.loadUserInfo( this.props.user ).then( userInfo => {
			dispatch( actions.updateUserInfo(userInfo) )
		})
	},

	render(){
		const { loaded, userInfo, avatar } = this.props

		console.log("Render")

		if ( ! loaded.userInfo ) return <Alert bsStyle="info">Loading...</Alert>

		const { user, points, villages } = userInfo
		const ally = user === userInfo.user

		const villageList = villages.map( v => {
			const menu = (
				<Popover id={v.id} title={v.name}>
					<VillageActions cell={v} ally={ally} goto={true}/>
				</Popover>
			)

			v.owner = v.user
			v.type = "village"

			return (
				<OverlayTrigger trigger="focus" placement="right" overlay={menu} key={v.id}>
					<li className="k-link">
						<a href="#" tabIndex="0">
							<span>{v.name} ({v.x},{v.y})</span>
						</a>
					</li>
				</OverlayTrigger>
			)
		})

		const {dispatch} = this.props
		const onChangeAvatar = e => {
			console.log("Change Avatar", e.target.files[0])
			
			api.updateAvatar(e.target.files[0]).then( response => {
				response = JSON.parse(response)
				dispatch( actions.updateUserInfo(Object.assign({}, userInfo, response)) )
			})
		}

		console.log("Avatar: ", avatar)
		const img = `/img/upload/${avatar}?${new Date().getTime()}`
		
		return (
			<div className="view">
				<PageHeader>
					{user} <br />
					<Image src={img} className='profile-avatar' thumbnail />
				</PageHeader>
				Change avatar: <input type='file' ref='avatar' onChange={onChangeAvatar} />
				<h3>Points: {points}</h3>
				<h3>Biggest Loot</h3> 12345r <a href='#'> report </a>
				<h3>Biggest Kill</h3> 12345fph <a href='#'> report </a>
				<h3>Villages</h3>
				<ul>
					{villageList}
				</ul>
			</div>
		)
	}
}))