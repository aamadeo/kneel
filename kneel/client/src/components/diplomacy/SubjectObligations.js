import React, { Component } from 'react'


export default class extends Component {
    render(){
        console.log(this.props)

        const { brnResources: food, whResources: materials, forceAvailable } = this.props

        return <div>
            materials: {parseInt(materials*10, 10) /10 || 0} <br/>
            food: { parseInt(food*10, 10) / 10 || 0} <br/>
            Troops: {forceAvailable || 0} fph
        </div>
    }
}