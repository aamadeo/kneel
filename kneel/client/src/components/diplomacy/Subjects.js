import React from 'react'
import { Button, Glyphicon, Panel, Row, Col, ButtonGroup } from 'react-bootstrap'
import { connect } from 'react-redux'

import { subjectRejected, subjectAccepted, updateDiplomacy } from '../ActionTypes'
import * as api from '../../api/api'

const mapStateToProps = ({ control: { login: { user} }}) => ({ user })

const Subject = connect(mapStateToProps)(React.createClass({
    getInitialState(){
        return { open: false, fph: this.props.forceAvailable }
    },

    onChange( event ){
        const { forceAvailable } = this.props
        this.setState({ fph: Math.min(forceAvailable, Math.max(0, event.target.value)) })
    },

    claim(){
			const { dispatch } = this.props
			console.log("Claiming " + this.state.fph + " fph of " + this.props.subject )
			api.troopClaim(this.state.fph, this.props.subject).then(
				() => api.loadDiplomacy().then(
						diplomacy => dispatch(updateDiplomacy(diplomacy))
				)
			)
    },

    cancel(e){
        const { subject, dispatch, user } = this.props
        console.log("Cancel surrender from user: " + subject)

        api.updateTerms({ king: user, subject, response: 'cancel' }).then(response => {
            if ( response.ok ){
                dispatch(subjectRejected(subject))
            }
        })
    },

    accept(e){
        const { subject, dispatch, user } = this.props

        console.log("Accept surrender from user: ", { subject })
        api.updateTerms({ king: user, subject, response: 'accept' }).then(response => {
            if ( response.ok ){
                dispatch(subjectAccepted(subject))
            }
        })
    },

    toggle(e){
         this.setState({ open: ! this.state.open })
    },

    render(){
        const { subject:name, forceAvailable:atDisposal, brnResources, whResources, fisotk, tax, status } = this.props
        
        const cancel = <Button bsSize="xsmall" title="cancel" bsStyle="danger" onClick={this.cancel}><Glyphicon glyph="remove-sign"/></Button>
        const accept = <Button bsSize="xsmall" title="accept" bsStyle="success" onClick={this.accept}><Glyphicon glyph="ok-sign"/></Button>
        const action = status === "accepted" ? cancel : accept
        const style = status === "pending" ? "warning" : "success"

        const resources = status === "accepted" ? [
            <Col key="bR" xs={3} title="food">
                <Row>Food</Row>
                <Row>{parseInt(brnResources*10,10)/10 || 0} units</Row>
            </Col>,
            <Col key="wR" xs={3} title="Materials">
                <Row>Materials</Row>
                <Row>{parseInt(whResources*10,10)/10 || 0} units</Row>
            </Col>
        ] :  (<Col xs={4}>Tax: {tax}%</Col>)
        
        const troops = status === "accepted" ?  
            (<Col xs={3} title="Force in service of the King">
                <Row>Troops at disposal</Row>
                <Row>{atDisposal || 0} fph</Row>
                <Row>
                    <input onChange={this.onChange} value={this.state.fph} type='number' />
                </Row>
                <Row>
                    <Button bsStyle="primary" bsSize="small" onClick={this.claim}>Claim troops</Button>
                </Row>
            </Col>) : 
            (<Col xs={5} title="Weekly troops to add to the force in service of the King">{fisotk}</Col>)

        const header = <div onClick={this.toggle} style={{ cursor: "pointer" }}>{name}</div>

        return <Panel collapsible expanded={this.state.open} header={header} bsStyle={style}>
            {resources}
            {troops}
            <Col xs={3}>
                <ButtonGroup>
                    {action}
                    { status === "pending" ? cancel : ""}
                </ButtonGroup>
            </Col>
        </Panel>
    }
}))

export default React.createClass({
    render(){
        const { subjects } = this.props
        const allegiances = subjects.map( subject => <Subject {...subject} key={subject.subject}/> )

        return <div id="view">
            <h1>Terms</h1>
            <Col xs={10}>{allegiances}</Col>
        </div>
    }
})
