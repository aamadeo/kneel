import React from 'react'
import { connect } from 'react-redux'
import { Panel } from 'react-bootstrap'

import Surrender from './Surrender'
import Subjects from './Subjects'
import SubjectObligations from './SubjectObligations'
import Allies from './Allies.js'

const mapStateToProps = ({ 
    diplomacy: { ruler, terms, allies },
    control: { loaded } 
}) => ({ terms, ruler, allies, loaded: loaded.diplomacy })
const SURRENDERING = 0
const NO_ALLEGIANCE = 1
const KING = 2

export default connect(mapStateToProps)( React.createClass({
    renderSubject(){
        const { allies, terms } = this.props

        if ( terms[0].status !== "accepted" ) {
            return <Surrender {...terms[0]} />
        }

        return <div>
            <Panel collapsible header="Obligations to the king" bsStyle="primary">
                <SubjectObligations {...terms[0]} />
            </Panel>
            <Panel collapsible header="Surrender terms" bsStyle="primary">
                <Surrender {...terms[0]} />
            </Panel>
            <Panel collapsible defaultExpanded header="Allies"  bsStyle="primary">
                <Allies allies={allies} />
            </Panel>
        </div>
            
    },
    render(){
        if ( ! this.props.loaded ) return <span>loading</span>   

        const { terms, ruler } = this.props
        let status = null
        status = ruler ? KING : (terms.length === 1 ? SURRENDERING : NO_ALLEGIANCE)

        switch(status){
            case SURRENDERING: return this.renderSubject()
            case NO_ALLEGIANCE: return <Surrender />
            case KING: return <Subjects subjects={terms} />
            default: return null
        }
    }
}))