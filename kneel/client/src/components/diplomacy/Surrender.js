import React from 'react'
import { connect } from 'react-redux'
import { Col, FormGroup, ControlLabel, FormControl, HelpBlock, InputGroup, Button } from 'react-bootstrap'

import * as api from '../../api/api'

const mapStateToProps = ({ control: { login: { user } } }) => ({ user })

export default connect(mapStateToProps)( React.createClass({

    getInitialState(){
        return { tax: '', fisotk: '', king: '' }
    },

    componentWillReceiveProps(nextProps){
        let { king, tax, fisotk, status } = nextProps

        console.log("New state", this.props)

        tax = tax || 4
        fisotk = fisotk || 30
        status = status || null

        const userExist = king 

        this.setState({userExist, king, tax, fisotk, status})
    },

    onChange(e){
        const key = e.target.id
        let value = e.target.value
        
        if( key === "king" ){
            api.freeUserName( value ).then( ({ free }) => { this.setState({ userExist: !free }) })
        } else if ( key === "tax") {
            value = value === null ? 0 : Math.max(0, Math.min(value, 100))
        } else if ( key === "fisotk") {
            value = value === null ? 0 : Math.max(0, value)
        }

        this.setState({ [key] : value })
    },

    submit(e){
        e.preventDefault()

        const data = Object.assign({},this.state)
        delete data.userExist

        if ( this.props.status === null ){
            api.surrender(data).then( response => {
                console.log(response)
                if ( response.status === "pending" ){
                    data.status = response.status
                    data.userExist = true
                    this.setState(data)
                }
            })
        } else {
            data.response = "update"

            api.updateTerms(data).then(response => {
                console.log(response)
            })
        }
    },

    cancel(){
        const { king, user } = this.props
        api.updateTerms({ subject: user, king, response: 'cancel' }).then(response => {
            console.log(response)

            if ( response.ok ){
                this.setState({ userExist: undefined, status: null, king: '', tax: '', fisotk: '' })
            }
        })
    },

    render(){
        const { king, tax, fisotk, status } = this.props
        const { userExist } = this.state
        const correct = typeof userExist !== "undefined" && userExist && tax !== '' && fisotk !== ''
        const validationState = typeof userExist === "undefined" ? undefined : ( userExist ? "success" : "error")
        const btnLabel = status === null ? "Surrender" : "Update"
        
        return ( 
            <Col xs={12} md={7}>
                <form onSubmit={this.submit}>
                    <h2>Diplomacy</h2>
                    <FormGroup controlId="king" validationState={validationState} onChange={this.onChange}>
                        <Col xs={12}><ControlLabel>Kneel to </ControlLabel></Col>
                        <Col xs={10}><FormControl value={king} type="text" placeholder="The king in the north" disabled={this.props.king} /></Col>
                        <Col xs={1}><FormControl.Feedback/></Col>
                        <Col xs={12}><HelpBlock>Enter the name of the player you're surrendering to</HelpBlock></Col>
                    </FormGroup>
                    <FormGroup controlId="tax">
                        <Col xs={12}><ControlLabel>Tax </ControlLabel></Col>
                        <Col xs={10}><InputGroup>
                            <FormControl type="number" placeholder="4" value={tax} onChange={this.onChange} />
                            <InputGroup.Addon>%</InputGroup.Addon>
                        </InputGroup></Col>
                        <Col xs={12}><HelpBlock>The % of your warehouses/barns capacity you give as tribute in a daily basis.</HelpBlock></Col>
                    </FormGroup>
                    <FormGroup controlId="fisotk">
                         <Col xs={12}><ControlLabel>Force in service of the King </ControlLabel></Col>
                         <Col xs={10}><InputGroup>
                            <FormControl type="number" value={fisotk} placeholder="30" onChange={this.onChange}/>
                            <InputGroup.Addon>fph</InputGroup.Addon>
                        </InputGroup></Col>
                         <Col xs={12}><HelpBlock>The amount of soldiers ( measured in food consumption fph ) the King has the right to claim per week.</HelpBlock></Col>
                    </FormGroup>
                    <FormGroup>
                        <Col xs={6}>
                            <Button bsStyle="info" disabled={!correct} type="submit"> {btnLabel} </Button>
                        </Col>
                        { status !== null ?
                        <Col xs={6}>
                            <Button bsStyle="info" onClick={this.cancel}> Cancel </Button>
                        </Col> : ""}
                    </FormGroup>
                </form>
            </Col>
        )
    }
}))