import React, { Component } from 'react'
import { withRouter } from 'react-router'


const Ally = ({ ally, selectUser }) => ({

    render(){
        return <li><a href="#" onClick={() => selectUser(ally)}>{ally}</a></li>
    }
})

class Allies extends Component {
    render(){
        const { router } = this.props

        const selectUser = function(user) {
            router.push("/user/" + user)
        }

        return <ul>
            {this.props.allies.map( ally => <Ally key={ally} ally={ally} selectUser={selectUser}/>)}
        </ul>
    }
}

export default withRouter( Allies )