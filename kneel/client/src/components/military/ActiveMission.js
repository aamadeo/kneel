import React from 'react'
import { connect } from 'react-redux'
import { Col, Row, Checkbox, Panel, Glyphicon, Button } from 'react-bootstrap'

import TroopList from './TroopList.js'
import Loot from '../reports/Loot.js'
import SiegeDamage from '../reports/SiegeDamage.js'

import * as api from '../../api/api.js'

const mapStateToProps = ({ control: { login, configuration }}) => ({ login, configuration })

export default connect(mapStateToProps)( React.createClass({
	getInitialState(){
		const t = new Date().getTime() / 1000
		return { t, dt: t - this.props.t0, loop: this.props.loop }
	},

	update(){
		const t = new Date().getTime() / 1000
		this.setState({ t, dt: t - this.props.t0 })
	},

	componentDidMount(){
		var intervalId = setInterval(this.update, 1000)
		this.setState({ intervalId })
	},

	componentWillUnmount(){
		clearInterval(this.state.intervalId)
	},

	toggleLoop(){
		const loop = ! this.state.loop
		const setState = () => this.setState( { loop })
		
		api.toggleLoop( this.props.id ).then ( response => {
			console.log( { response , loop, setState } )
			if ( response && response.ok ) setState()
		})
	},

	abort(){
		api.abort( this.props.id )
	},

	distance(p1, p2){
		const sum = Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y, 2)
		return Math.sqrt(sum)
	},

	render(){
		const { loot, objective, siegeDamage, siege, abortMission } = this.props
		let style = 'info'
		let prevObjective = objective.action === "return" && this.props.origin
		const { mission } = this.props.configuration

		objective.action = objective.action || this.props.action
		objective.name = objective.name || this.props.login.villageName
		objective.owner = objective.owner || this.props.login.user

		if ( prevObjective ){
			prevObjective = ` From (${prevObjective.x},${prevObjective.y}) ${prevObjective.name} of ${prevObjective.owner}`
			style = 'warning'
		}

		if ( loot ){
			style = 'success'
		} else if ( objective.action === mission.support ) {
			style = 'primary'
		} else if ( siege && siege.type === 'Trebuchet' ){
			style = 'danger'
		}

		const digit = d => ("00" + d).slice(-2)
		const { elapsedTime, from: location, casualties, troops, speed, totalTime } = this.props
		const dt = totalTime - elapsedTime // RemainingMilliseconds
		const totalSeconds = parseInt(Math.floor( dt / 1000 - this.state.dt), 10)
		//console.log({ stdt: this.state.dt, dt, totalSeconds, elapsedTime, totalTime })

		const hours = digit( Math.floor(totalSeconds / 3600) )
		const mins = digit( Math.floor((totalSeconds % 3600) / 60 ) )
		const seconds = digit( totalSeconds % 60 )

		var defaults = {  }

		if ( hours === 0 && mins === 0 && seconds === 0){
			console.log("Done: ", seconds, new Date(totalSeconds))
			this.props.refresh()
		} else if ( totalSeconds < 0 ){
			return <div />
		}

		for( var troop in troops ){
			defaults[troop] = {
				count: troops[troop],
				deaths: casualties ? casualties[troop] : 0,
				troop
			}
		}

		const header = `to ${objective.action} (${objective.x},${objective.y}) ${objective.name} of ${objective.owner} ${prevObjective || ""}`
		const timeRemaining = totalSeconds > 0 ? `${hours}:${mins}:${seconds}` : 'Arriving...'

		return (
			<Row>
				<Col xs={11}>
				<Panel collapsible header={header} bsStyle={style} style={{ cursor: 'pointer' }}>
					<Row>
						<Col xs={10}>
							<TroopList readOnly={true} defaults={defaults} group={this.props.id} owners={[this.props.login.user]} casualties={true}/>
							{ this.props.loot ? <Loot loot={this.props.loot} width="100%"/> : ""}
							{ this.props.taxes ? <Loot loot={this.props.taxes} width="100%"/> : ""}
						</Col>
						<Col xs={2}>
							{ objective.action === mission.support ? "" : 
							<Checkbox inputRef={ref => { this.loop = ref}} onChange={this.toggleLoop} checked={this.state.loop} inline> Loop mission </Checkbox>}
							<p>
								eta: {timeRemaining}<br/>
								distance : {Math.round(this.distance(location, objective))} sq<br/>
								speed : {Math.round(speed * 3600)} sq/h
							</p>
							{ ! (abortMission || prevObjective) ? <Button bsStyle="danger" onClick={this.abort}>
								<span> Abort </span>
								<Glyphicon glyph="remove"/>
							</Button> : "" }
							
						</Col>
					</Row>
						{siege && siege.type === 'trebuchet' ?
						<Row>
							<Col xs={12}>
								<SiegeDamage damage={siegeDamage} width="100%"/>
							</Col>
						</Row>
						: "" }
				</Panel>
				</Col>
			</Row>
		)
	}

}))