import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Panel } from 'react-bootstrap'

import { loadMissions } from '../../api/api'
import { updateMissions } from '../ActionTypes'

import ActiveMission from './ActiveMission'
import TroopList from './TroopList'
import Loot from '../reports/Loot'

let ActiveMissions = React.createClass({

	load(){
		const { dispatch } = this.props

		loadMissions().then( response => {
			dispatch( updateMissions(response) )
		})
	},
	
	componentDidMount() {
		this.load()
	},

	render() {
		const mTroops = { }
		const mLoot = {}
		const missionComponents = this.props.missions.map( mission => {
			let { troops, loot } = mission

			if ( mission.taxes ){
				loot = mission.taxes
			}

			for( let troop in troops ){
				if ( ! mTroops[troop] ) {
					mTroops[troop] = { count: parseInt(troops[troop], 10)}
				} else {
					mTroops[troop].count += parseInt(troops[troop], 10)
				}
			}

			if( loot ){
				for( let r in loot ){
					if ( ! mLoot[r] ) {
						mLoot[r] = parseInt(loot[r], 10)
					} else {
						mLoot[r] += parseInt(loot[r], 10)
					}
				}
			}

			return <ActiveMission key={mission.id} {...mission} refresh={this.load}/> 
		})

		return (
			<div className="traveling-troops">
				<Row>
					<Col xs={11}>
						<Panel defaultExpanded collapsible header="Missions" bsStyle="primary">
							<TroopList readOnly={true} defaults={mTroops} group={"missions"} owners={[this.props.login.user]}/>
							<h3>Total Loot</h3>
							<Loot loot={mLoot} width="100%"/>
						</Panel>
					</Col>
				</Row>
				{missionComponents}
			</div>
		)
	}
})

export default connect(
	({ 
		military: { missions },
		control: { login, tab }
 	}) => ({ missions, login, update: tab.view === "troops" })
)(ActiveMissions)