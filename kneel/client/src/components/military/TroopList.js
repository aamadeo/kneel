import React from 'react'
import Troop from './Troop'
import { connect } from 'react-redux'
import TroopEditable from './TroopEditable'
import ResponsiveBox from '../ResponsiveBox'

const mapStateToProps = ({ control }) => ({ control })

export default  connect(mapStateToProps)(React.createClass({
	getInitialState(){
		return { troops: {} }
	},
	renderTroop(name){
		var key = this.props.group + `(${name})`
		const troop = Object.assign({}, this.props.defaults[name])
		troop.count = troop.count ? troop.count : 0
		
		if ( this.props.readOnly ){
			return <Troop name={name} troop={troop} key={key} casualties={this.props.casualties}/>	
		}
		
		return <TroopEditable name={name} default={troop.count} key={key} getters={this.props.getters} disabled={troop.disabled} onChange={this.props.onChange} />
	},
	renderOwners(owners){
		return (
			<div className="troop">
				<span className="troop-q"></span>
				<img className="troop-ico" alt="troops" src="/img/banner.png" title={owners[0]}/>
				{
					owners.map((name) => { 
						var tombstone = this.props.casualties ? <img src="/img/tombstone.png" height="70%" alt="deaths"/> : ""
					
						return ( 
							<div key={name} style={{ width : "100%", height: "100%" }}>
								<span className="troop-q">{tombstone}</span>
							</div>
						)
					})
				}
			</div>
		)
	},
	render() {
		var troopList = []
		const troopsNames= this.props.control.configuration.troops

		troopsNames.forEach( troop => {
			troopList.push(this.renderTroop(troop))
		})

		return (
			<ResponsiveBox width={100} whRatio={20} prefix="ltroops">
				<div className="troop-list" style={{ height: "100%" }}>
					{this.props.noOwner ? "" : this.renderOwners(this.props.owners)}
					{troopList}
				</div>
			</ResponsiveBox>
		)
	}
}))