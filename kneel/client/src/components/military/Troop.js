import React from 'react'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'

import { connect } from 'react-redux'

const mapStateToProps = ({ control }) => ({ control })

export default  connect(mapStateToProps)( React.createClass({
	renderStats(profile){
		const attrs = Object.keys(profile).map( attr =>  
			<li key={attr}>{attr} : {profile[attr]}</li>
		)

		const style = { 
			listStyleType: 'none'
		}

		return <ul style={style}>{attrs}</ul>
	},

	render() {
		
		const title=""
		const { troop, name, control } = this.props
		const src = `/img/${name}.png`
		let count = troop ? troop.count : ""
		
		let casualties = ""
		if ( this.props.casualties ){
			casualties = troop ? troop.deaths : ""
		}

		const troopClass = "troop" + ( count > 0 ? "" : " disabled")

		const profile = control ? control.configuration.profiles[name]
								: undefined

//		console.log({ profiles: control.configuration.profiles })
		const stats = profile ? this.renderStats(profile) : ""

		const menu = <Tooltip id={name} className="stat" >
			{stats}
		</Tooltip>

		return (
			<OverlayTrigger placement="bottom" overlay={menu}>
				<div className={troopClass}>
					<img src={src} className="troop-ico" alt={title}/>
					<span className="troop-q">{count}</span>
					<span className="troop-q death">{casualties}</span>
				</div>
			</OverlayTrigger>
		)
	}
}))