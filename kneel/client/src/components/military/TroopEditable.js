import React from 'react'
import {withRouter} from 'react-router'

export default  withRouter( React.createClass({
	getInitialState(){
		return { count: 0 }
	},
	componentWillMount(){
		var name = this.props.name
		
		this.props.getters[name] = () =>{
			return this.state.count
		}
	},
	handleChange(e){
		const count = parseInt(e.target.value, 10)
		
		this.setState({ count })

		const { name, onChange } = this.props

		if ( onChange ){
			onChange({ name, count })
		}
	},
	render() {
		const src = `/img/${this.props.name}.png`
		const troop = this.props.name
		const { disabled } = this.props 
		const className = "troop" + ( disabled ? " unavailable" : "" )

		return (
			<div className={className}>
				<img src={src} className="troop-ico" alt={troop}/>
				<input type="number" min={0} className="troop-q editable" defaultValue={this.state.count} onChange={this.handleChange} width="100%" readOnly={disabled}/>
			</div>
		)
	}
}))