import React from 'react'

import { Row } from 'react-bootstrap'

import LocalTroops from './LocalTroops'
import ActiveMissions from './ActiveMissions'

export default (props) => ({

	render(){
		return (
			<div>
				<Row>
					<LocalTroops/>
				</Row>
				<Row>
					<ActiveMissions/>
				</Row>
			</div>
		)
	}
})