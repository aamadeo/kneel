import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Panel, Glyphicon } from 'react-bootstrap'

import * as api from '../../api/api'
import { updateTroops, updateMissions } from '../ActionTypes'

import TroopList from './TroopList'

 let LocalTroops = (props) => ({
	refresh(){
		console.log("Refresh Troops")
		const dispatch = props.dispatch

		api.loadVillageTroops().then( response => {
			dispatch(updateTroops(response.troops))
		})

		api.loadMissions().then( response => {
			console.log({ response })
			dispatch( updateMissions(response) )
		})
	},
	render() {
		const header = <a href="#" onClick={this.refresh}>
			<span> {"@" + this.props.villageName} </span>
			<Glyphicon glyph="refresh"/>
		</a>

		return <Row> 					
			<Col xs={11}>
				<Panel defaultExpanded header={header} bsStyle="primary">
					<TroopList defaults={this.props.troops} readOnly={true} getters={{}} group={"home"} owners={[this.props.user]}/>
				</Panel>
			</Col>
		</Row>
	}
})

const mapStateToProps = ({ 
	village: { troops }, 
	control: { 
		login: { user, villageName } 
	}
}) => ({ troops, user, villageName })

export default connect(mapStateToProps)(LocalTroops)