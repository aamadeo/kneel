import React from 'react'
import { connect } from 'react-redux'
import { Button, Row, Col } from 'react-bootstrap'

import { spawnArmy, loadTasks } from '../../api/api'
import * as actions from '../ActionTypes'
import core from '../../api/core'
import TroopList from '../military/TroopList'
import Cost from '../production/Cost'

const mapStateToProps = ({ 
    village: { buildings },
    control: { 
        login : { user },
        configuration: { tech, costs }
     } 
}) => ({ buildings, user, tech, costs })
const costZero = () => ({food: 0, iron:0, stone:0, wood:0, time: 0})

export default connect(mapStateToProps)(React.createClass({
    get: {},
    getInitialState(){
        return { cost: costZero() }
    },

    calcDisabled(){
        let hasBarracks = false
        let hasArchery = false
        let hasStable = false
        let hasSWFactory = false

        this.props.buildings.forEach( b => {
            if ( !b ) return

            hasBarracks = hasBarracks || b.name === "Barracks"
            hasArchery = hasArchery || b.name === "Archery"
            hasStable = hasStable || b.name === "Stable"
            hasSWFactory = hasSWFactory || b.name === "SiegeWeaponFactory"
        })

        const troops = {
            infantry:{ count: 0, disabled: ! hasBarracks },
            pikeman:{ count: 0, disabled: ! hasBarracks },
            archer: { count: 0, disabled: ! hasArchery },
            "heavy-archer": { count: 0, disabled: ! hasArchery },
            scout: { count: 0, disabled: ! hasStable },
            knight: { count: 0, disabled: ! hasStable },
            ram: { count: 0, disabled: ! hasSWFactory },
            catapult: { count: 0, disabled: ! hasSWFactory },
            trebuchet: { count: 0, disabled: ! hasSWFactory }
        }

        this.setState({ troops })
    },

    submit(){
        const { troops } = this.state
        const { dispatch } = this.props
        const data = {}

        for( let t in troops ){
            if ( troops[t].count === 0 ) continue
            data[t] = troops[t].count
        }

        spawnArmy(data).then(
            () => loadTasks().then(
				tasks => {
					dispatch( actions.updateTasks(tasks) )
					dispatch( actions.changeTab("tasks") )
				}
			)
        )

    },

    onChange({ name, count }){
        const troops = Object.assign({}, this.state.troops)
        const cost = costZero()

        troops[name].count = count

        for( let t in troops ){
            if ( troops[t] === 0 ) continue
            
            let troopCost = core.getCostOf(t, 0, this.props.costs)
            for( let c in cost ){
                cost[c] += troopCost[c] * troops[t].count
            }
        }

        this.setState({ troops, cost })
    },

    componentWillMount(){
        this.calcDisabled()
    },

    render(){
        return (
            <div className="view">
                <Row>
                    <Col xs={10} xsOffset={1}>
                        <h2>Army spawn</h2>
                        <TroopList noOwner={true} onChange={this.onChange} readOnly={false} defaults={this.state.troops} getters={this.get} group={""} owners={[this.props.user]}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs={15} xsOffset={1}>
                        <Cost title="" cost={this.state.cost} />
                    </Col>
                </Row>
                <Row>
                    <Col xs={2} xsOffset={1}>
                        <Button bsStyle="info" onClick={this.submit} block> Train </Button>
                    </Col>
                </Row>
            </div>
        )
    }
}))