import React from 'react'
import Buildings from './Buildings'
import ResponsiveBox from '../ResponsiveBox'

export default React.createClass({
    render() {
        return (
            <div className="view">
                <ResponsiveBox width={100} whRatio={44} prefix="vill">
                    <Buildings/>
                </ResponsiveBox>
            </div>
        )
    }
})