import React from 'react'
import classNames from 'classnames'
import config from './Config'
import { OverlayTrigger, Popover } from 'react-bootstrap'
import BuildingItems from './BuildingItems'
import Item from './Item'
import core from '../../api/core'

export default (props) => ({
    render() {
        var style = {
            top: config.center.y - this.props.slotConfig.y - ( 0.4 + this.props.slotConfig.k ) * config.radius.slot,
            left: config.center.x + this.props.slotConfig.x - this.props.slotConfig.k * config.radius.slot,
            width: this.props.slotConfig.k * 2 * config.radius.slot,
            height: this.props.slotConfig.k * 2 * config.radius.slot
        }

        style = {
            top: style.top + "%",
            left: style.left + "%",
            width: style.width + "%",
            height: style.height + "%"
        }
        
        var classes = classNames('slot')
        
        var items = BuildingItems[this.props.name].map((item)=>{ 
			var level = 0
            item.slot = this.props.data.slot
            const { tech, costs } = this.props

            if ( item.type !== "link" && ! core.techAvailable(item.name, tech) ) {
                return null
            }

            if ( item.type === "research" && (!core.techAvailable(item.name, tech) || core.techEnabled(item.name, tech)) ) {
                return null
            }
            
			if ( item.name === this.props.name ) {
				level = parseInt(this.props.data.detail.nextLevel,10) || parseInt(this.props.data.detail.level,10) + 1
                item.level = level
			} else if ( item.type === "resourceTech" ){
                const { occupationsLevels } = this.props
                const { occupation } = item 
                level = parseInt( occupationsLevels[occupation] + 1, 10)
            }

			var cost = item.type !== "link" ? core.getCostOf(item.name, level, costs) : null

			return <Item key={item.name} cost={cost} {...item}/>
		})

        var menuId = "menu-b" + this.props.data.slot
        var title = `${this.props.name} Lv: ${this.props.data.detail.level}`

        const menu = (
            <Popover id={menuId} title={title}>
                <div className="bm-items">
                    {items}
                </div>
            </Popover>
        )

        const src = `../img/${this.props.name}.png`

        return (
            <div className={classes} style={style}>
                <OverlayTrigger trigger="focus" placement="bottom" overlay={menu}>
                    <a href="#" tabIndex="0">
                        <img src={src} className='building' alt={this.props.name}/>
                    </a>
                </OverlayTrigger>
            </div>
        )
    }
})