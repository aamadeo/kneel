import React from 'react'
import {Tooltip, OverlayTrigger} from 'react-bootstrap'
import Cost from '../production/Cost'
import { connect } from 'react-redux'
import * as api from '../../api/api'
import * as actions from '../ActionTypes'

const afterBuy = (dispatch, response, type) => {

	if ( response.errorCode !== null ){
		if ( response.lacks !== null ){
			dispatch( actions.lacks(response.lacks) )
		}
	} else {
		dispatch(actions.updateResources(response.resources))

		if ( type === "build"){
			dispatch(actions.updateBuildings(response.buildings))
			dispatch(actions.updateOccupations(response.occupations) )
		}

		if ( type !== "spawn" ){
			api.loadTasks().then(
				tasks => {
					dispatch( actions.updateTasks(tasks) )
					dispatch( actions.changeTab("tasks") )
				}
			)
		}

		if ( type === "upgradeOccupation" ){
			dispatch( actions.updateOccupations(response.occupations) )
		}
	}
}

const Item = (props) => ({
	action(e){
		var item = Object.assign({}, props)

		if ( item.type === "build" || item.type === "upgradeBuilding" ){
			console.log(props.builders)
			item.builders = parseInt(props.builders, 10)
		}
	
		console.log("Item :", item)
		const { dispatch } = props

		if ( item.name.match(/Tech$/) ){
			item.occupation = item.name.replace("Tech","")
			item.type = "upgradeOccupation"
		}

		if ( api.hasOwnProperty(item.type) ){
			api[item.type](item).then( response => {
				console.log("API::response = ", response)
				afterBuy(dispatch, response, item.type)
			})
		} else if( props.type === "link" ) {
			dispatch( actions.changeTab(props.name) )
		} else {
			console.log("Item desconocido")
		}
	},
	render() {
		
		var name = this.props.name
		let icon = name
		let src = ""

		//console.log(this.props)

		if ( this.props.icon ) icon = this.props.icon

		if ( name.match(/.+Tech$/) ) {
			const occupation = name.replace("Tech","")
			src = `/img/${occupation}-occupation.png`
			
		} else {
			src = `/img/${icon}.png`
		}

		let tooltip = null

		if ( this.props.cost !== null ) {
			const title = this.props.name + (this.props.level ? " Lv" + this.props.level : "")
			const cost = <Cost title={title} cost={this.props.cost} />
			tooltip = <Tooltip id="tooltip">{cost}</Tooltip>

			return (
				<OverlayTrigger placement="top" overlay={tooltip}>
					<div className="bm-item" onClick={this.action}>
						<img src={src} style={{ height: "20%" }} alt={title}/>
					</div>
				</OverlayTrigger>
			)	
		}
		
		return (
			<div className="bm-item" onClick={this.action}>
				<img src={src} alt={this.props.name}/>
			</div>
		)
	}
})

export default connect( ({ control: { builders } }) => ({ builders }))(Item)