import React from 'react'
import classNames from 'classnames'
import config from './Config'
import Item from './Item'
import core from '../../api/core'

import { OverlayTrigger, Popover } from 'react-bootstrap'

export default React.createClass({
    render() {
        var style = {
            top: config.center.y - this.props.slotConfig.y - ( 1.5 + this.props.slotConfig.k ) * config.radius.slot,
            left: config.center.x + this.props.slotConfig.x - this.props.slotConfig.k * config.radius.slot,
            width: this.props.slotConfig.k * 2 * config.radius.slot,
            height: this.props.slotConfig.k * 2 * config.radius.slot
        }

        style = {
            top: style.top + "%",
            left: style.left + "%",
            width: style.width + "%",
            height: style.height + "%"
        }
        
        var classes = classNames('slot')

        var upgrade = {
            upgrade: true,
            slot: this.props.data.slot,
            name: "Tower",
            type: "building"
        }
        var cost = core.getCostOf("Tower", parseInt(this.props.data.level, 10) + 1)
        var items = [<Item key={upgrade.name} cost={cost} {...upgrade}/>]

        var menuId = "menu-b" + this.props.data.slot
        var title = `${this.props.name} Lv: ${this.props.data.level}`

        const menu = (
            <Popover id={menuId} title={title}>
                <div className="bm-items">
                    {items}
                </div>
            </Popover>
        )

        var src = `../img/${this.props.data.name}.png`
        return (            
            <div className={classes} style={style}>
                <OverlayTrigger trigger="focus" placement="bottom" overlay={menu}>
                    <a href="#" tabIndex="0">
                        <img src={src} className='building' alt='Tower'/>
                    </a>
                </OverlayTrigger>
            </div>
        )
    }
})