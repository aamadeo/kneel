export default {
	Building: [],
	TownCenter: [
		{name: "TownCenter", type: "upgradeBuilding"},
		{name: "peasant", type: "spawn"},
		{name: "foodTech", type: "resourceTech", occupation: "food"},
		{name: "ironTech", type: "resourceTech", occupation: "iron"},
		{name: "stoneTech", type: "resourceTech", occupation: "stone"},
		{name: "woodTech", type: "resourceTech", occupation: "wood"},
		{name: "buildersTech", type: "resourceTech", occupation: "builders"}
	],
	Archery: [
		{name: "Archery", type: "upgradeBuilding"},
		{name: "archer", type: "spawn"},
		{name: "heavy-archer", type: "spawn"},
		{name: "multi-spawn", type: "link", icon: "GroupA"}
	],
	Barracks: [
		{name: "Barracks", type: "upgradeBuilding"},
		{name: "infantry", type: "spawn"},
		{name: "pikeman", type: "spawn"},
		{name: "multi-spawn", type: "link", icon: "GroupB"}
	],
	Stable: [
		{name: "Stable", type: "upgradeBuilding"},
		{name: "scout", type: "spawn"},
		{name: "knight", type: "spawn"},
		{name: "multi-spawn", type: "link", icon: "GroupC"}
	],
	SiegeWeaponFactory: [
		{name: "SiegeWeaponFactory", type: "upgradeBuilding"},
		{name: "ram", type: "spawn"},
		{name: "catapult", type: "spawn"},
		{name: "trebuchet", type: "spawn"},
		{name: "multi-spawn", type: "link", icon: "GroupS"}
	],
	Barn: [	
		{name: "Barn", type: "upgradeBuilding"},
	],
	University: [
		{name: "University", type: "upgradeBuilding"},
		{name: "Ram Research", type: "research", icon: "ram"},
		{name: "Catapult Research", type: "research", icon: "catapult"},
		{name: "Trebuchet Research", type: "research", icon: "trebuchet"},
		{name: "Siege", type: "research", icon: "SiegeWeaponFactory"},
		{name: "Archery Research", type: "research", icon: "Archery"},
		{name: "Stable Research", type: "research", icon: "Stable"},
		{name: "Blacksmith Research", type: "research", icon: "Blacksmith"},
	],
	Warehouse: [
		{name: "Warehouse", type: "upgradeBuilding"},
	],
	Market: [
		{name: "trade-cart", type: "spawn"},
	]
}