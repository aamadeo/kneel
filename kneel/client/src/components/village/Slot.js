import React from 'react'
import classNames from 'classnames'
import config from './Config'
import Item from './Item'
import core from '../../api/core'

import { OverlayTrigger, Popover } from 'react-bootstrap'

export default React.createClass({
    render() {
        var style = {
            top: config.center.y - this.props.y - ( 0.4 + this.props.k ) * config.radius.slot,
            left: config.center.x + this.props.x - this.props.k * config.radius.slot,
            width: this.props.k * 0.85 * config.radius.slot,
            height: this.props.k * 1.5 * config.radius.slot,
            transform: "rotateZ(10deg) rotateX(10deg)"
        }

        style = {
            top: style.top + "%",
            left: style.left + "%",
            width: style.width + "%",
            height: style.height + "%"
        }

        var classes = classNames('slot','empty')
        
        //invocar al menu empty slot(id)
        var items = []
        var buildings = core.buildingsNames;

        if ( this.props.slot === 12 ) {
            items.push({name: "TownCenter", type: "build", slot: 12})
        } else if ( this.props.slot > 12 ) {
            items.push({name: "Tower", type: "build", slot: this.props.slot})
        } else {
            for( var i = 0 ; i < buildings.length; i++){
                items.push({name: buildings[i], type: "build", slot: this.props.slot})
            }
        }

        const { tech, costs } = this.props

        items = items.map((item)=>{

            if ( ! core.techAvailable(item.name, tech) ) {
                return null
            }

            var cost = core.getCostOf(item.name, 0, costs)
            return <Item key={item.name} cost={cost} {...item}/>
        }) 

        var menuId = "menu-s" + this.props.slot
        const menu = (
            <Popover id={menuId} title="Empty Lot">
                <div className="bm-items">
                    {items}
                </div>
            </Popover>
        )

        return (
            <OverlayTrigger trigger="focus" placement="bottom" overlay={menu}>
                <div className={classes} style={style} title='click to build'>
                    <a href="#" tabIndex="0" className="focusCatcher"></a>
                </div>
            </OverlayTrigger>
        )
    }
})