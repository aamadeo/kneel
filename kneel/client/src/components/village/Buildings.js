import React from 'react'
import { connect } from 'react-redux'

import Slot from './Slot'
import Tower from './Tower'
import Building from './Building'
import config from './Config'

var BuildingRCs = {
    Tower: Tower
}

let Buildings = (props) => ({
    slotConfigs: [],
    componentWillMount(){
        let slots = [], slotConfigs = []
        let outRing = 8
        let s = 0;

        for(let i = 0 ; i < outRing ; i++){
            let alpha = i / outRing
            let x = Math.cos(alpha*2*Math.PI) * config.radius.town.x
            let y = Math.sin(alpha*2*Math.PI) * config.radius.town.y
            
            slotConfigs.push( this.slot( x, y, 1.1, i) )
            slots.push(slotConfigs[s++])
        }
        
        let inRing = 4;
        for(let i = 0 ; i < inRing ; i++){
            let alpha = 0.125 + i / inRing
            let x = Math.cos(alpha*2*Math.PI) * config.radius.town.x / 1.9
            let y = Math.sin(alpha*2*Math.PI) * config.radius.town.y / 1.9
            
            slotConfigs.push( this.slot( x, y, 1.1 , 8 + i) )
            slots.push(slotConfigs[s++])
        }

        slotConfigs.push( this.slot(-2, 0, 1.5, 12) )
        slots.push(slotConfigs[s++])

        slotConfigs.push( this.slot( -config.radius.town.x * 1.1, -config.radius.town.y, 1, 13) )
        slots.push(slotConfigs[s++])

        slotConfigs.push( this.slot( -config.radius.town.x * 1.1, config.radius.town.y * 0.7 , 1, 14) )
        slots.push(slotConfigs[s++])

        slotConfigs.push( this.slot( config.radius.town.x * 1.1, config.radius.town.y * 0.7, 1, 15) )
        slots.push(slotConfigs[s++])

        slotConfigs.push( this.slot( config.radius.town.x * 1.1, -config.radius.town.y, 1, 16) )
        slots.push(slotConfigs[s++])

        this.slotConfigs = slotConfigs
    },
    slot(x,y,k,slotID){
        const { tech, costs } = this.props

        const slot = {
            x: x,
            y: y,
            k: k,
            reactElement(){
                return <Slot key={slotID} x={x} y={y} k={k} slot={slotID} tech={tech} costs={costs}/>
            }
        }

        return slot
    },
    renderBuilding(name, data, slotConfig){
        let MyComponent = BuildingRCs[name]
        const { costs, tech } = this.props
        const bProps = { costs, tech, slotConfig, data, name }

        if ( name === "TownCenter"){
            const occupationsLevels = {}

            for( let occupation in this.props.occupations ){
                occupationsLevels[occupation] = this.props.occupations[occupation].level
            }

            bProps.occupationsLevels = occupationsLevels
        }

        if ( ! MyComponent ) MyComponent = Building

        return <MyComponent key={data.slot} {...bProps}/>
    },
    render() {
        var buildings = []

        for( var i = 0; i < this.slotConfigs.length ; i++){
            
            if( this.props.buildings[i] ){
                var name = this.props.buildings[i].name
                var data = this.props.buildings[i]
                var slotConfig = this.slotConfigs[i]

                buildings.push(this.renderBuilding(name, data, slotConfig))
            } else {
                buildings.push(this.slotConfigs[i].reactElement())
            }
        }
        
        return (
            <div className='buildings-container'>
            	{buildings}
            </div>
        )
    }
})

export default connect(
    ({ 
        village: { buildings, occupations },
        control: { configuration: { tech, costs } }  
    }) => ({ buildings, tech, costs, occupations })
)(Buildings)