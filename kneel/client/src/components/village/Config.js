export default {
    radius: {
        town: {
            x: 40,
            y: 37.5,
        },
        slot: 10
    },
    center: { 
        x: 55,
        y: 55
    }
}