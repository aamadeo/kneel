import React from 'react'
import Tooltip  from 'rc-tooltip';
import core from '../../api/core'

function sectionIn(x, y, radius){
    return {
        x: Math.floor((radius + x) / (radius*2+1)),
        y: Math.floor((radius + y) / (radius*2+1))
    }
}

function title(village){
	var tooltip = `Village ${village.name} of ${village.owner}`
  	return (<span>{tooltip}</span>)
}

export default React.createClass({
    render() {
        var location = {
            x: core.location.x,
            y: core.location.y
        },objective = {
            x: this.props.target.x,
            y: this.props.target.y
        }
    
        var center = {
            x: (location.x + objective.x) / 2,
            y: (location.y + objective.y) / 2
        }

        var radius = core.radius
        var oSection = sectionIn(objective.x, objective.y, radius)
        var locSection = sectionIn(location.x, location.y, radius)

        var range = {
            left: Math.min(location.x, objective.x) - 2,
            right: Math.max(location.x, objective.x) + 2,
            top: Math.max(location.y, objective.y) + 2,
            bottom: Math.min(location.y, objective.y) - 2
        }

        var cs = core.cellSize
        var maxDistance = Math.max( (range.right - range.left),  (range.top - range.bottom))
        var size = Math.min( 100, maxDistance * cs)
        let factor = { x: size / ((range.right - range.left) * cs), y: size / ((range.top - range.bottom)*cs)}

        var style = {
            width: size + "%",
            height: size + "%",
				backgroundSize:  (size) + "%"
        }
		  
        location.y = location.y - center.y
        location.x = location.x - center.x
        objective.y = objective.y - center.y
        objective.x = objective.x - center.x

        var locStyle = {
            position: "absolute",
            top:    (size/2 - factor.y*location.y*cs) + "%",
            left:   (size/2 + factor.x*location.x*cs) + "%",
				zIndex: 1
        }
		  
        var objStyle = {
            position: "absolute",
				top:  (size/2 - factor.y*objective.y*cs) + "%",
            left: (size/2 + factor.x*objective.x*cs) + "%",
				zIndex: 1
        }

		  var dy = parseFloat(objStyle.top) - parseFloat(locStyle.top)
		  var dx = parseFloat(objStyle.left) - parseFloat(locStyle.left)
		  var d = Math.sqrt(dx*dx + dy*dy)
		  var alpha = ( Math.atan(dy/dx) * 180 / Math.PI - 90) + "deg"

		  var plStyle = {
			  position: "absolute",
			  width: "1px",
			  height: Math.round(d) + "%",
			  transform: `rotateZ(${alpha})`,
			  top: (50 - d / 2) + "%",
			  left: "45%"
		  }

        return (
			<div className="path-wrapper">
				<div className="mission-path" style={style}>
					<div className="path-point origin" style={locStyle}></div>
					<div className="path-line" style={plStyle}></div>
					<div className="path-point objective" style={objStyle}></div>
				</div>
			</div>
        )
    }
})