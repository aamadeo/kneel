import React from 'react'

import { Col, Row, ListGroup, Button, InputGroup, FormControl, Glyphicon, HelpBlock, ControlLabel } from 'react-bootstrap'

class SiegeInfo extends React.Component {
    constructor(props){
        super(props)
        this.timeLeftChange = this.timeLeftChange.bind(this)
        this.useTimeLeftChange = this.useTimeLeftChange.bind(this)
        this.damageThresholdChange = this.damageThresholdChange.bind(this)
        this.useDamageThresholdChange = this.useDamageThresholdChange.bind(this)
        this.destroyAllChange = this.destroyAllChange.bind(this)
    }

    addTarget(){
        const { targets, onChange } = this.props

        const target = {
            building: 'TownCenter',
            weight: 1
        }

        onChange( { targets: [...targets, target] })
    }

    unitsChange(target){
        const {targets, onChange} = this.props
        return (e) => {
            const targetDef = Object.assign({}, targets[target], {weight: e.target.value})

            onChange({ targets: targets.map( (t, i) => (i === target ? targetDef : t)) })
        }
    }

    buildingChange(target){
        const {targets, onChange} = this.props
        return e => {
            const targetDef = Object.assign({}, targets[target], {building: e.target.value})

            onChange({ targets: targets.map( (t, i) => (i === target ? targetDef : t)) })
        }
    }

    destroyAllChange(e){
        this.props.onChange({ destroyAll: e.target.checked })
    }

    timeLeftChange(e){
        const { endCondition } = this.props

        endCondition.timeLeft = endCondition.useTimeLeft && parseInt(e.target.value,10 )

        this.props.onChange({ endCondition })
    }

    useTimeLeftChange(e){
        const { endCondition } = this.props

        endCondition.useTimeLeft = e.target.checked

        this.props.onChange({ endCondition })
    }

    damageThresholdChange(e){
        const { endCondition } = this.props

        endCondition.damageThreshold = endCondition.useDamageThreshold && parseInt(e.target.value, 10)
        
        this.props.onChange({ endCondition })
    }

    useDamageThresholdChange(e){
        const { endCondition } = this.props

        endCondition.useDamageThreshold = e.target.checked
        
        this.props.onChange({ endCondition })
    }

    removeTarget(target){
        const { targets, onChange } = this.props

        return () => {
            const newTargets = []
            targets.forEach( (t, i) => {
                if ( i === target ) return
                newTargets.push(t)
            })
            onChange(newTargets)
        }
    }

    render(){
        const total = this.props.targets.reduce( (a,b) => a + parseInt(b.weight, 10), 0)
        const buildings =  this.props.buildings.map( b => (<option value={b} key={b}>{b}</option>)  )
        const endCondition = this.props.endCondition || {}

        const targets = this.props.targets.map( (target, idx) => {
            const percentage = Math.round(target.weight/total * 100)

            return <div key={idx}>
                <InputGroup>
                    <InputGroup.Addon>{idx+1}</InputGroup.Addon>
                    <FormControl componentClass="select" onChange={this.buildingChange(idx)}>
                        {buildings}
                    </FormControl>
                    <InputGroup.Addon>weight</InputGroup.Addon>
                    <FormControl type="number" value={target.weight} onChange={this.unitsChange(idx)}/>
                    <InputGroup.Addon>%{percentage}</InputGroup.Addon>
                    <FormControl componentClass={Button} bsStyle="primary" onClick={this.removeTarget(idx)}>
                        <Glyphicon glyph='trash'/>
                    </FormControl>
                </InputGroup>
            </div>
        })

        console.log({ endCondition })

        return <div>
            <h4>Siege Info</h4>
            { this.props.type === 'Trebuchet' ? <Row><Col  sm={12} md={12} lg={7}>
                <input type="checkbox" value={this.props.destroyAll} onChange={this.destroyAllChange}/>
                <ControlLabel> Destroy village completely </ControlLabel>
                <HelpBlock>Destroy every single building in the village.</HelpBlock>
                <ControlLabel>Siege Duration</ControlLabel>
                <InputGroup>
                    <FormControl type="number" value={(endCondition.useTimeLeft && endCondition.timeLeft) || ''} onChange={this.timeLeftChange} disabled={! endCondition.useTimeLeft}/>
                    <InputGroup.Addon>mins</InputGroup.Addon>
                    <InputGroup.Addon>
                        <input type="checkbox" value={endCondition.useTimeLeft} onChange={this.useTimeLeftChange}/>
                    </InputGroup.Addon>
                </InputGroup>
                <HelpBlock>Maximum duration of siege.</HelpBlock>
                <ControlLabel>Retreat condition</ControlLabel>
                <InputGroup>
                    <FormControl type="number" value={(endCondition.useDamageThreshold && endCondition.damageThreshold) || ''} onChange={this.damageThresholdChange} disabled={! endCondition.useDamageThreshold}/>
                    <InputGroup.Addon>fph</InputGroup.Addon>
                    <InputGroup.Addon>
                        <input type="checkbox" value={endCondition.useDamageThreshold} onChange={this.useDamageThresholdChange}/>
                    </InputGroup.Addon>
                </InputGroup>
                <HelpBlock>retreat after troops lost (fph) in attack</HelpBlock>
            </Col></Row>
            : ""}
            <Row>
                <Button bsStyle="primary" className='pull-right' onClick={this.addTarget.bind(this)}>
                    Add Target
                </Button>
            </Row>
            <Row>
                <ListGroup fill>
                    {targets}
                </ListGroup>
            </Row>
        </div>
    }
}

export default SiegeInfo