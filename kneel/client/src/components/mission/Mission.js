import React from 'react'
import { Button, Row, Col, Checkbox, FormControl, InputGroup } from 'react-bootstrap'
import { connect } from 'react-redux'

import { changeTab, updateTroops, clearMissionConfig } from '../ActionTypes'
import * as api from '../../api/api'
import core from '../../api/core'
import TroopList from '../military/TroopList'
import SiegeInfo from './SiegeInfo'

let Mission = React.createClass({
	getInitialState(){
		return { 
			notEmpty: true, 
			enoughTroops: true, 
			action : "raid", 
			targets: [],
			resources: {}
		}
	},
	/*Troops count getter container*/
	get: {},
	
	errors(){
		return this.state.notEmpty || this.state.enoughTroops
	},
	startMission(e){
		const { dispatch, objective, location, mission } = this.props
		const loop = this.loop.checked

		let siege = Object.assign({}, this.state.siege)

		if ( this.state.action !== mission.attack && this.state.action !== mission.conquer ){
			siege = undefined
		}

		const trebuchets = (this.get.trebuchet && this.get.trebuchet()) || 0

		if ( siege ){
			if (trebuchets > 0 ){
				const { timeLeft, useTimeLeft, damageThreshold, useDamageThreshold } = siege.endCondition

				if ( useTimeLeft ) siege.timeLeft = timeLeft
				if ( useDamageThreshold ) siege.damageThreshold = damageThreshold

				delete siege.endCondition
			} else {
				siege = siege.targets
			}
		}

		let resources = undefined
		if ( this.state.action === mission.sendResources ){
			resources = this.state.resources
			const { resourcesNames } = this.props
			resourcesNames.forEach( r => resources[r] = resources[r] || 0)
		}

		var config = {
			troops: {},
			action: this.state.action,
			objective,
			path: [],
			loop,
			siege,
			resources
		}

		//Crea un objeto de solo x,y a partir de un objeto con x,y +otros properties
		var pathPoint = ({x, y}) =>({x , y})

		config.path.push(pathPoint(location))
		config.path.push(pathPoint(config.objective))
		
		const { troopsNames } = this.props

		troopsNames.forEach( t => 
			config.troops[t] = { count: this.get[t]() } 
		)

		console.log({ config })

		api.startMission( config ).then( response => {
			if ( ! response.enoughTroops ) {
				this.setState({ enoughTroops: false })
			} else {
				dispatch( updateTroops(response.troops) )
				dispatch( clearMissionConfig())
				dispatch( changeTab("troops") )
			}
		})
	},

	onActionChange(e){
		const { mission } = this.props
		let siege = undefined

		if ( e.target.value === mission.attack || e.target.value === mission.conquer ){
			siege = {
				endCondition: {},
				targets: []
			}
		}

		this.setState({ action: e.target.value, siege })
	},

	onSiegeChange(mods){
		const siege = Object.assign({}, this.state.siege, mods)
		this.setState({ siege })
	},

	resourceChange(name){
		const vResources = this.props.resources

		return e => {
			const count = parseInt(e.target.value,10)
			if ( vResources[name].r0 < count ){
				return
			} else if ( count < 0 ){
				return
			}

			const resource = { [name] : count }
			const resources = Object.assign({}, this.state.resources, resource)

			this.setState({ resources })
		}
	},

	render() {
		const { objective, mission, diplomacy, resourcesNames } = this.props

		console.log( { objective })

		if ( objective.owner === undefined ) return <div className="view"></div>

		const target = `(${objective.x} , ${objective.y}) ${objective.name} of ${objective.owner}`
		const subject = diplomacy.ruler && diplomacy.terms.findIndex( s => s.subject === objective.owner ) !== -1

		const action = (
			<InputGroup>
				<InputGroup.Addon>Action</InputGroup.Addon>
				<FormControl componentClass="select" value={this.state.action} onChange={this.onActionChange} >
					<option value={mission.raid}>Raid</option>
					<option value={mission.attack}>Attack</option>
					<option value={mission.conquer}>Conquer</option>
					<option value={mission.support}>Support</option>
					<option value={mission.sendResources}>Send resources</option>
					{ subject ? <option value={mission.collect}>Collect Tax</option> : "" }
				</FormControl>
			</InputGroup>
		)

		var troops = {}
		
		for( var i = 0; i < core.troopsNames.length; i++ ){
			var troop = core.troopsNames[i]
			troops[troop] = this.props.troops[troop]
		}


		const trebuchets = ( this.get.trebuchet && this.get.trebuchet() ) || 0
		const catapults = ( this.get.catapult && this.get.catapult() ) || 0
		const type = trebuchets > 0 ? 'Trebuchet' : ( catapults > 0 ? 'Catapult' : undefined)

		const showSiegeInfo = this.state.action === mission.attack || this.state.action === mission.conquer		
		let siegeInfo = ''
		if ( showSiegeInfo && type ){
			const targetList = ( this.state.siege && this.state.siege.targets ) || [] 

			siegeInfo = <SiegeInfo 
				onChange={this.onSiegeChange} 
				targets={targetList} 
				buildings={this.props.buildings}
				endCondition={this.state.siege.endCondition || {}}
				destroyAll={this.state.siege.destroyAll}
				type={type}
			/>
		}

		const showResources = this.state.action === mission.sendResources
		let resources = ''
		if ( showResources ){
			var img = `/icons/${name}.png`

			const resource = (name, value = 0) => <div key={name}>
				<InputGroup>
                    <InputGroup.Addon>
						<img src={'/icons/' + name + '.png'} className="ico" alt={name}/>
					</InputGroup.Addon>
                    <FormControl type="number" value={value} onChange={this.resourceChange(name)}/>
                </InputGroup>
			</div>
	
			resources = resourcesNames.map( 
				r => resource(r, this.state.resources[r])
			)
		}

		console.log({ showResources, resources })

		return (
			<div className="view">
				<Row>
					<Col xs={11} xsOffset={1}>
						<h3>Mission to {target}</h3>
					</Col>
				</Row>
				<Row>
					<Col xs={11} xsOffset={1} style={{ height: "90%" }}>
						<TroopList readOnly={false} defaults={troops} getters={this.get} group={"mission"} owners={[this.props.user]}/>
						{ this.state.enoughTroops ? "" : <span className='error mission-message'> You dont have enough troops </span> }
						{ this.state.notEmpty ? "" : <span className='error mission-message'> You must sent at least one unit </span> }
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						{action}
					</Col>
				</Row>
				<Row>
					<Col xs={12} >
						<Checkbox inputRef={ref => { this.loop = ref}} >Loop mission</Checkbox>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						{siegeInfo}
						{resources}
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<Button bsStyle="primary" onClick={this.startMission} block> Start </Button>
					</Col>
				</Row>
			</div>
		
		)
	}
})

export default connect(
	({ 
		military: { mission: { objective } },
		village: { troops, resources },
		map: { location },
		control: { 
			configuration,
			login: { user }
		},
		diplomacy
	}) =>({ 
		objective, 
		troops, 
		mission: configuration.mission,
		resourcesNames: configuration.resources,
		resources,
		location, 
		user, 
		diplomacy,
		buildings: configuration.buildingsNames,
		troopsNames: configuration.troops
	})
)(Mission)