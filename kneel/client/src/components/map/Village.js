import { OverlayTrigger, Popover, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'
import React from 'react'
import core from '../../api/core'

import VillageActions from './VillageActions'

const mapStateToProps = ({ 
	map: { marks },
	control: { login: { user } } 
}) => ({ marks, user })

export default connect( mapStateToProps )((props) => ({
	render(){
		const ally = this.props.cell.owner === this.props.user
		const marked = this.props.marks.hasOwnProperty(core.p2Str(this.props.cell))
		const { className, goto } = this.props
		const {x,y} = this.props.cell
		const menuId = `Menu-Village-(${x},${y})`

		const title = (
			<span>
				<span className="hidden-sm hidden-xs">
					<Glyphicon glyph="tower"/> {this.props.cell.name}
				</span>
				<br/>
				<span className="hidden-sm hidden-xs">
					<Glyphicon glyph="king"/> {this.props.cell.owner}
				</span>
			</span>
		)

		const menu = (
			<Popover id={menuId} title={title}>
				<VillageActions cell={this.props.cell} ally={ally} marked={marked} goto={goto}/>
			</Popover>
		)

		return (
			<OverlayTrigger trigger="focus" placement="right" overlay={menu}>
				<a href="#" className={className} tabIndex="0">
					{props.children}
				</a>
			</OverlayTrigger>
		)
	}
}))