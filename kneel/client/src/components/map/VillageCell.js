import React from 'react'
import Village from './Village'
import core from '../../api/core'

export default React.createClass({
    render() {
        const { cell, center, ally, radius } = this.props

		const cS = core.cellSize
        const middle = cS * radius 
        const rx = (cell.x - center.x)
        const ry = (cell.y - center.y)

        const cellStyle = {
			poisition: "absolute",
            top: (middle - ry * cS) + "%",
            left: (middle + rx * cS) + "%",
            width: cS  + "%",
            height: cS + "%",
			zIndex: ( this.props.radius - ry) + 1
        }

		cell.type = "village"

		const img = ! cell.raidable ? "/img/castle.png" : "/img/raid-town.png"

        const className = 'cell-content' + ( ally ? ' cell-ally' : '')

		return (
            <div className={className} style={cellStyle}>
                <Village cell={cell} className="focusCatcher">
					<img src={img} className='village-cell' alt={cell.villageName}/>
                </Village>
            </div>
		)
    }
})

