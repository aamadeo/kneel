import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, FormGroup, FormControl, ControlLabel, Button, Glyphicon } from 'react-bootstrap';
import core from '../../api/core'
import * as api from '../../api/api'
import * as actions from '../ActionTypes'
//import { subscribe, unsubscribe } from '../../api/events'

import Cell from './Cell'
import Coord from './Coord'
import VillageCell from './VillageCell'
import TroopCell from './TroopCell'
import ResponsiveBox from '../ResponsiveBox'
import Village from './Village'

const withinSection = (center, cell, radius ) => Math.abs( cell.x - center.x) <= radius &&
		  										 Math.abs( cell.y - center.y) <= radius

const sectionOf = ({ x, y }, radius) => ({
	x: Math.floor( (radius + x) / (radius*2 + 1)),
	y: Math.floor( (radius + y) / (radius*2 + 1))
})

const getRangeCenterAt = (center, radius) => ({
	left: center.x - radius,
	bottom: center.y - radius,
	right: center.x + radius,
	top:  center.y + radius
})

const sectionsInRange = (range, radius) => {
	const sections = []
	const alreadyInRange = new Set()

	for(let x = range.left ; x < range.right ; x++ ){
		for(let y = range.bottom ; y < range.top ; y++ ){
			let section = sectionOf({x,y} , radius)
			let sid = section.x + "," + section.y

			if ( alreadyInRange.has(sid) ) continue
			alreadyInRange.add(sid)

			sections.push(section)
		}  
	}

	return sections
}

const distanceBetween = ( p1, p2 ) => {
	const d = Math.sqrt((Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y,2)),2)

	return d
}

let Explorer = React.createClass({
	componentDidMount() {
		const { location, radius } = this.props
		const mainSections = new Set()

		const sections = sectionsInRange( getRangeCenterAt(location, radius), radius )
		
		sections.forEach( ({x, y}) => {
			//subscribe(`map-section:${x}:${y}`)
			mainSections.add(`${x}:${y}`)
		})

		this.setState({ mainSections })
	},

	getInitialState(){
		return { x: this.props.location.x, y: this.props.location.y }
	},

	renderCell(x,y, occupied){
		const { radius } = this.props
		return <Cell x={x} y={y} key={x + ":" + y} center={this.state} occupied={occupied} radius={radius}/>
	},

	renderCoord(x,y,horizontal){
		const { radius } = this.props
		return <Coord x={x} y={y} key={x + "," + y} center={this.state} horizontal={horizontal} radius={radius}/>
	},

	componentWillUpdate(nextProps){
		const oldLocation = this.props.location
		const newLocation = nextProps.location

		if ( oldLocation.x !== newLocation.x || oldLocation.y !== newLocation.y ){
			const { x, y } = newLocation
			this.setState({ x, y })
		}
	},

	renderCellContent(cell){
		const { radius, allies, user } = this.props
		const ally = allies.findIndex( a => a === cell.owner ) !== -1

		const props = {
			x: cell.x,
			y: cell.y,
			key: `${cell.x}:${cell.y}:${cell.type}:${cell.id}`,
			center: this.state,
			radius,
			ally,
			own: cell.owner === user,
			cell
		}

		if ( cell.type === "village" ) {
			const marked = this.props.marks.hasOwnProperty(core.p2Str(cell))
			return <VillageCell {...props} marked={marked}/>
		}

		if( cell.type === "troops" ){
			return <TroopCell {...props}/>
		}

		return null
	},

	findPoint: {
		x: 0,
		y: 0
	},

	onChangeGoto(e){
		this.findPoint[e.target.id] = e.target.value
	},

	loadCells(p){
		const { dispatch } = this.props
		
		api.loadMap(p).then( data => {
			dispatch( actions.updateMap(data))
		})
	},

	goto(p){
		const { radius } = this.props
		if ( typeof p === "undefined" ){
			p = { x: parseInt(this.findPoint.x,10), y: parseInt(this.findPoint.y, 10) }
		}

		//const { subscribe, unsubscribe } = this.props
		const { x, y, mainSections } = this.state
		const newSection = sectionOf(p, radius)
		const oldSection = sectionOf(({x,y}), radius)

		if ( ! mainSections.has(`${newSection.x}:${newSection.y}`)){
			if ( ! mainSections.has(`${oldSection.x}:${oldSection.y}`) ) {
				console.log("Unsubscribing", { oldSection })
				//unsubscribe(`map-section:${oldSection.x}${oldSection.y}`)
			}

			console.log("Subscribing", { newSection })
			//subscribe(`map-section:${newSection.x}:${newSection.y}`)
		}

		for(let dx = -1 ; dx <= 1; dx += 2){
			for(let dy = -1; dy <= 1; dy += 2){
				let section = sectionOf({ x: p.x + dx, y: p.y + dy })

				if ( ! mainSections.has(`${section.x}:${section.y}`)){
					this.loadCells(p)

					//break both loops
					dx = 3
					dy = 3
				}
			}
		}

		this.setState(Object.assign({}, p))
	},

	down(e){
		let {x,y} = this.state
		y -= 1
		this.setState({ x,y })
		this.goto({ x,y })
	},

	right(e){
		let {x,y} = this.state
		x += 1
		this.setState({ x,y })
		this.goto({ x,y })
	},

	left(e){
		let {x,y} = this.state
		x -= 1
		this.setState({ x,y })
		this.goto({ x,y })
	},

	up(e){
		let {x,y} = this.state
		y += 1
		this.setState({ x,y })
		this.goto({ x,y })
	},

	//Crear componente Village
	renderMark(mark){
		const cell = this.props.marks[mark]

		const village = ( 
			<Village cell={cell} className="k-link" goto="true">
				<span>[{cell.x},{cell.y}] {cell.name} of {cell.owner}</span>
			</Village>
		)


		return (<li key={mark}>{village}</li>)
	},

	render() {
	  var cells = []
	  var cellContents = []
	  const center = this.state
	  const occupiedCells = new Map()
	  const { location, radius } = this.props

	  this.props.cells.forEach((cell) => {
		  if ( ! withinSection(center, cell, radius) ) return
		  
		  occupiedCells.set(`${cell.x},${cell.y}`, cell)
		  cellContents.push(this.renderCellContent(cell))
	  })

	  for ( let f = 0 ; f < radius *2 + 1 ; f++ ){
		  cells.push(this.renderCoord( f,11, true))
		  cells.push(this.renderCoord( 11, f, false))
	  }

	  //console.log(this.state)

	  for( let x = center.x - radius; x <= center.x + radius; x++){
			for( let y = center.y - radius; y <= center.y + radius; y++){
				cells.push(this.renderCell(x,y, occupiedCells.has(`${x},${y}`)))
			}
	  }

	  const arrowStyle = { fontSize: "4vw" }

	  let marks = []

	  for ( let mark in this.props.marks ){
		  marks.push(mark)
	  }

	  marks = marks.sort( (m1, m2) => {
		  const cell1 = this.props.marks[m1]
		  const d1 = -distanceBetween( location, cell1 )

		  const cell2 = this.props.marks[m2]
		  const d2 = -distanceBetween( location, cell2 )

		  return d2 - d1
	  })

	  marks = marks.map( mark => this.renderMark(mark) )

	  return (
			<div className="view">
				<Row>
					<Col xs={12}>
						<ResponsiveBox width={70} whRatio={90} prefix="map">
							<div className="map">
								{cells}
							</div>
							<div className="map-content">
								{cellContents}
							</div>
						</ResponsiveBox>
					</Col>
				</Row>
				<Row>
					<br/>
				</Row>
				<Row>
					<Col xs={1} xsOffset={1}>
						<i className="fa fa-arrow-up" style={arrowStyle} onClick={this.up} />
					</Col>
				</Row>
				<Row>
					<Col xs={1}>
						<i className="fa fa-arrow-left" style={arrowStyle}  onClick={this.left}/>
					</Col>
					<Col xs={1} xsOffset={1}>
						<i className="fa fa-arrow-right" style={arrowStyle}  onClick={this.right}/>
					</Col>
				</Row>
				<Row>
					<Col xs={1} xsOffset={1}>
						<i className="fa fa-arrow-down" style={arrowStyle}  onClick={this.down}/>
					</Col>
				</Row>
				<Row>
					<Col xs={12} >
						<FormGroup>
							<Col componentClass={ControlLabel} xs={1}>x</Col>
							<Col xs={3}>
								<FormControl type="number" id="x" onChange={this.onChangeGoto}/>
							</Col>
							<Col componentClass={ControlLabel} xs={1}>y</Col>
							<Col xs={3}>
								<FormControl type="number" id="y" onChange={this.onChangeGoto}/>
							</Col>
							<Col xs={2}>
								<Button bsStyle="success" onClick={() => this.goto()}>Go to</Button>
							</Col>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col xs={12} >
						<a href="#" onClick={() => this.goto(location)}> 
							<span className="hidden-xs"> Home </span> 
							<Glyphicon glyph='home' />
						</a>
						<ul>{marks}</ul>
					</Col>
				</Row>
			</div>
	  )
	}
})

const mapStateToProps = ({ 
	map: { cells, location, marks },
	control, diplomacy 
}) => ({ 
//	subscribe: subscription => subscribe(control.socket, subscription),
//	unsubscribe: subscription => unsubscribe(control.socket, subscription),
	cells,
	location,
	marks, 
	update: control.tab.view === "map",
	radius: control.configuration.radius,
	user: control.login.user,
	allies: diplomacy.allies
})

export default connect(mapStateToProps)(Explorer)