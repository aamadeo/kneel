import React from 'react'
import core from '../../api/core'

const Cell = (props) => ({
    render() {
        var cS = core.cellSize
        var rx = this.props.x
        var ry = this.props.horizontal ? this.props.radius*2 + 1 : this.props.y
        
        var cellStyle = {
            position: "absolute",
            top: (ry * cS) + "%",
            left: (rx * cS) + "%",
            width: cS  + "%",
            height: cS + "%",
            fontSize: "2vw",
            textAlign: "center"
        }

        let coord = this.props.horizontal ? this.props.x + this.props.center.x - this.props.radius : this.props.center.y - this.props.y + this.props.radius

        return <div style={cellStyle}>{coord}</div>
    }
})

export default Cell