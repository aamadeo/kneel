import React from 'react'
import { Nav, NavItem, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'

import { prepareMission, changeTab, updateLocation, updateMap, mark, unmark } from '../ActionTypes'
import { loadMap, markLocation, unmarkLocation } from '../../api/api'

let VillageActions = React.createClass({
    render() {
		const goto = this.props.goto ? (
			<NavItem key="goto" eventKey="goto">
				<Glyphicon glyph="globe"/><span className="hidden-sm hidden-xs"> See in map </span>
			</NavItem>
		) : null

		const std = [
			<NavItem key="view" eventKey="view">
				<Glyphicon glyph="eye-open"/><span className="hidden-sm hidden-xs"> View </span>
			</NavItem>,
			<NavItem key="troops" eventKey="troops">
				<Glyphicon glyph="knight"/><span className="hidden-sm hidden-xs"> Send troops </span>
			</NavItem>
		]

		const mark = this.props.marked ? (
			<NavItem key="unmark" eventKey="unmark">
				<Glyphicon glyph="remove-circle"/><span className="hidden-sm hidden-xs"> Remove mark </span>
			</NavItem>
		) : (
			<NavItem key="mark" eventKey="mark">
				<Glyphicon glyph="ok-circle"/><span className="hidden-sm hidden-xs"> Add mark </span>
			</NavItem>
		)

		return (
			<Nav stacked bsStyle="pills" onSelect={this.handleSelect}>
				{[ ...std, mark, goto ]}
			</Nav>
		)
    },
	goto({ x, y }){
		const dispatch = this.props.dispatch

		loadMap({ x, y }).then(
			cells => {
				dispatch( updateLocation({x, y}) )
				dispatch( updateMap(cells) )
				dispatch( changeTab("map"))
			}
		)
		
	},
	view(cell){
		console.log("View: ", cell)
	},
	toggleMark(){
		const dispatch = this.props.dispatch
		const {cell} = this.props

		if ( this.props.marked ){	
			unmarkLocation(cell).then( location => dispatch( unmark(location) ) )
		} else {
			markLocation(cell).then( location => {
				dispatch( mark(location) ) 
			})
		}
	},
    handleSelect(key) {
      console.log(`${key} (${this.props.cell.x},${this.props.cell.y}) `)

		if ( this.hasOwnProperty(key) ) {
			this[key](this.props.cell)
		} else if ( key === "mark" || key === "unmark") {
			this.toggleMark()
		} else {
			this.props.dispatch(prepareMission(this.props.cell))
		}
    }
})

export default connect()(VillageActions)