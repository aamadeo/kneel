import React from 'react'
import { OverlayTrigger, Popover, Glyphicon } from 'react-bootstrap'
import core from '../../api/core'

export default (props) => ({
    render() {
		var cS = core.cellSize
        var center = cS * this.props.radius 
        var rx = (this.props.cell.x - this.props.center.x)
        var ry = (this.props.cell.y - this.props.center.y)
        
        var cellStyle = {
			poisition: "absolute",
            top: (center - ry * cS) + "%",
            left: (center + rx * cS) + "%",
            width: cS  + "%",
			zIndex: ( this.props.radius - ry) + 2
        }
		
		var cell = this.props.cell

		console.log( { cell })

		var title = (
			<span>
				<span className="hidden-xs">
					<Glyphicon glyph="knight"/> {cell.id}
				</span>

				<br/>
				<span className="hidden-xs">
					<Glyphicon glyph="screenshot"/> ({cell.objective.x},{cell.objective.y})
				</span>
				
				<br/>
				<span className="hidden-xs">
					<Glyphicon glyph="king"/> {cell.user}
				</span>
				<br/>
				{ cell.loot ? (
						
						<span className="hidden-xs">
							<Glyphicon glyph="grain"/>
						</span> 
				) : ""}
			</span>
		)

		const { avatar } = this.props.cell

		var img = avatar ? `/img/upload/${avatar}` : '/img/troops.png'

		var menuId = `menu-cell-(${rx},${ry})`
		var menu = (
			<Popover id={menuId} title={title}>
				
			</Popover>
		)

		return (
            <div className='cell-content' style={cellStyle}>
                <OverlayTrigger trigger="focus" placement="right" overlay={menu}>
                    <a href="#" className="focusCatcher" tabIndex="0">
						<img src={img} className='troop-cell' alt={cell.user}/>
					</a>
                </OverlayTrigger>
            </div>
		)
    }
})