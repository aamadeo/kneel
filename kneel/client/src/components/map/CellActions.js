import React from 'react'
import { Nav, NavItem, Glyphicon } from 'react-bootstrap'

export default React.createClass({
    render() {
        return (
			<Nav stacked bsStyle="pills" onSelect={this.handleSelect}>
				<NavItem eventKey="set">
					<Glyphicon glyph="flag"/><span className="hidden-sm hidden-xs"> Set Village </span>
				</NavItem>
			</Nav>
        )
    },
    handleSelect(key) {
        console.log(`Setting village in (${this.props.x},${this.props.y}) `)
    }
});