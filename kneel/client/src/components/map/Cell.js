import React from 'react'
import { OverlayTrigger, Popover } from 'react-bootstrap'
import CellActions from './CellActions'
import core from '../../api/core'

const Cell = (props) => ({
    render() {
        var cS = core.cellSize
        var center = cS * this.props.radius 
        var rx = (this.props.x - this.props.center.x) 
        var ry = (this.props.y - this.props.center.y)

        var cellStyle = {
            top: (center - ry * cS) + "%",
            left: (center + rx * cS) + "%",
            width: cS  + "%",
            height: cS + "%"
        }

        var menu = ""

        if ( ! this.props.occupied ) {
            var menuId = `menu-cell-(${rx},${ry})`
            var title = `(${this.props.x},${this.props.y})`
            menu = (
                <Popover id={menuId} title={title}>
                    <CellActions {...this.props}/>
                </Popover>
            )

            return (
                <div className="cell" style={cellStyle}>
                    <OverlayTrigger trigger="focus" placement="right" overlay={menu}>
                        <a href="#" className="focusCatcher" tabIndex="0"></a>
                    </OverlayTrigger>
                </div>
            )  
        }

        return (
            <div className="cell" style={cellStyle}></div>
        )
    }
})

export default Cell