import React from 'react'
import { Col, Thumbnail } from 'react-bootstrap'

export default class extends React.Component {
	render() {
		let { damage } = this.props

		damage = damage.filter( d => d.damage > 0 )

		damage = damage.map ( (d, index) => {
			const src = `/img/${d.name}.png`
			const destroyed = d.detail.level === -1

			return <Col  key={index} xs={2}>

				<Thumbnail src={src}>
					<h5>{d.name}</h5>
					<span className="">Previous Level: {d.detail.level + d.damage}</span>
					<br/>
					<span className="siege-damage">Damage: {-d.damage}</span>
					<br/>
					{ destroyed ? <span className="siege-damage"> Destroyed </span> : ""}
				</Thumbnail>
			</Col>
		})

		const style = { overflowX: 'scroll' }

		return <div style={style}>
			<h4>Siege Damage</h4>
			{damage}
		</div>
	}
}