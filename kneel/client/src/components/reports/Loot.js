import React from 'react'
import { Col } from 'react-bootstrap'

export default React.createClass({ 
	render() {
		
		var resources = ["food","iron","stone","wood"].map((r)=>{
			var img = `/icons/${r}.png`

			return (
				<Col xs={2} key={r}>
					<Col xs={6}>
						<img alt={r} src={img} className="ico" />
					</Col>
					<Col xs={6} style={{ fontSize: "2vh" }}>
						<span>{ Math.round(this.props.loot[r] || 0) }</span>
					</Col>
				</Col>
			)
		})
		
		return (
			<div className="loot">
				<Col xs={1} style={{ fontSize: "2vh" }}> Loot </Col>
				{resources}
			</div>
		)
	}
})