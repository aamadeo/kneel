import React from 'react'
import TroopList from '../military/TroopList'
import Loot from './Loot'
import SiegeDamage from './SiegeDamage'
import { Panel } from 'react-bootstrap'
import core from '../../api/core'

export default React.createClass({
	parse(group,name){ 
		var detail = this.props.report
		var original = detail.troops[group].original[name]
		var after = detail.troops[group].after[name]
		
		return {
			count: original !== null ? original.count : 0,
			deaths: original !== null ? original.count - after.count : 0
		}
	},
	render() {
		var attack = {}
		core.troopsNames.forEach((name)=>{
			attack[name] = this.parse("attack", name)
		})
		
		var defense = {}
		core.troopsNames.forEach((name)=>{
			defense[name] = this.parse("defense", name)
		})
		
		console.log( "Report", this.props.report)

		const { siegeDamage, loot, village } = this.props.report

		const header = `Attack on (${village.x},${village.y}) ${village.name} of ${village.owner}`

		return (
			<Panel collapsible header={header} bsStyle="primary">
			<div className="report">
				<TroopList readOnly={true} defaults={attack} group={"report"} owners={[this.props.report.attack[0]]} casualties={true}/>
				<h3>Defense</h3>
				<TroopList readOnly={true} defaults={defense} group={"report"} owners={[this.props.report.defense[0]]} casualties={true}/>
				{ loot ? <Loot loot={loot} width="100%"/> : null}
				{ siegeDamage ? <SiegeDamage damage={siegeDamage} width="100%"/> : null}
			</div>
			</Panel>
		)
	}
})