import React from 'react'
import { connect } from 'react-redux'
import Report from './Report'

let Reports = (props) => ({
	render() {
		var reports = this.props.reports.map((x) => {
			var detail = x.detail
			
			return <Report key={x.mission} report={detail} user={this.props.login.user}/>
		})

		return <div className="view">
			<p>REPORTS VIEJA</p>
			{ reports }
		</div>
	}
})

export default connect( 
	({ military: { reports }, control: { login } }) => ({ reports, login })
)(Reports)