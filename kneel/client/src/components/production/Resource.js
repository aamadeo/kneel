import React from 'react'
import { ProgressBar, Tooltip, OverlayTrigger } from 'react-bootstrap'
import core from '../../api/core'

export default React.createClass({
    getInitialState(){
        return { t: new Date().getTime() }
    },

    update(){
        this.setState({ t: new Date().getTime() })
    },

    componentDidMount: function() {
        var intervalId = setInterval(this.update, 1000)
        this.setState({ intervalId })
    },

    componentWillUnmount: function() {
        clearInterval(this.state.intervalId)
    },
    
    render() {
        const r = this.props.data
        const o = this.props.occupation

        let available = core.available(r)
        let percentage = ( available / r.capacity )

        let name = r.resource

        var img = `/icons/${name}.png`

        var level = "info"
        var striped = false

        if ( percentage === 1.00 ){
            level = "success"
            striped = true
        } else if ( percentage < 0.30 ){
            level = "danger"
        } else if ( percentage < 0.60 ) {
            level = "warning"
        }

        var ttid = "tt" + name
        var rLevel = o.level
        const tooltip = (
  			<Tooltip id={ttid}>{Math.round(r.rps*3600) + "rph Lv:" + rLevel}</Tooltip>
		)

        return (
        <div className="resource" >
            <span className="hidden-xs">{ Math.floor(available) } / {r.capacity}</span>
            <img src={img} className="ico" alt={name}/>
            <div style={{ width: "100%"}}>
                <OverlayTrigger placement="bottom" overlay={tooltip}>
                    <ProgressBar bsStyle={level} now={100 * percentage} striped={striped} />
                </OverlayTrigger>
            </div>
        </div>
        )
    }
})