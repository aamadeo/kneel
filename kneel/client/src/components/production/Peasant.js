import React from 'react'

import { ProgressBar, Col, ButtonGroup, Button, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'

import * as api from '../../api/api'
import { updateVillage } from '../ActionTypes'

let Peasant = React.createClass ({
    moveWorker(occupation, workers)  {
        if ( this.props.name === "unoccupied" || this.props.name === "builders" ) return

        if ( workers < 0 ) return

        api.moveWorker(occupation, workers).then( summary => {
           if ( summary.errorCode ) return console.error( summary.errorCode )

            this.props.dispatch(updateVillage(summary))
        })
    },
    render() {
        var o = this.props.data
        let editable = this.props.name !== "unoccupied" && this.props.name !== "builders"

        o.percentage = o.workers / o.total

        var img = `/img/${o.occupation}-occupation.png`
		var size = "35px"

        const moveWorker = this.moveWorker
        
        const more = () => {
            moveWorker(o.occupation, o.workers +1)
        }
        
        const less = () => {
            moveWorker(o.occupation, o.workers -1)
        }

        return (
            <div className="occupation">
                { ! editable ? "" :
                <Col xs={1}>
                   <ButtonGroup vertical>
                        <Button bsSize="xsmall" bsStyle="success" onClick={more}>
                            <Glyphicon glyph='plus'/>
                        </Button>
                        <Button bsSize="xsmall" bsStyle="danger" onClick={less}>
                            <Glyphicon glyph='minus'/>
                        </Button>
                    </ButtonGroup>
                </Col>
                }
                <Col xs={5}>
                    <span style={{ fontSize: '2vh' }}>{o.workers}</span>
                    <img alt={o.occupation} src={img} className="ico" style={{ width: size, height: size }}/>
                    <ProgressBar bsStyle="info" now={100 * o.percentage} />
                </Col>
            </div>
        )
    }
})

export default (props) => ({
    render(){
        const CustomPeasant = connect(
            ({ village: { occupations }}) => ({
                data: occupations[props.name]
            })
        ) (Peasant)

        return <CustomPeasant {...props}/>
    }
})