import React from 'react'
import Peasants from './Peasants'
import { FormControl, ControlLabel, Row, Col } from 'react-bootstrap'
import { connect } from 'react-redux'

import * as actions from '../ActionTypes'

export default  connect(({ control: { builders } }) => ({ builders }) )( React.createClass ({
    onChange(e){
        this.props.dispatch(actions.updateBuilders(e.target.value))
    },
    render() {
        return (
            <div className="peasants view">
                <Row>
                    <Peasants />
                </Row>
                <Row>
                    <Col  xs={5}>
                        <ControlLabel> Peasants per building job </ControlLabel>
                    </Col>
                    <Col xs={5}>
                        <FormControl min="1" type="number" id="ppb" onChange={this.onChange} value={this.props.builders}/>
                    </Col>
                </Row>
            </div>
        )
    }
}))