import React from 'react'
import Peasant from './Peasant'


export default () => ({
    render() {
        const occupations = ["food","iron","stone","wood","builders","unoccupied"]
        var peasants = occupations.map( (name) => {
            return (<Peasant name={name} key={"occupation-" + name} />)
        })

        return ( 
            <div className="peasants">
                <h3>Peasants</h3>
                {peasants}
            </div>
        )
    }
})