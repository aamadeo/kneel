import React from 'react'

export default React.createClass ({
	render() {
		const { title, cost } = this.props

		return (
			<div className="bm-cost">
				<div style={{ clear: "both" }}>{title}</div>
				<span className="bm-cost-r" style={{ clear: "left" }}> Cost: </span>
				<span className="bm-cost-r">{ Math.round(cost.food) }<img className="cost-ico" src="/icons/food.png" alt='food'/></span>
				<span className="bm-cost-r">{ Math.round(cost.iron) }<img className="cost-ico" src="/icons/iron.png" alt='food'/></span>
				<span className="bm-cost-r">{ Math.round(cost.stone) }<img className="cost-ico" src="/icons/stone.png" alt='food'/></span>  
				<span className="bm-cost-r">{ Math.round(cost.wood) } <img className="cost-ico" src="/icons/wood.png" alt='food'/></span>  
				<span className="bm-cost-r">{ Math.round(cost.time) }mins <img className="cost-ico" src="/icons/time.gif" alt='time'/></span>  
			</div>
		)
	}
})