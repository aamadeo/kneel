import React from 'react'
import Resource from './Resource'

import {Row, Col} from 'react-bootstrap'
import { connect } from 'react-redux'

 let Resources = (props) => ({
    render() {
        // console.log("Rendering resources")
        var resources = ["food","iron","stone","wood"]

        resources = resources.map( (name) => {
            const resource = this.props.resources[name]
            const occupation = this.props.occupations[name]

            return (
                <Col lg={3} md={3} sm={3} xs={3} key={name}>
                    <Resource data={resource} occupation={occupation}/>
                </Col>
            )
        })

        return ( 
            <Row>
                {resources}
            </Row>
        )
    }
})

export default connect(
    ({ village: { resources, occupations }}) => ({ resources, occupations })
)(Resources)