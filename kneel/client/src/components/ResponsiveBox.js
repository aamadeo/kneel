import React from 'react'

export default React.createClass({
    propTypes:{
        prefix: React.PropTypes.string.isRequired,
        width: React.PropTypes.number.isRequired,
        whRatio: React.PropTypes.number.isRequired
    },
	render() {

		var inner = this.props.prefix + "-inner"
		var wrapper = this.props.prefix + "-wrapper"
		var styleBody = `
			.${wrapper} {
				width: ${this.props.width}%;
				display: inline-block;
				position: relative;
			}
			.${wrapper}:after {
				padding-top: ${this.props.whRatio}%;
				display: block;
				content: '';
			}
			.${inner} {
				position: absolute;
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
			}
		`
		return (
			<div key="div" className={wrapper}>
				<style key="style">{styleBody}</style>

				<div className={inner}>
					{this.props.children}
				</div>
			</div>
		)
	}
})