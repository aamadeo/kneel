import React from 'react'
import TimerMixin from 'react-timer-mixin'
import { ProgressBar, Tooltip, OverlayTrigger } from 'react-bootstrap'
import { connect } from 'react-redux'

const Task = React.createClass({
	once: 0,
	mixins: [TimerMixin],
	
	getInitialState(){
		return { t: new Date().getTime() }
	},

	update(){
		this.setState({ t: new Date().getTime() })
	},

	componentDidMount: function() {
		var intervalId = setInterval(this.update, 1000)
		this.setState({ intervalId })
	},

	componentWillUnmount: function() {
		clearInterval(this.state.intervalId)
	},

	formatTime( time ){
		time = new Date(time * 1000)

		var f = {
			h: (time.getUTCDate()-1)*24+ time.getUTCHours(),
			m: time.getUTCMinutes(),
			s: time.getUTCSeconds()
		}

		function digit(s){
			s = "00" + s
			return s.slice(-2)
		}

		f.h = digit(f.h)
		f.m = digit(f.m)
		f.s = digit(f.s)

		return f.h + ":" + f.m + ":" + f.s
	},
	
	renderIcon( params, index ){
		let src = ""
		let title = ''

		if ( params.job === "spawnUnit" ){
			src = `/img/${params.type}.png`
		} else if ( params.job === "build" ){
			src = `/img/${params.building}.png`
		} else if ( params.job === "upgradeBuilding" ){
			console.log("params", params)
			src = `/img/upgrade.png`
		} else if ( params.job === "upgradeTech" ){
			src = `/img/${params.occupation}-occupation.png`
		} else if ( params.job === "spawnArmyUnit"){
			const { building } = this.props
			title = params.units.map( 
				unit => (unit.troop + "("+unit.count+") ")
			).reduce( 
				(a, b) => a + b 
			)

			const group = building === 'Barracks' ? 'GroupB' 
			           : ( building === 'Archery' ? 'GroupA' 
				       : ( building === 'Stable'  ? 'GroupC' : 'GroupS'))

			src = `/img/${group}.png`
		} else if ( params.job === "enableTech"){
			src = `/img/${params.icon}.png`
		}

		return (
			<div className="task-icon-container">
				<img className="task-icon" src={src} alt={title} title={title} />
			</div>
		)
	},
	render() {
		var remaining = (this.props.task.remaining - (this.state.t - this.props.task.t0) / 1000 )

		if ( remaining < 0) {
			console.log("Task Done")
			return <div />
		}

		var end = new Date(this.state.t + remaining * 1000)
		var progress = 1 - remaining / this.props.task.duration

		var level = "warning"

        if ( progress >= 0.90 ){
            level = "danger"
        } else if ( progress < 0.30 ){
            level = "info"
        } else if ( progress < 0.60 ) {
            level = "success"
        }

		var tooltip = (
			<Tooltip id="tooltip">
				Ends : {end.toLocaleDateString() + " " + end.toLocaleTimeString()}
			</Tooltip>
		)

		var task = (
			<div className="task">
				{this.renderIcon(this.props.task.params)}
				<span className="task-info">{this.formatTime(remaining)}</span>
				<span className="task-progress">
					<ProgressBar bsStyle={level} now={Math.min(100, 100 * progress)} active />
				</span>
			</div>
		)
		
		return (
			<OverlayTrigger placement="top" overlay={tooltip}>
				{task}
			</OverlayTrigger>
		)
	}
})

export default connect()(Task)