import React from 'react'
import Task from './Task'
import { Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'

let Tasks = React.createClass({
	getInitialState(){
		return { collapsed : [] }
	},

	renderIcon( name, type, index ){
		var src =  `/img/${name}.png`

		var className = type + "-icon-container"
		return (
			<div className={className} key={ index !== null ? "pending." + index : "main"}>
				<img className="task-icon" src={src} alt={name} />
				{ index !== null ? <div className='task-index'>{index}</div> : ""}
			</div>
		)
	},

	toggleCollapse(index){ // [!!!]
		
	},
	
	renderSlot(tasks, building) {
		if ( tasks === null ) return null;

		let active = tasks[0]

		if ( active !== undefined ){
			active = <Task key={active.id} task={active} refresh={this.loadTasks} building={building}/>
		}

		var inactives = tasks.map((task) => {
			if (task.index === 0) return null

			if ( task.index === 10 ){
				return (
					<div className="inactive-icon-container inactive-last" key={"pending.last"}>
						<Glyphicon glyph="option-horizontal"/>
					</div>
				)
			} else if ( task.index > 10 ){
				return null
			}

			var params = task.params
			var name = ""

			if ( params.job === "spawnUnit" ){
				name = params.type
			} else if ( params.job === "build" ){
				name = params.building
			} else if ( params.job === "upgradeBuilding" ){
				name = 'upgrade'
			} else if ( params.job === "upgradeTech"){
				name = params.occupation + "-occupation"
			} else if ( params.job === "spawnArmyUnit" ){
				name = building === 'Barracks' ? 'GroupB' 
				   : ( building === 'Archery'  ? 'GroupA' 
					 : ( building === 'Stable'   ? 'GroupC' : 'GroupS'))
			}

			return this.renderIcon(name, "inactive", task.index)
		})

		return (
			<div className="slot-tasks-content">
				{active}
				<div className="slot-pending-tasks">
					{inactives}
				</div>
			</div>
		);
	},
	render(){

		const buildings = this.props.buildings

		const slots = this.props.slots.map(slot=>{
			if ( ! slot || slot.length === 0 ) return null

			var slotIdx = slot[0].params.slot

			var name = "Empty Lot"
			if ( buildings[slotIdx] !== null ){
				name = buildings[slotIdx].name
			} else {
				name = "builders-occupation"
			}

			return (
				<div key={"slot." + slotIdx} className='slot-tasks'>
					{this.renderIcon(name,"slot")}
					{this.renderSlot(slot, name)}
				</div>
			)
		})

		return (
			<div className="view">
				<p>TASKS VIEJA</p>
					{ slots }
			</div>
		)
	}
})

export default connect(
	({ tasks, village: { buildings }}) => ({ slots: tasks, buildings }) 
)(Tasks)