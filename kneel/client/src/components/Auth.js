//Combinar con auth.js
import { loginSuccess, loginError, updateVillage, updateDiplomacy, paramsReceived } from './ActionTypes'
//setSocket
import * as api from '../api/api'
//import * as events from '../api/events'

export default {
	login(dispatch, user, pass) {
		api.login(user, pass).then(
			data => {
				if ( ! data ) return
				const {authenticated} = data

				const userInfo = Object.assign({}, data)
				if ( authenticated ){
					console.log({ userInfo })
					dispatch( loginSuccess(userInfo) )

					api.loadVillage().then( village => {
						dispatch(updateVillage(village))
					})

					api.loadParams().then( params => {
						dispatch(paramsReceived(params))
					})

					api.loadDiplomacy().then( diplomacy => {
						dispatch(updateDiplomacy(diplomacy))
					})

					//const wsURL = 'https://' + api.apiServer + "/websockets/"
					//console.log({ wsURL })
					//const socket = window.io.connect(wsURL)
					//events.init(socket, dispatch)
					//dispatch(setSocket(socket))
				} else {
					if ( dispatch && user !== null) dispatch( loginError )
				}
			}, ( err ) => {
				console.log(err)
			}
		)	
	}
}