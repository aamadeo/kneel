import React from 'react'
import { connect } from 'react-redux'
import { PageHeader, Popover, OverlayTrigger, Alert } from 'react-bootstrap'
import VillageActions from './map/VillageActions'

import * as api from '../api/api'
import * as actions from './ActionTypes'

const mapStateToProps =  ({ 
	userInfo,
	control: { 
		loaded, login: { user } 
	}
}) => ({ userInfo, loaded, user })

export default connect (mapStateToProps)( React.createClass ({
	componentWillMount(){
		const dispatch = this.props.dispatch
		api.loadUserInfo( this.props.params.user ).then( userInfo => {
			dispatch( actions.updateUserInfo(userInfo) )
		})
	},

	render(){
		const { loaded, userInfo } = this.props

		console.log({ loaded, userInfo })

		if ( ! loaded.userInfo ) return <Alert bsStyle="info">Loading...</Alert>

		console.log("userInfo", userInfo)

		const { user, points, villages } = userInfo
		const ally = user === userInfo.user

		if ( typeof userInfo.user === "undefined" ) return <div>No user selected</div>

		const villageList = villages.map( v => {
			const menu = (
				<Popover id={v.id} title={v.name}>
					<VillageActions cell={v} ally={ally} goto={true}/>
				</Popover>
			)

			v.owner = v.user
			v.type = "village"

			return (
				<OverlayTrigger trigger="focus" placement="right" overlay={menu} key={v.id}>
					<li className="k-link">
						<a href="#" tabIndex="0">
							<span>{v.name} ({v.x},{v.y})</span>
						</a>
					</li>
				</OverlayTrigger>
			)
		})
		
		return (
			<div className="view">
				<PageHeader>{user}</PageHeader>
				<h2>{points} points</h2>
				<h2>Villages</h2>
				<ul>
					{villageList}
				</ul>
			</div>
		)
	}
}))