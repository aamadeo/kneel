import React from 'react'
import { connect } from 'react-redux'

import * as api from '../api/api'
import * as actions from './ActionTypes'

import Production from './production/Production'
import Resources from './production/Resources'
import Village from './village/Village'
import MultiSpawn from './village/MultiSpawn'
import Troops from './military/Troops'
import Reports from './reports/Reports'
import Explorer from './map/Explorer'
import Mission from './mission/Mission'
import Tasks from './tasks/Tasks'
import Market from './market/Market'
import Ranking from './ranking/Ranking'
import Diplomacy from './diplomacy/Diplomacy'
import SideBarNav from './SideBarNav'


import {Row, Col, Tab, Alert} from 'react-bootstrap'

const easyUpdate = new Set()
easyUpdate.add("village")
easyUpdate.add("tasks")
easyUpdate.add("map")
easyUpdate.add("reports")
easyUpdate.add("ranking")
easyUpdate.add("diplomacy")

const mapStateToProps = ({ control: { tab, loaded } }) => ({ tab, loaded })

export default connect( mapStateToProps) (React.createClass ({

	refresh(object, changeView){
		const refresh = "load" + object.charAt(0).toUpperCase() + object.substring(1)
		const update = "update" + object.charAt(0).toUpperCase() + object.substring(1)
		const dispatch = this.props.dispatch

		if ( ! api.hasOwnProperty(refresh) || ! actions.hasOwnProperty(update)) return

		api[refresh]().then( data => {
			console.log("API::response = ", { data })
			dispatch( actions[update](data))

			if ( changeView ){
				dispatch( actions.changeTab(object) )
			}
		})
	},

	handleSelect(key){
		const { tab: { view }, dispatch } = this.props

		if ( key === view ){
			this.refresh(key)

			return
		}

		if ( key === "troops" ) {
			api.loadMissions().then(
				missions => {
					console.log("Missions", missions)
					dispatch( actions.updateMissions(missions) )
					dispatch( actions.changeTab(key) )
				}
			)
		} if ( easyUpdate.has(key) ) {
			this.refresh(key, true)

		} else {
			dispatch( actions.changeTab(key) )
		}
	},

	render() {
		const { tab: { view }, loaded } = this.props

		if ( ! loaded.params || ! loaded.village ) return <Alert bsStyle="info">Loading...</Alert>

		const content = (
			<Tab.Content animation>
					<Tab.Pane eventKey="village">
						<Village />
					</Tab.Pane>
					<Tab.Pane eventKey="market">
						<Market />
					</Tab.Pane>
					<Tab.Pane eventKey="occupations">
						<Production />
					</Tab.Pane>
					<Tab.Pane eventKey="map">
						<Explorer height={600}/>
					</Tab.Pane>
					<Tab.Pane eventKey="mission">
						<Mission />
					</Tab.Pane>
					<Tab.Pane eventKey="troops">
						<Troops />
					</Tab.Pane>
					<Tab.Pane eventKey="reports">
						<Reports />
					</Tab.Pane>
					<Tab.Pane eventKey="tasks">
						<Tasks />
					</Tab.Pane>
					<Tab.Pane eventKey="ranking">
						<Ranking />
					</Tab.Pane>
					<Tab.Pane eventKey="multi-spawn">
						<MultiSpawn />
					</Tab.Pane>
					<Tab.Pane eventKey="diplomacy">
						<Diplomacy />
					</Tab.Pane>		
			</Tab.Content>
		)

		return (
			<Tab.Container id="tabcontainer" activeKey={view} onSelect={this.handleSelect}>
					<Row>
					<Col xs={2} >
						<SideBarNav/>
					</Col>
					<Col xs={10}>
						<Resources />
						{content}
					</Col>
				</Row>
			</Tab.Container>
		)
	}
}))