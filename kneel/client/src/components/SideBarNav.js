
import React from 'react'

import { Nav, NavItem, Glyphicon } from 'react-bootstrap';

export default React.createClass({
	render(){
		return (
			<Nav stacked bsStyle="pills">
				<NavItem eventKey="village">
					<Glyphicon glyph="home"/><span className="hidden-xs hidden-sm"> Village </span>
				</NavItem>

				<NavItem  eventKey="market">
					<Glyphicon glyph="transfer"/><span className="hidden-xs hidden-sm"> Market </span>
				</NavItem>
				
				<NavItem eventKey="occupations">
					<Glyphicon glyph="grain"/><span className="hidden-xs hidden-sm"> Production </span>
				</NavItem>
				<NavItem eventKey="map">
					<Glyphicon glyph="globe"/><span className="hidden-xs hidden-sm"> Map </span>
				</NavItem>

				<NavItem  eventKey="troops">
					<Glyphicon glyph="knight"/><span className="hidden-xs hidden-sm"> Troops </span>
				</NavItem>

				<NavItem  eventKey="reports">
					<Glyphicon glyph="list-alt"/><span className="hidden-xs hidden-sm"> Reports </span>
				</NavItem>

				<NavItem  eventKey="tasks">
					<Glyphicon glyph="tasks"/><span className="hidden-xs hidden-sm"> Tasks </span>
				</NavItem>

				<NavItem  eventKey="ranking">
					<Glyphicon glyph="stats"/><span className="hidden-xs hidden-sm"> Ranking </span>
				</NavItem>

				<NavItem  eventKey="diplomacy">
					<Glyphicon glyph="book"/><span className="hidden-xs hidden-sm"> Diplomacy </span>
				</NavItem>				
			</Nav>
		)
	}
})