import React from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { Alert, Row, Col, Form, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

import * as api from '../api/api'

const validPasswords = ({password, password2}) => {
	if ( password === null || password.length === 0 ) return undefined
	if ( password2 === null || password2.length === 0 ) return undefined

	return password === password2 ? "success" : "error"
}

const validationUsername = (validUser) => {
	if ( validUser === null ) return undefined

	return validUser ? "success" : "error"
}


export default withRouter( connect( ({ control: { login: {loggedIn} }}) => ({ loggedIn }))( React.createClass({
	getInitialState() {
		return { user: "", password: "", password2: "", villageName: "", validUser: null }
	},

	componentWillMount(){
		if ( this.props.loggedIn ) this.props.router.push("/")
	},

	submit(event) {
		event.preventDefault()

		if ( validPasswords(this.state) !== "success" ) return
		if ( validationUsername(this.state.validUser) !== "success" ) return 

		const redirect = this.props.router.push

		api.createUser(this.state).then( response => {
			console.log({ response })
			if ( ! response ){
				this.setState({ error: 'Connection problems' })
			} else if ( ! response.hasOwnProperty("ok")){
				console.log("Register response:", response)
				this.setState({ error: response.errorCode || JSON.stringify(response) })
			} else {
				redirect("/login")
			}
		})
	},

	onFormChange(e){
		const user = e.target.id === "user" ? e.target.value : this.state.user
		const password2 = e.target.id === "pass2" ? e.target.value : this.state.password2
		const password = e.target.id === "pass" ? e.target.value : this.state.password
		const villageName = e.target.id === "village" ? e.target.value : this.state.villageName

		if ( e.target.id === "user" ){
			api.freeUserName( user ).then( response => {
				this.setState({ validUser: response.free })
			})
		}

		this.setState({ user, password, password2, villageName })
	},

	render() {
		const invalid = validPasswords(this.state) !== "success" || validationUsername(this.state.validUser) !== "success"
		const error = this.state.error ? (<Alert bsStyle='danger'>{this.state.error}</Alert>) : ""

		return (
			<Form horizontal onSubmit={this.submit}>
				<Row><Col sm={5} smOffset={3}><h2>Register</h2></Col></Row>
				<Row><Col sm={5} smOffset={3}>
					{error}
				</Col></Row>
				<Row>
					<FormGroup validationState={validationUsername(this.state.validUser)}>
						<Col componentClass={ControlLabel} sm={2} smOffset={2}>
							Username :
						</Col>
						<Col componentClass={ControlLabel} sm={3}>
							<FormControl onChange={this.onFormChange}
											 placeholder="JonSnow"
											 id="user"
							/>
							<FormControl.Feedback />
						</Col>
					</FormGroup>
					{' '}
					<FormGroup validationState={validPasswords(this.state)}>
						<Col componentClass={ControlLabel} sm={2} smOffset={2}>
							Password
						</Col>
						<Col componentClass={ControlLabel} sm={3}>
							<FormControl	type="password" 
												onChange={this.onFormChange}
												placeholder="IKnowNothing"
												id="pass"
							/>
							<FormControl.Feedback />
						</Col>
					</FormGroup>
					<FormGroup validationState={validPasswords(this.state)}>
						<Col componentClass={ControlLabel} sm={2} smOffset={2}>
							Password Confirmation
						</Col>
						<Col componentClass={ControlLabel} sm={3}>
							<FormControl type="password" 
											 onChange={this.onFormChange}
											 id="pass2"
							/>
							<FormControl.Feedback />
						</Col>
					</FormGroup>
					<FormGroup>
						<Col componentClass={ControlLabel} sm={2} smOffset={2}>
							Village Name
						</Col>
						<Col componentClass={ControlLabel} sm={3}>
							<FormControl onChange={this.onFormChange}
											 id="village"
							/>
							<FormControl.Feedback />
						</Col>
					</FormGroup>
					<FormGroup>
						<Col componentClass={ControlLabel} sm={5}  smOffset={2}>
							<Button disabled={invalid} bsSize="large" bsStyle="info" block type="submit">Accept</Button>
						</Col>
					</FormGroup>
				</Row>
			</Form>
		)
	}
})))