import React from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

import * as api from '../api/api'
import * as actions from './ActionTypes'

import Dashboard from './Dashboard'
import Login from './Login'
import auth from './Auth'

import Navigation from './Navigation'

import '../css/kneel.css'
import '../css/village.css'
import '../css/troops.css'
import '../css/map.css'
import '../css/ctx-menu.css'
import '../css/mission.css'
import '../css/tasks.css'
import '../css/nav.css'
import '../css/production.css'

const App =  React.createClass({
	shouldComponentUpdate(nextProps, nextState) {
		//console.log(nextProps.data.lastAction, this.props.data.lastAction)
		//console.log(nextProps.location.pathname, this.props.location.pathname)

		if ( this.props.location.pathname !== nextProps.location.pathname ) return true
		if ( nextProps.location.pathname === "/register" ) return true
		if ( nextProps.location.pathname.match("/user/") ) return true
		

		if ( this.props.lastAction === "LOGIN_ERROR" ) return true

		if (nextProps.lastAction === "CHANGE_TAB" ){
			return this.props.tab.view !== nextProps.tab.view
		}

		if (nextProps.lastAction === "UPDATE_MAP" ) return true 

		//if ( this.props.data.lastAction !== nextProps.data.lastAction ) console.log("Rendering App")

		return this.props.lastAction !== nextProps.lastAction
	},
	componentWillMount() {
		const { login: { loggedIn } } = this.props
		const { dispatch } =  this.props

		if ( ! loggedIn ) {
			auth.login(dispatch, null, null)
		}

		api.errorHandlers.set("logout", () => {
			if ( loggedIn ) {
				dispatch(actions.logout)
			}
		})

		api.loadMarks().then( marks => {
			dispatch( actions.loadMarks(marks) )
		})
	},

	componentWillUpdate(nextProps){
		const {  login: { loggedIn }, lastAction } = nextProps

		//console.log("App updating. LoggedIn:", loggedIn, "lastAction:", nextProps.data.lastAction)

		if ( loggedIn ) {
			const { location } = nextProps

			if ( location.pathname === "/login" ) {
				console.log("to Login")
				this.props.router.replace('/dashboard')
			}  else if (location.state && location.state.nextPathname) {
				this.props.router.replace(location.state.nextPathname)
			}
		} else if ( lastAction === "LOGOUT" ) {
			this.props.router.push("/login")
		}
	},

	render() {
		const { login: { loggedIn }, loaded } = this.props
		const appLoaded = loaded.params && loaded.village && loaded.diplomacy

		let view = <img className="loading" src="/img/loading.gif" alt="loading"/>

		if ( appLoaded ){
			view = <Dashboard />
		}

		return (
			<div className="app">
				<Navigation loggedIn={loggedIn}/>
				{ this.props.children || (loggedIn ? view : <Login />) }
				<div>
					Icons made by <a href="http://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from 
					<a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by 
					<a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
				</div>
			</div>
		)
	}
})


const stateToProps = ({ control: { login, loaded, tab, lastAction } }) => ({ login, loaded, tab, lastAction })
const dispatchToProps = (dispatch) => ({
	dispatch
})

export default withRouter  ( connect((stateToProps), dispatchToProps)(App) )