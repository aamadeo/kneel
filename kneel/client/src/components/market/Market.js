import React from 'react'
import { connect } from 'react-redux'
import * as api from '../../api/api'
import { updateResources } from '../ActionTypes'
import core from '../../api/core'
import ResponsiveBox from '../ResponsiveBox'

import { Col, Form, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

const round = (num) => Math.floor(num* 100) / 100

let Market = React.createClass({
	getInitialState(){
		return { buy: "iron", sell: { resource :"food", count: core.available(this.props.resources.food) } }
	},
	onSellCountChange(e){
		const { resources } = this.props
		const sellMax = round( core.available( resources[this.state.sell.resource] ) ) 

		if ( e.target.value < sellMax ) this.setState({ sell: { count: e.target.value, resource: this.state.sell.resource } })
	},
	onSellChange(e){
		if( e.target.value === this.state.buy ) return 

		let sell = {
			count: this.state.sell.count,
			resource: e.target.value
		}
		this.setState({ sell })
	},
	onBuyChange(e){
		if( e.target.value === this.state.sell.resource ) return

		this.setState({ buy: e.target.value })
	},
	sell(){
		const dispatch = this.props.dispatch
		api.sell(this.state).then(
			response => {
				console.log("API::response = ", response)
				
				if (response !== null) dispatch( updateResources(response.resources) )
			}
		)
	},
	render() {
		const { resources, buildings } = this.props

		let villageHasMarket = buildings.findIndex( (x) => { return x !== null && x.name === 'Market' }) !== -1
		
		const selling = resources[this.state.sell.resource]
		const buying = resources[this.state.buy]
		
		const sellMax = round( core.available( selling ) / selling.capacity)
		const buyMax = round( core.available( buying ) / buying.capacity)
		const price = round ( Math.min( this.props.topPrice, sellMax / buyMax ) ) //market.topPrice
		var buyCount = round (this.state.sell.count / price)

		var content = "Village Has no Market"

		if ( villageHasMarket ){ 
			content = (
				<Form horizontal>
					<FormGroup>
						<Col componentClass={ControlLabel} sm={1}>
							Sell
						</Col>
						<Col componentClass={ControlLabel} sm={2}>
							<FormControl componentClass="select" value={this.state.sell.resource} onChange={this.onSellChange}>
								<option value="food">food</option>
								<option value="iron">iron</option>
								<option value="stone">stone</option>
								<option value="wood">wood</option>
							</FormControl>
						</Col>
						<Col componentClass={ControlLabel} sm={2}>
							<FormControl type="number" value={round(Math.min(this.state.sell.count, core.available( selling )))} onChange={this.onSellCountChange}/>
						</Col>
					</FormGroup>
					{' '}
					<FormGroup>
						<Col componentClass={ControlLabel} sm={1}>
							Buy
						</Col>
						<Col componentClass={ControlLabel} sm={2}>
							<FormControl componentClass="select" value={this.state.buy} onChange={this.onBuyChange}>
								<option value="food">food</option>
								<option value="iron">iron</option>
								<option value="stone">stone</option>
								<option value="wood">wood</option>
							</FormControl>
						</Col>
						<Col componentClass={ControlLabel} sm={2}>
							{buyCount}
						</Col>
					</FormGroup>
					<FormGroup>
						<Col componentClass={ControlLabel} sm={1}>
							Price
						</Col>
						<Col componentClass={ControlLabel} sm={2}>
							{price}
						</Col>
					</FormGroup>
					<FormGroup>
						<Col componentClass={ControlLabel} sm={5}>
							<Button bsSize="large" bsStyle="info" block onClick={this.sell}>Sell</Button>
						</Col>
					</FormGroup>
				</Form>
			)
		}

		return (
			<div className="view">
					<ResponsiveBox width={100} whRatio={60} prefix="market">
						{content}
					</ResponsiveBox>
			</div>
		)
	}
})

export default connect (
	({ 
		village: { resources, buildings },
		control: { configuration }
	}) => ({ 
		resources, 
		buildings,
		topPrice: configuration.market.topPrice
	})
)(Market)