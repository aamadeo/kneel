import React from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { Navbar, Nav, NavItem, Glyphicon } from 'react-bootstrap'

import * as api from '../api/api'
import * as actions from './ActionTypes'

export default withRouter ( connect()(React.createClass({
	handleSelect(key){
		const { dispatch } = this.props
		
		if ( key === "logout" ){
			api.logout().then( () => {
				dispatch(actions.logout)
			})
		} else if ( key === "login" ) {
			this.props.router.push("/login")

		} else if ( key === "register" ){
			console.log("Registering")
			this.props.router.push("/register")
		} else if ( key === 'profile' ){
			this.props.router.push("/profile")
		}
	},

	render(){
		return (
			<Navbar >
				<Navbar.Header>
					<Navbar.Brand>
						<a href="/#">kneel</a>
					</Navbar.Brand>
    			</Navbar.Header>
				<Nav bsStyle="pills" pullRight onSelect={this.handleSelect}>
					{
						this.props.loggedIn
							? [
								<NavItem eventKey="profile" key='profile'>
									<Glyphicon glyph='user' /><span ></span>
								</NavItem>,
								<NavItem eventKey="logout" key='logout'>
									<Glyphicon glyph='off' /><span ></span>
								</NavItem>,
							]
							: [<NavItem key="login" eventKey="login">Log In</NavItem>,
								 <NavItem key="signup" eventKey="register">Register</NavItem>]
					}
				</Nav>
			</Navbar>
		)
	}
})))