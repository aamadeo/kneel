import React from 'react'
import { render } from 'react-dom'
import { Router, Route, browserHistory, IndexRoute  } from 'react-router'
import { Provider } from 'react-redux'

import configureStore from './configureStore'
import routes from './components/routes'

const store = configureStore()
const routesWrapper = routes(store)

const renderApp = () => {
  render((
    <Provider store={store}>
      <Router routes={routesWrapper} history={browserHistory}/>
    </Provider>
  ), document.getElementById('app'))
}

store.subscribe( renderApp )

renderApp()

core.store = store