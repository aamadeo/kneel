module.exports = {
	Params: `https://api.dev.localhost/v2/users/params`,
	/*Village*/
	Village:`https://api.dev.localhost/v2/villages`,
	villageTroops:`https://api.dev.localhost/v2/villages/troops`,
	moveWorker: `https://api.dev.localhost/v2/villages/moveWorker`,
	research: `https://api.dev.localhost/v2/villages/research`,
	upgradeOccupation: `https://api.dev.localhost/v2/villages/upgradeOccupation`,
	spawn: `https://api.dev.localhost/v2/villages/spawn`,
	spawnArmy: `https://api.dev.localhost/v2/villages/spawnArmy`,
	build: `https://api.dev.localhost/v2/villages/build`,
	upgradeBuilding: `https://api.dev.localhost/v2/villages/upgrade`,
	trade:  `https://api.dev.localhost/v2/villages/sell`,
	tech: `https://api.dev.localhost/v2/villages/tech`,
	/*Map*/
	Map: `https://api.dev.localhost/v2/cells/`,
	/*Mission*/
	Missions: `https://api.dev.localhost/v2/missions/`,
	reports: `https://api.dev.localhost/v2/missions/reports`,
	toggleLoop: `https://api.dev.localhost/v2/missions/toggleLoop`,
	abort: `https://api.dev.localhost/v2/missions/abort`,
	/*Tasks*/
	Tasks:  `https://api.dev.localhost/v2/tasks`,
	/*Auth*/
	login: `https://api.dev.localhost/v2/users/login`,
	logout: `https://api.dev.localhost/v2/users/logout`,
	freeUser: `https://api.dev.localhost/v2/users/free`,
	users: `https://api.dev.localhost/v2/users/`,
	/*Diplomacy*/
	surrender: `https://api.dev.localhost/v2/diplomacy/surrender`,
	Diplomacy: `https://api.dev.localhost/v2/diplomacy`,
	troopsCalling: `https://api.dev.localhost/v2/diplomacy/troopsCalling`
}