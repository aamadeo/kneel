const express = require('express');
const url = require('url');
const checkAuth = require('./checkAuth');

module.exports = {
	publish(services, hook) {
		const router = express.Router();

		services.forEach(service => module.exports.addRoute(service, hook, router));

		return router;
	},

	addRoute: ({ method, resource, serviceHandler }, hook, router) => router[method](resource, checkAuth, (req, res, next) => {
		if (resource) resource = resource.substring(1);
		serviceHandler = serviceHandler || resource;
		console.log({ resource, serviceHandler, request: req.body });

		hook(serviceHandler)(req, object => res.send(JSON.stringify(object)));
	})
};