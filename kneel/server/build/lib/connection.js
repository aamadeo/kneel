"use strict";

const pg = require('pg');

// create a config to configure both pooling behavior 
// and client options 
// note: all config is optional and the environment variables 
// will be read if the config is not present 
const config = {
	user: 'kneel', //env var: PGUSER 
	database: 'kneel', //env var: PGDATABASE 
	password: 'orShitHappens', //env var: PGPASSWORD,
	host: 'localhost',
	port: 5432, //env var: PGPORT 
	max: 10, // max number of clients in the pool 
	idleTimeoutMillis: 30000 };

const connection = {
	pool: new pg.Pool(config),
	run(f) {
		connection.pool.connect((err, client, release) => {
			if (err) {
				console.log(err);
				console.log("Database connection error \n------------");
				return release();
			}

			f(client, release);
		});
	}
};

module.exports = connection;