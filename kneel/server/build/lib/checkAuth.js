let checkAuth = (req, res, next) => {
	let status = {
		authenticated: false,
		token: null
	};
	if (!req.session.user) {
		res.send(JSON.stringify({
			authenticated: false
		}));
	} else {
		next();
	}
};
module.exports = checkAuth;