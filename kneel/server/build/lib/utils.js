"use restrict";
//!!!!!! TODO bind() !!

let async = require('async');
let parse = require('postgres-interval');

let utils = {
	series: function (final) {
		let s = [];

		s.execute = callback => {
			async.series(s, (err, result, cb) => {
				if (err) console.log(err);

				if (cb) cb();

				console.log("Series result:", result);

				callback();
			});
		};

		s.add = transaction => {
			s.push(cb => {
				transaction.finally = status => cb(null, status);
				transaction.run();
			});
		};

		s.addList = elements => elements.forEach(x => {
			s.add(x);
		});

		return s;
	},
	bind(container, binding, ignores) {
		/*console.log("binding ----")*/
		for (f in container) {
			if (typeof container[f] !== "function") continue;
			if (ignores[f]) continue;

			container[f] = container[f].bind(binding);
			/*console.log(f)*/
		}
		/*console.log("----binding")*/
	},
	toPgInterval(time) {
		if (time === null) return null;

		let interval = Math.floor(time / 3600) + ":";

		time = time % 3600;
		interval = interval + ("00" + Math.floor(time / 60)).slice(-2);
		time = time % 60;
		interval = interval + ":" + ("00" + Math.floor(time)).slice(-2);
		interval = parse(interval);

		return interval;
	}
};

module.exports = utils;