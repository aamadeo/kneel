const express = require('express');
const router = express.Router();
const url = require('url');
const logic = require('./index');
const checkAuth = require('../lib/checkAuth');
const transactions = require('../lib/transactional');

const respond = (response, object) => response.send(JSON.stringify(object));

router.post('/surrender', checkAuth, function (req, res, next) {

	const session = req.session;
	console.log("Surrendering", req.body);
	const transaction = transactions.transaction();

	logic.surrender(session, req.body, transaction);

	transaction.finally = status => {
		respond(res, status === 0 ? { status: "pending" } : { errorCode: status });
	};

	transaction.run();
});

router.put('/surrender', checkAuth, function (req, res, next) {

	const session = req.session;
	console.log("Update Surrender: ", req.body);
	const transaction = transactions.transaction();
	const { response } = req.body;
	let action = null;

	switch (response) {
		case "accept":
		case "cancel":
			action = logic.confirm;break;
		case "update":
			action = logic.updateTerms;break;
		default:
			action = () => undefined;
	}

	action(session, req.body, transaction);

	transaction.finally = status => {
		respond(res, status === 0 ? { ok: true } : { errorCode: status });
	};

	transaction.run();
});

router.get('/', checkAuth, function (req, res, next) {

	const transaction = transactions.transaction();
	const info = { user: req.session.user };

	console.log("Diplomacy Info");

	logic.info(info, transaction);

	transaction.finally = status => {
		respond(res, status === 0 ? info : { errorCode: status });
	};

	transaction.run();
});

router.post('/troopsCalling', checkAuth, function (req, res, next) {

	const session = req.session;
	console.log("Calling Troops", req.body);
	const transaction = transactions.transaction();

	logic.callTroops(session, req.body, transaction);

	transaction.finally = status => {
		respond(res, status === 0 ? { ok: true } : { errorCode: status });
	};

	transaction.run();
});

module.exports = router;