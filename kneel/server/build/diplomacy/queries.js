const queryManager = require('../lib/qm');

module.exports = queryManager({
	queries: {
		surrender: "insert into allegiance_terms values ( $1, $2, $3, null, null, $4, null, 'pending')",
		info: 'select * from allegiance_terms where king = $1 or subject = $1',
		updateSurrender: 'update allegiance_terms set tax = $1, fisotk = $2 where king = $3 and subject = $4',
		cancelSurrender: 'delete from allegiance_terms where king = $1 and subject = $2',
		acceptSurrender: "update allegiance_terms set status = 'accepted' where king = $1 and subject = $2",
		updateCounters: `update allegiance_terms set "forceAvailable" = $1, "whResources" = $2, "brnResources" = $3 where king = $4 and subject = $5`,
		updateFiSotK: `update allegiance_terms set "forceAvailable" = "forceAvailable" + $1 where subject = $2`,
		updateTaxes: `update allegiance_terms set "whResources" = $1, "brnResources" = $2  where subject = $3`,
		subjectsOf: `select subject as name from allegiance_terms where king = $1`
	}
});