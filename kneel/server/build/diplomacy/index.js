const dao = require('./dao');
const core = require('../core');
const troops = require('../troops');
const village = require('../village');
const tasks = require('../tasks');
const map = require('../map');
const transactions = require('../lib/transactional');

const OperationType = {
	add(op) {},
	addAction(action) {},
	addQuery({ text, value, args }, action) {}
};

const TransactionType = {
	add(op) {},
	addAction(action) {},
	addQuery({ text, value, args }, action) {}
};

const diplomacy = {
	name: "diplomacy",
	err: {
		unauthorized: "User is not authorized to peform this operation",
		cantClaimMoreTroops: "You can't claim more troop from this subject at this time",
		noTroopsAvailable: "This subject doesn't have troops available"
	},

	getUsersLogic() {
		if (!this.users) {
			this.users = require('../users');
		}

		return this.users;
	},

	surrender({ user }, { tax, fisotk, king }, transaction) {

		//validaciones Todo!!

		console.log("Surrendering");
		dao.surrender({ user, tax, fisotk, king }, transaction);
	},

	callTroops({ user, village: kingVillage, location }, { fph, subject }, transaction) {
		const subjectInfo = {
			user: subject,
			villages: [],
			king: undefined,
			terms: []
		};
		this.info(subjectInfo, transaction);

		const users = this.getUsersLogic();
		users.userVillages(subjectInfo, transaction);

		transaction.addAction(op => {
			if (subjectInfo.king !== user) {
				transaction.abort(diplomacy.err.unauthorized);
			}

			fph = Math.min(fph, subjectInfo.terms[0].forceAvailable);

			if (fph < 1) {
				return transaction.abort(diplomacy.err.cantClaimMoreTroops);
			}

			subjectInfo.villages.forEach(v => {
				v.summary = core.villageSummary({ user: subject, village: v.id });
				village.getVillageTroops({ summary: v.summary, transaction: op });
			});
		});

		transaction.addAction(op => {

			/*User Total Force in Food Per Hour*/
			let utfph = 0;

			subjectInfo.villages.forEach(v => {
				v.fph = troops.calcFph(v.summary.troops);
				utfph += v.fph;
			});

			if (utfph < 1) {
				return transaction.abort(diplomacy.err.noTroopsAvailable);
			}

			subjectInfo.villages.forEach(v => {
				/*Village Required Force in Food Per Hour*/
				v.rfph = fph * v.fph / utfph;

				/*Required Troops*/
				v.rTroops = {};

				for (let troop in v.summary.troops) {

					/* Village.troops[troop] Force in Food Per Hour*/
					let vtfph = v.summary.troops[troop].count * core.profiles[troop].fph;

					/* Village.troops[troop] Required Force in Food Per Hour */
					let vtrfph = v.rfph * vtfph / v.fph;
					v.rTroops[troop] = { count: Math.round(vtrfph / core.profiles[troop].fph) };
				}

				const mission = {
					user: user,
					village: v.id,
					troops: v.rTroops,
					action: core.mission.support,
					origin: { x: v.x, y: v.y }, //!!
					objective: {
						type: 'village',
						id: v.id,
						owner: user,
						x: location.x,
						y: location.y
					}, //!!
					path: [{ x: v.x, y: v.y }, location], //!!
					user: subject
				};

				const params = {
					desc: `Troops Call`,
					job: 'supportMission',
					module: 'mission',
					mission,
					supported: user,
					supporter: subject,
					village: v
				};

				tasks.setTask({
					user,
					time: 0,
					days: 0,
					visible: false,
					start: null,
					params,
					selector: `troopCalling:${ user }:${ subject }`
				}, op);
			});
		});
	},

	updateTerms({ user }, { tax, fisotk, king }, transaction) {
		console.log("Updating");
		dao.updateTerms({ user, tax, fisotk, king }, transaction);
	},

	confirm({ user }, { king, subject, response }, transaction) {
		console.log("confirming");

		if (user !== king && user !== subject) {
			transaction.abort(diplomacy.err.unauthorized);
		}

		if (user !== king && response === "accept") {
			transaction.abort(diplomacy.err.unauthorized);
		}

		dao.confirm({ king, subject, response }, transaction);

		const subjectInfo = { user: subject, villages: [] };

		const users = this.getUsersLogic();
		users.userVillages(subjectInfo, transaction);

		transaction.addAction(op => {
			subjectInfo.villages.forEach(v => {
				map.getCell(v, op);
			});
		});

		if (response === "accept") {
			const params = {
				desc: `Terms of allegiance king:${ king }, subject:${ subject }`,
				job: 'termsEnforcement',
				module: 'diplomacy',
				king,
				subject
			};

			tasks.setTask({
				user,
				time: 60 * 60 * 24,
				days: 0,
				visible: false,
				start: null,
				params,
				selector: `termsEnforcement:${ king }:${ subject }`
			}, transaction);

			transaction.addAction(op => {
				subjectInfo.villages.forEach(v => {
					const villageCell = v.cells.find(c => c.type === "village");

					villageCell.detail.king = king;
					map.updateCell(villageCell, op);
				});
			});
		} else {
			const selector = `termsEnforcement:${ king }:${ subject }`;
			tasks.deleteTaskBySelector(selector, transaction);

			transaction.addAction(op => {
				subjectInfo.villages.forEach(v => {
					const villageCell = v.cells.find(c => c.type === "village");

					villageCell.detail.king = undefined;
					map.updateCell(villageCell, op);
				});
			});
		}
	},

	updateFiSotK(fph, subject, transaction) {
		dao.updateFiSotK(fph, subject, transaction);
	},

	updateTaxes(taxInfo, subject, transaction) {
		console.log("diploIndex", { taxInfo, subject });
		dao.updateTaxes(taxInfo, subject, transaction);
	},

	termsEnforcement({ days, king, subject }, transaction) {
		const info = {
			user: subject,
			terms: []
		};
		const subjectInfo = {};

		dao.info(info, transaction);
		const users = this.getUsersLogic();
		users.userCapacity(subjectInfo, subject, transaction);

		transaction.addAction(op => {
			console.log({ info });
			console.log("termsEnforcement", info.terms[0]);

			const { tax, fisotk } = info.terms[0];
			let { whResources, brnResources, forceAvailable } = info.terms[0];

			forceAvailable = forceAvailable || 0;
			whResources = whResources || 0;
			brnResources = brnResources || 0;

			brnResources += subjectInfo.brnCapacity * tax / 100;
			whResources += subjectInfo.whCapacity * 3 * tax / 100;

			if (days % 7 === 0) {
				forceAvailable += fisotk;
			}

			console.log({ brnResources, whResources, forceAvailable, days });

			dao.updateCounters({ king, subject, brnResources, whResources, forceAvailable }, op);
		});

		const params = {
			desc: `Terms of allegiance king:${ king }, subject:${ subject }`,
			job: 'termsEnforcement',
			module: 'diplomacy',
			days: days + 1 || 1,
			king,
			subject
		};

		tasks.setTask({
			king,
			time: 60 * 60 * 24,
			visible: false,
			start: null,
			params,
			selector: `termsEnforcement:${ king }:${ subject }`
		}, transaction);
	},

	info(userInfo, transaction) {
		dao.info(userInfo, transaction);

		transaction.addAction(op => {
			userInfo.ruler = userInfo.terms.length > 0 && userInfo.terms[0].king === userInfo.user;

			if (userInfo.ruler) {
				userInfo.allies = userInfo.terms.map(t => t.subject);
			} else if (userInfo.terms.length === 1) {
				userInfo.allies = [userInfo.terms[0].king];
				userInfo.king = userInfo.terms[0].king;

				dao.subjectsOf(userInfo.king, userInfo.allies, op);

				op.addAction(op => {
					const idx = userInfo.allies.findIndex(s => s === userInfo.user);

					userInfo.allies.splice(idx, 1);
				});
			} else {
				userInfo.allies = [];
			}
		});
	}
};

module.exports = transactions.transactionalModule(diplomacy);