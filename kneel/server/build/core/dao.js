const transactions = require('../lib/transactional');

const core = {
	queries: {
		costs: "select * from costs",
		profiles: "select * from troop_profiles",
		technologies: "select * from technologies",
		technology_dependencies: "select * from technology_dependencies"
	},
	technologies(technologies) {
		let transaction = transactions.transaction();

		let op = transactions.trxOperation(core.queries.technologies);
		op.perRow = (row, result) => {
			technologies[row.name] = row;
			technologies[row.name].dependencies = [];
		};
		transaction.add(op);

		op = transactions.trxOperation(core.queries.technology_dependencies);
		op.perRow = (row, result) => {
			technologies[row.technology].dependencies.push(row.dependency);
		};
		transaction.add(op);

		transaction.finally = status => {
			console.log("Technologies loaded");
			technologies.loaded = true;
		};

		transaction.run();
	},
	costs(costs) {
		let perRow = (row, result) => {
			costs[row.item] = row;
		};

		let resultHandler = result => {
			console.log("Costs loaded");
			costs.loaded = true;
		};

		transactions.singleQueryExec(core.queries.costs, { perRow, resultHandler });
	},
	profiles(profiles) {
		let perRow = (row, result) => {
			let profile = {
				atkToInfantry: 0,
				atkToArchers: 0,
				atkToCalvary: 0,
				atkToSiege: 0,
				atkToBuilding: 0,
				atkToWall: 0,
				defFromInfantry: 0,
				defFromArchers: 0,
				defFromCalvary: 0,
				defFromSiege: 0,
				speed: 0,
				fph: 0,
				capacity: 0
			};

			for (attr in profile) {
				if (row.profile[attr] === null) continue;
				profile[attr] = row.profile[attr];
			}

			profiles[row.troop] = profile;
		};

		let resultHandler = result => {
			console.log("Profiles loaded");
			profiles.loaded = true;
		};

		transactions.singleQueryExec(core.queries.profiles, { perRow, resultHandler });
	}
};

module.exports = core;