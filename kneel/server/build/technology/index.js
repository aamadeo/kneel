const dao = require('./dao');
const trx = require('../lib/transactional');
module.exports = trx.transactionalModule({
	available(user, technologies, transaction) {
		dao.available(user, technologies, transaction);
	}
});