const queryManager = require('../lib/qm');

const epochFrom = time => `extract(epoch from ${ time })`;
const epoch2TS = epoch => `TIMESTAMP ${ tz } epoch + + `;

module.exports = queryManager({
	idSpace: 'village',
	queries: {
		updatePopulation: 'update villages set population = population + $1 where id = $2',
		population: 'select population from villages where id = $1',
		resources: 'select name as resource, r0, current_timestamp - t0 as dt from village_resources where village = $1',
		occupations: 'select occupation, workers, level from village_occupations where village = $1',
		lockOccupation: 'select * from village_occupations where village = $1 and occupation = $2',
		troops: 'select troop, count, upgrades from village_troops where village = $1',
		buildings: `select slot, building as name, detail,
								 COALESCE(free_after, current_timestamp) <= current_timestamp as free,
								 extract(epoch from (COALESCE(free_after, current_timestamp) -  current_timestamp)::Interval) as free_after
						  from village_buildings where village = $1`,
		addWorker: "update village_occupations set workers = workers + 1 where village = $1 and occupation = 'unoccupied'",
		moveWorker: 'update village_occupations set workers = workers + $1 where village = $2 and occupation = $3',
		upgradeOccupation: 'update village_occupations set level = level + 1 where village = $1 and occupation = $2',
		updateResource: 'update village_resources set r0 = $1, t0 = current_timestamp where village = $2 and name = $3',

		newVillageId: "select nextval('village_seq') as id from dual",
		insertVillage: 'insert into villages values ($1, $2, $3, $4, $5, $6)',
		insertTroop: 'insert into village_troops values ($1, $2, $3, $4)',
		insertResource: 'insert into village_resources values ($1, $2, 1500, current_timestamp)',
		insertOccupation: 'insert into village_occupations values ($1, $2, $3, $4)',

		enableTech: 'insert into user_technologies values ($1, $2, 1, 2)'
	}
});