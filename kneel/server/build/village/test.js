const target = require('./index');
const core = require('../core');
const transactions = require('../lib/transactional');
const utils = require('../lib/utils');
const async = require('async');

const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number);

const validResource = r => r.resource.length > 0 && validNumber(r.r0) && validNumber(r.capacity) && validNumber(r.rps);

const validResources = resources => validResource(resources.food) && validResource(resources.iron) && validResource(resources.stone) && validResource(resources.wood);

const validOccupation = o => o.occupation.length > 0 && validNumber(o.workers) && validNumber(o.level);

const validOccupations = occupations => validOccupation(occupations.unoccupied) && validOccupation(occupations.food) && validOccupation(occupations.iron) && validOccupation(occupations.stone) && validOccupation(occupations.wood);

const validBuilding = b => b.name.length > 0 && validNumber(b.slot) && 0 <= b.slot && b.slot <= 16 && validNumber(b.detail.level) && validNumber(b.free_after);

const reduceAnd = (a, b) => a && b;

const validBuildings = buildings => buildings.map(b => b === null || validBuilding(b)).reduce(reduceAnd);

const validTroop = t => t.troop.length > 0 && validNumber(t.count) && t.upgrades !== undefined;
const validTroops = troops => Object.keys(troops).map(t => validTroop(troops[t])).reduce(reduceAnd);

const validVillageSummary = summary => validResources(summary.resources) && validOccupations(summary.occupations) && validBuildings(summary.buildings) && validTroops(summary.troops);

const startingConditions = () => {
		const transaction = transactions.transaction();
		transaction.addQuery({
				text: `update village_resources set r0 = 1000, t0 = current_timestamp where village = 39`,
				name: "testVillage.startingCondition.resources",
				values: []
		});

		transaction.addQuery({
				text: `delete from tasks where "user" = 'JonSnow'`,
				name: "testVillage.startingCondition.deleteTasks",
				values: []
		});

		return transaction;
};

const checkTask = (selector, task) => {
		const transaction = transactions.transaction();

		transaction.addQuery({
				text: `select * from tasks where "user" = 'JonSnow' and selector = $1`,
				name: "testVillage.startingCondition.checkTask",
				values: [selector]
		}, result => {
				Object.assign(task, result.rows[0]);
		});

		return transaction;
};

const tests = {
		testGetVillage(callback) {
				const series = utils.series();

				const transaction = transactions.transaction();
				const summary = core.villageSummary({ user: 'JonSnow', village: 39 });

				target.getVillage({ summary, transaction });

				series.add(startingConditions());
				series.add(transaction);

				series.execute(() => {
						callback(validVillageSummary(summary));
				});
		},
		testSell(callback) {
				const series = utils.series();
				series.add(startingConditions());

				/*Initial Village Status*/
				const loadVillageStatus = transactions.transaction();
				const summary = core.villageSummary({ user: 'JonSnow', village: 39 });
				target.getVillage({ summary, transaction: loadVillageStatus });

				series.add(loadVillageStatus);

				const buy = "iron";
				const sell = { resource: "food", count: 500 };

				/*Sell*/
				const sellTrx = transactions.transaction();
				const afterSummary = core.villageSummary({ user: 'JonSnow', village: 39 });

				target.sell({ summary: afterSummary, transaction: sellTrx }, { buy, sell });

				series.add(sellTrx);

				series.execute(() => {
						callback(parseInt(summary.resources.food.r0 - sell.count) == parseInt(afterSummary.resources.food.r0));
				});
		},
		testSpawn(callback) {
				const series = utils.series();
				series.add(startingConditions());

				const transaction = transactions.transaction();
				const summary = core.villageSummary({ user: 'JonSnow', village: 39 });

				/**
     * Validaciones:
     *    Slot hace spawn de ese "name" + Tiene Recursos
     */

				target.spawn({ summary, transaction }, { type: 'spawn', name: 'peasant', slot: 12 });
				series.add(transaction);

				const task = {};
				series.add(checkTask('39:spawn:peasant', task));

				series.execute(() => {
						callback(task.id !== undefined);
				});
		},
		x() {
				/* Service Handlers */
				// GET https://host/villages/
				getVillage({ summary, transaction });

				// POST https://host/villages/sell


				// POST https://host/villages/spawnArmy
				spawnArmy({ summary, transaction }, { troops });

				// POST https://host/villages/spawn
				spawn({ summary, transaction }, spec);

				// POST https://host/villages/build
				newBuilding({ summary, transaction }, spec);

				// POST https://host/villages/upgrade
				upgrade({ summary, transaction }, { slot, type, builders });

				// POST https://host/villages/research
				research({ summary, transaction }, { technology, slot, icon });

				// POST https://host/villages/upgradeTech
				upgradeOccupation({ summary, transaction }, { occupation });

				// PUT https://host/villages/moveWorker
				moveWorker({ summary, transaction }, { occupation, workers });

				/* Task Handlers */

				build(params, transaction);

				enableTech({ technology, user }, transaction);

				upgradeTech(params, transaction);

				upgradeBuilding(params, transaction);

				spawnArmyUnit(params, transaction);

				spawnUnit({ village, user, type }, transaction);

				/* Exposed functions */

				collectTaxes(mission, transaction);

				collect(collector, payer, title, transaction);

				updatePopulation(count, village, transaction);
		}

};

const executeTest = () => {
		if (!(core.profiles.loaded && core.costs.loaded)) {
				return setTimeout(executeTest, 1000);
		}

		const results = {};
		const testCases = [];

		for (let testFunction in tests) {
				if (testFunction === "x") continue;
				let name = testFunction.replace("test", "");
				name = name.charAt(0).toLowerCase() + name.substring(1);

				testCases.push(callback => {

						tests[testFunction](result => {
								const testCase = name;

								console.log(" Done ", testCase);
								results[testCase] = result ? "success" : "error";

								callback(null);
						});
				});
		}

		async.series(testCases, err => {
				if (err) console.log(err);else console.log(results);
		});
};

executeTest();