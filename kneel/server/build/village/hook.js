const village = require('./index');
const core = require('../core');
const transactions = require('../lib/transactional');
const validator = require('./request.validator');

module.exports = name => ({ session, body }, callback) => {
	const transaction = transactions.transaction();
	const summary = core.villageSummary(session);

	console.log("Executing: village." + name, "\nbody", body);

	try {
		validator[name](body);

		village[name](summary, body, transaction);

		transaction.finally = status => {
			if (status === 0) {
				delete summary.ignore;
				delete summary.locks;

				callback(summary);
			} else {
				console.log({
					error: status,
					shortage: summary.shortage
				});

				callback({ errorCode: status });
			}
		};

		transaction.run();
	} catch (err) {
		console.log(err);
		return callback(err);
	}
};