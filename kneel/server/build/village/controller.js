const publisher = require('../lib/publisher');
const villageHook = require('./hook');

const services = [{ method: 'get', resource: '/', serviceHandler: 'getVillage' }, { method: 'get', resource: '/troops', serviceHandler: 'getVillageTroops' }, //!!
{ method: 'post', resource: '/spawnArmy' }, { method: 'post', resource: '/spawn' }, { method: 'post', resource: '/build', serviceHandler: 'newBuilding' }, { method: 'post', resource: '/upgrade' }, { method: 'post', resource: '/upgradeOccupation' }, { method: 'post', resource: '/sell' }, { method: 'post', resource: '/research' }, { method: 'put', resource: '/moveWorker' }];

module.exports = publisher.publish(services, villageHook);