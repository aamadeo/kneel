const transactions = require('../lib/transactional');
const qm = require('./queries');

const map = {
	remove(type, object, transaction) {
		qm.addSimpleQuery("delete", [type, object], transaction);
	},

	put({ x, y, section }, object, type, detail, transaction) {
		qm.addSimpleQuery("insert", [section.x, section.y, type, object, x, y, detail], transaction);
	},

	cellsInSection(section, cells, transaction) {
		qm.addQuery({
			name: "sectionCells",
			args: [section.x, section.y],
			perRow: row => {
				let cell = {
					x: row.x,
					y: row.y,
					type: row.type,
					id: row.object
				};

				for (attr in row.detail) cell[attr] = row.detail[attr];

				cells.push(cell);
			}
		}, transaction);
	},

	getCellByCoordinates(object, transaction) {
		object.cells = [];
		qm.addQuery({
			name: "cellByCoordinates",
			args: [object.x, object.y],
			perRow: row => object.cells.push(row)
		}, transaction);
	},

	getCellByIdentifier(object, transaction) {
		object.cells = [];
		qm.addQuery({
			name: "cellByIdentifier",
			args: [object.type, object.id],
			perRow: row => object.cells.push(row)
		}, transaction);
	},

	updateCell(object, transaction) {
		console.log("updating:", object);

		qm.addSimpleQuery("updateCell", [object.detail, object.type, object.object], transaction);
	},

	move(type, object, from, to, detail, transaction) {
		detail.t0 = new Date().getTime();
		qm.addSimpleQuery("move", [to.x, to.y, detail, to.section.x, to.section.y, type, object], transaction);
	}
};

module.exports = transactions.transactionalModule(map);