const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number);

const validator = {
	err: {
		invallidPoint: -1
	},

	getCells({ x, y }) {
		if (x === undefined && y === undefined) return;

		if (!validNumber(x)) throw {
			code: validator.err.invallidPoint,
			msg: "Invalid value for x " + x
		};

		if (!validNumber(y)) throw {
			code: validator.err.invallidPoint,
			msg: "InvalidValue for y " + y
		};
	}
};

module.exports = validator;