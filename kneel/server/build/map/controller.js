const express = require('express');
const router = express.Router();
const url = require('url');
const map = require('./index');
const checkAuth = require('../lib/checkAuth');
const transactions = require('../lib/transactional');
const validator = require('./request.validator');

const respond = (response, object) => response.send(JSON.stringify(object));

router.get('/', checkAuth, function (req, res, next) {
	const transaction = transactions.transaction();
	const cells = [];
	const { location, radius } = req.session;
	console.log("Executing: map.getCells");

	const url_parts = url.parse(req.url, true);
	const query = url_parts.query;

	try {
		validator.getCells(query);

		map.getCells({ cells, location, radius }, query, transaction);

		transaction.finally = status => {
			console.log("status: ", status);

			respond(res, status === 0 ? cells : { errorCode: status });
		};

		transaction.run();
	} catch (err) {
		console.log({ err });
		respond(res, err);
	}
});

module.exports = router;