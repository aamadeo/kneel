const queryManager = require('../lib/qm');

module.exports = queryManager({
	idSpace: 'map',
	queries: {
		delete: 'delete from "map" where type = $1 and object = $2',
		insert: 'insert into map values( $1, $2, $3, $4, $5, $6, $7)',
		move: 'update "map" set x = $1, y = $2, detail = $3, xs = $4, ys = $5 where type = $6 and object = $7',
		sectionCells: 'select x, y, object, type, detail from map where xs = $1 and ys = $2',
		cellByCoordinates: 'select * from map where x = $1 and y = $2',
		cellByIdentifier: 'select * from map where type = $1 and object = $2',
		updateCell: 'update map set detail = $1 where type = $2 and object = $3'
	}
});