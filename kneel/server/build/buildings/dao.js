const utils = require('../lib/utils');
const qm = require('./queries');
const trx = require('../lib/transactional');

const buildings = {
	startBuilding(village, slot, building, detail, transaction) {
		qm.addSimpleQuery("startBuilding", [village, slot, building, detail], transaction);
	},

	finishBuilding(village, slot, building, transaction) {
		qm.addSimpleQuery("finishBuilding", [building, village, slot], transaction);
	},

	upgrade(village, slot, detail, freeAfter, transaction) {
		qm.addSimpleQuery("upgrade", [detail, freeAfter, village, slot], transaction);
	},

	update(start, duration, village, slot, transaction) {
		qm.addSimpleQuery("update", [start, duration, village, slot], transaction);
	},

	lock(village, slot, transaction) {
		qm.addSimpleQuery("lock", [village, slot], transaction);
	}
};

module.exports = trx.transactionalModule(buildings);