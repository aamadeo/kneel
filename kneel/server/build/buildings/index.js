const utils = require('../lib/utils');
const dao = require('./dao');
const trx = require('../lib/transactional');

const buildings = {
	startBuilding({ village, slot, building }, transaction) {
		let detail = { level: 0 };
		dao.startBuilding(village, slot, building, detail, transaction);
	},

	finishBuilding({ village, slot, building }, transaction) {
		dao.finishBuilding(village, slot, building, transaction);
	},

	upgrade({ village, slot, detail, freeAfter }, transaction) {
		console.log("logic", { village, slot, detail, freeAfter });
		dao.upgrade(village, slot, detail, freeAfter, transaction);
	},

	update({ village, slot, duration, start }, transaction) {
		duration = utils.toPgInterval(duration);
		start = utils.toPgInterval(start);

		dao.update(start, duration, village, slot, transaction);
	},

	lock({ village, slot }, transaction) {
		dao.lock(village, slot, transaction);
	}
};

module.exports = trx.transactionalModule(buildings);