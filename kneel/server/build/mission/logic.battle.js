const village = require('../village');
const mission = require('../mission');
const troops = require('../troops');
const users = require('../users');
const core = require('../core');

const battle = {

	troopType(troop) {
		if (troop === "infantry" || troop === "pikeman") return "Infantry";
		if (troop === "scout" || troop === "knight") return "Calvary";
		if (troop === "archer" || troop === "heavy-archer") return "Archers";
		if (troop === "ram" || troop === "catapult" || troop === "trebuchet") return "Siege";
	},
	adjustCasualties(group) {
		if (group.casualties === undefined) return;

		const names = Object.keys(group.troops);
		group.count = 0;

		for (let t = 0; t < names.length; t++) {
			let troop = group.troops[names[t]];

			troop.count = Math.round(troop.count);
			group.casualties[names[t]] = Math.round(group.casualties[names[t]]);
			group.count += troop.count;
		}
	},
	damageFrom(troops, type) {
		let atk = {
			toArchers: 0,
			toCalvary: 0,
			toInfantry: 0,
			toSiege: 0
		};

		let types = Object.keys(troops);

		for (let t = 0; t < types.length; t++) {
			let troop = troops[types[t]];

			if (this.troopType(types[t]) !== type) continue;

			atk.toArchers += core.profiles[types[t]].atkToArchers * troop.count || 0;
			atk.toCalvary += core.profiles[types[t]].atkToCalvary * troop.count || 0;
			atk.toInfantry += core.profiles[types[t]].atkToInfantry * troop.count || 0;
			atk.toSiege += core.profiles[types[t]].atkToSiege * troop.count || 0;
		}

		return atk;
	},
	calcDamage(attack, defense) {
		attack.damageGiven = {
			fromArchers: this.damageFrom(attack.troops, "Archers"),
			fromCalvary: this.damageFrom(attack.troops, "Calvary"),
			fromInfantry: this.damageFrom(attack.troops, "Infantry"),
			fromSiege: this.damageFrom(attack.troops, "Siege"),
			from(from, to) {
				to = battle.troopType(to);
				return this[`from${ from }`][`to${ to }`] * attack.weight;
			}
		};

		/*		console.log("Attack.damageGiven = ")
  		console.log(attack.damageGiven)*/

		if (defense.casualties === undefined) {
			defense.casualties = {};
		}

		defense.damage = {};
		let dNames = Object.keys(defense.troops);
		for (let d = 0; d < dNames.length; d++) {
			let dTroop = defense.troops[dNames[d]];

			let damage = attack.damageGiven.from("Archers", dNames[d]);
			damage += attack.damageGiven.from("Calvary", dNames[d]);
			damage += attack.damageGiven.from("Infantry", dNames[d]);

			defense.damage[dNames[d]] = Math.min(dTroop.count, damage * (dTroop.count / defense.count));

			if (defense.casualties[dNames[d]] === undefined) defense.casualties[dNames[d]] = 0;
			defense.casualties[dNames[d]] += defense.damage[dNames[d]];
		}

		/*		console.log("Defense.damage: ")
  		console.log(defense.damage)*/
	},
	inflictDamage(group) {
		group.count = 0;

		let dNames = Object.keys(group.troops);
		for (let d = 0; d < dNames.length; d++) {
			group.troops[dNames[d]].count -= group.damage[dNames[d]];
			group.count += group.troops[dNames[d]].count;
		}
	},
	troopCount(group) {
		if (group.troops === null) return 0;

		let names = Object.keys(group.troops);
		group.count = 0;

		for (let a = 0; a < names.length; a++) {
			group.troops[names[a]].count = parseInt(group.troops[names[a]].count);
			group.count += parseInt(group.troops[names[a]].count);
		}
	},
	prepare(group) {
		group.count = 0;
		group.originalTroops = group.troops;
		group.troops = {};

		if (group.troops === null) return;

		let names = Object.keys(group.originalTroops);

		for (let t = 0; t < names.length; t++) {
			group.troops[names[t]] = { count: group.originalTroops[names[t]].count };
			group.count += parseInt(group.troops[names[t]].count);
			group.originalTroops[names[t]].fph = core.profiles[names[t]].fph;
		}
	},
	fight(attack, defense, result) {

		this.prepare(attack);
		this.prepare(defense);

		while (attack.count > 0 && defense.count > 0) {
			/*			console.log("\n---Start round---\n")*/

			attack.weight = defense.count === 0 ? 1 : attack.count / defense.count;
			defense.weight = 1 / attack.weight;

			attack.weight = attack.weight;
			defense.weight = defense.weight;

			/*			console.log("Attack -> Defense")*/
			this.calcDamage(attack, defense);

			/*			console.log("Defense -> Attack")*/
			this.calcDamage(defense, attack);

			/*			console.log({
   				attack: {
   					count: attack.count,
   					damage: attack.damage,
   					casualties: attack.casualties
   				}, defense : {
   					count: defense.count,
   					damage: defense.damage,
   					casualites: defense.casualties
   				}
   			})*/

			this.inflictDamage(attack);
			this.inflictDamage(defense);

			if (attack.action === core.mission.raid) break;

			/*console.log("\n---End round---\n")*/
		}

		this.adjustCasualties(attack);
		this.adjustCasualties(defense);

		result.winner = attack.count === 0 ? defense : attack;
		result.looser = attack.count === 0 ? attack : defense;
		result.winnerSide = attack.count === 0 ? "defense" : "attack";

		/*console.log("result :", result)*/
	},
	attack(attack, keepMoving, endMission, saveReport, curOp) {

		let result = {};
		let defense = null;

		attack.type = "mission";

		if (attack.objective.type === "village") {
			defense = core.villageSummary({ user: attack.objective.owner, village: attack.objective.id });
			defense.type = "village";
			defense.locks.troops;
			defense.locks.resources;
			defense.locks.buildings;

			//console.log({ defense })
			village.load(defense, curOp);
		}

		curOp.addAction(operation => {
			village.adjustModel(defense);
			battle.fight(attack, defense, result);
			battle.loot(attack, defense, operation);

			const report = battle.battleConclusion(attack.id, result, keepMoving.from, operation);

			console.log("Saving report");
			saveReport(report, operation);
		});

		curOp.addAction(op => {
			console.log("Winner:", result.winnerSide);

			if (result.winnerSide === "attack") keepMoving(op);else endMission(attack.id, op);
		});
	},

	battleConclusion(missionId, result, from, curOp) {
		let defense = result.winnerSide === "defense" ? result.winner : result.looser;
		let attack = result.winnerSide === "defense" ? result.looser : result.winner;

		if (defense.casualties) {
			let deathCount = 0;
			for (let t in defense.troops) {
				const troopDeaths = defense.originalTroops[t].count - defense.troops[t].count;
				deathCount += core.profiles[t].fph * troopDeaths;
			}

			troops.casualties(defense, curOp);
			users.addPoints(attack.user, deathCount, curOp);
		}

		if (attack.casualties) {
			let deathCount = 0;
			for (let t in attack.troops) {
				const troopDeaths = attack.originalTroops[t].count - attack.troops[t].count;
				deathCount += core.profiles[t].fph * troopDeaths;
			}

			console.log("attack casualties: ", deathCount);
			troops.casualties(attack, curOp);
			users.addPoints(defense.user, deathCount, curOp);
		}

		return battle.battleReport(missionId, result, curOp);
	},
	loot(robber, victim, transaction) {
		village.collect(robber, victim, 'loot', transaction);
	},
	battleReport(missionId, result, curOp) {
		let report = {};
		let reporting = [];

		let defense = result.winnerSide === "defense" ? result.winner : result.looser;
		let attack = result.winnerSide === "defense" ? result.looser : result.winner;

		report.id = missionId;
		report.time = new Date();
		report.attack = [attack.user];
		report.defense = [defense.user];
		report.loot = attack.loot;
		report.village = attack.objective;
		report.troops = {
			attack: {
				original: attack.originalTroops,
				after: attack.troops
			},
			defense: {
				original: defense.originalTroops,
				after: defense.troops
			}
		};

		return report;
	}
};

module.exports = battle;