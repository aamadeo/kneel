const qm = require('./queries');
const troops = require('../troops/');
const trx = require('../lib/transactional');

const mission = {
	queries: {
		getId: "select nextval('mission_seq')",
		newMission: 'insert into missions values ($1, $2, $3, $4)',
		delete: 'delete from missions where mission = $1',
		addTroop: 'insert into mission_troops values ($1, $2, $3)',
		deleteTroops: 'delete from mission_troops where mission = $1',
		addPath: 'insert into path values ($1, $2, $3, $4)',
		deletePath: 'delete from path where mission = $1',
		getMissions: 'select mt.*, m.action, m.details from missions m, mission_troops mt where m.user = $1 and m.mission = mt.mission order by mt.mission',
		getMission: 'select * from missions where mission = $1',
		update: 'update missions set details = $2 where mission = $1',
		reports: 'select r.* from reports r, user_reports ur where ur.mission = r.mission and ur.user = $1 order by mission desc limit 10',
		saveReport: 'insert into reports values ( $1, $2 )',
		saveUserReport: 'insert into user_reports values ( $1, $2 )'
	},

	setQuery(name, args, lock) {
		let forUpdate = typeof lock === "boolean" && lock ? " for update" : "";
		args = args !== null ? args : [];

		return { text: mission.queries[name] + forUpdate, name: "mission." + name, values: args };
	},

	renewMission(config, transaction) {
		transaction.addQuery(this.setQuery("getId"), result => {
			config.id = result.rows[0].nextval;
		});

		transaction.addAction(op => {
			console.log("Mission.id:", config.id);
		});
	},

	setMission(config, transaction) {
		qm.addQuery({
			name: "getId",
			args: [],
			perRow: row => {
				config.id = row.nextval;
			}
		}, transaction);

		transaction.addAction(op => {
			console.log("Mission.id:", config.id);

			let { speed, objective, loop } = config;
			let detail = { speed, objective, loop };
			const owner = config.owner || config.user;

			qm.addSimpleQuery("newMission", [owner, config.id, config.action, detail], op);

			//Setting of GroupMembers
			for (name in config.troops) {
				let count = config.troops[name].count || 0;
				qm.addSimpleQuery("addTroop", [config.id, name, count], op);
			}

			//Setting of path
			let path = config.path;
			for (let p = 0; p < path.length; p++) {
				qm.addSimpleQuery("addPath", [config.id, p, path[p].x, path[p].y], op);
			}
		});
	},

	endMission(id, transaction) {
		transaction.addQuery(this.setQuery("deleteTroops", [id]));
		transaction.addQuery(this.setQuery("deletePath", [id]));
		transaction.addQuery(this.setQuery("delete", [id]));
	},

	getMission(id, container, transaction) {
		transaction.addQuery(this.setQuery("getMission", [parseInt(id)]), result => {
			container.mission = result.rows[0];
		});
	},

	update({ mission, details }, transaction) {
		transaction.addQuery(this.setQuery("update", [mission, details]));
	},

	getMissions(user, missions, transaction) {
		let mIdx = -1;

		qm.addQuery({
			name: "getMissions",
			args: [user],
			perRow: row => {
				if (mIdx === -1 || missions[mIdx].id !== row.mission) {
					mIdx++;
					missions[mIdx] = {
						id: row.mission,
						action: row.action,
						details: row.details,
						troops: {}
					};
				}

				missions[mIdx].troops[row.troop] = row.count;
			}
		}, transaction);
	},

	getMissionPath(user, groupId, path) {
		let params = [user, groupId];
		let loadGroupPath = dao.core.preparedQuery("select * from path where owner = ? and group = ?", params);

		loadGroupPath.handle = (err, result) => {
			if (err) return;

			for (let s = 0; s < result.rows.length; s++) {
				let step = result.rows[s];

				delete step.group;
				delete step.owner;

				path[s] = step;
			}
		};

		return loadGroupPath;
	},
	saveReport(report, transaction) {
		console.log("Saving report: " + report.id);

		let id = report.id;
		delete report.id;

		transaction.addQuery(this.setQuery("saveReport", [id, report]));

		for (let i = 0; i < report.attack.length; i++) {
			console.log("Saving user report ", [report.attack[i], id]);
			transaction.addQuery(this.setQuery("saveUserReport", [report.attack[i], id]));
		}

		for (let i = 0; i < report.defense.length; i++) {
			transaction.addQuery(this.setQuery("saveUserReport", [report.defense[i], id]));
		}
	},
	getReports(user, reports, transaction) {
		qm.addQuery({
			name: "reports",
			args: [user],
			perRow: row => reports.push(row)
		}, transaction);
	}
};

for (f in mission) {
	if (typeof mission[f] !== "function") continue;
	mission[f] = mission[f].bind(mission);
}

module.exports = trx.transactionalModule(mission);