const core = require('../core');

const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number);
const positiveNumber = (number, title, code) => {
	if (!validNumber(number) || number > 0) {
		throw {
			msg: `Invalid ${ title } count : ` + number,
			code
		};
	}
};

const validTroop = troop => {
	if (!core.profiles[troop]) {
		throw {
			msg: "Troop unknown: " + troop,
			code: validator.err.invalidTroopName
		};
	}
};

const validAction = action => {
	if (!core.mission[action]) {
		throw {
			code: validator.err.InvalidAction,
			msg: 'Invalid action: ', action
		};
	}
};

const validObject = object => typeof object === 'object';

const validPoint = ({ x, y }) => {
	if (!validNumber(x)) {
		throw {
			code: validator.err.invallidPoint,
			msg: "Invalid value for x " + x
		};
	}

	if (!validNumber(y)) {
		throw {
			code: validator.err.invallidPoint,
			msg: "InvalidValue for y " + y
		};
	}
};

const validString = (_string, title, code) => {
	if (typeof _string !== 'string' || _string.length === 0) {
		throw {
			msg: title + " must be a valid string : " + _string,
			code
		};
	}
};

const validator = {
	err: {
		invalidMissionId: -1,
		invalidAction: -2
	},

	getMissions({}) {},

	getReports({}) {},

	toggleLoop({ missionId }) {
		if (!validNumber(missionId)) {
			throw {
				msg: 'Invalid Mission ID: ' + missionId,
				code: validator.err.invalidMissionId
			};
		}
	},

	newMission({ troops, action, objective, path }) {

		for (let troop in troops) {
			validTroop(troop);
			positiveNumber(troops[troop].count, "Troop Count", validator.err.invalidTroopCount);

			console.log(troop);

			validRequest = true;
		}

		validAction(action);
		validPoint(objective);

		let idx = ['village', 'troops'].findIndex(e => e === objective.type);
		if (idx !== -1) {
			throw {
				msg: "Invalid type of objective : " + objective.type,
				code
			};
		}

		positiveNumber(objective.id);

		/* PATH */
	}
};