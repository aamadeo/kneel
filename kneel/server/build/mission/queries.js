const queryManager = require('../lib/qm');

module.exports = queryManager({
	idSpace: 'mission',
	queries: {
		getId: "select nextval('mission_seq')",
		newMission: 'insert into missions values ($1, $2, $3, $4)',
		delete: 'delete from missions where mission = $1',
		addTroop: 'insert into mission_troops values ($1, $2, $3)',
		deleteTroops: 'delete from mission_troops where mission = $1',
		addPath: 'insert into path values ($1, $2, $3, $4)',
		deletePath: 'delete from path where mission = $1',
		getMissions: 'select mt.*, m.action, m.details from missions m, mission_troops mt where m.user = $1 and m.mission = mt.mission order by mt.mission',
		getMission: 'select * from missions where mission = $1',
		update: 'update missions set details = $2 where mission = $1',
		reports: 'select r.* from reports r, user_reports ur where ur.mission = r.mission and ur.user = $1 order by mission desc limit 10',
		saveReport: 'insert into reports values ( $1, $2 )',
		saveUserReport: 'insert into user_reports values ( $1, $2 )'
	}
});