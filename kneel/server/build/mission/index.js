const dao = require('./dao');

const tasks = require('../tasks');
const village = require('../village');
const map = require('../map');
const battle = require('./logic.battle');
const diplomacy = require('../diplomacy');
const troops = require('../troops');

const core = require("../core");
const utils = require("../lib/utils");
const trx = require('../lib/transactional');

const maxDistance = core.diameter * Math.sqrt(2);

function point(p) {
	return {
		d(to) {
			return Math.sqrt(Math.pow(this.x - to.x, 2) + Math.pow(this.y - to.y, 2));
		},
		equals(p) {
			return this.x === p.x && this.y === p.y;
		},

		x: p.x,
		y: p.y
	};
}

//Cells per second
let troopSpeed = 1 / 100;

const mission = {
	err: {
		unauthorizedUpdate: "User does not own mission",
		invalidAction: "You can't perform this action to this user"
	},
	getReports(user, reports, transaction) {
		dao.getReports(user, reports, transaction);
	},
	getMissions(user, missions, transaction) {
		console.log("Missions of " + user);

		dao.getMissions(user, missions, transaction);

		transaction.addAction(op => {
			missions.forEach(mission => {
				const selector = "mission:" + mission.id;
				tasks.getTaskBySelector(selector, mission, op);
			});
		});

		transaction.addAction(op => {
			missions.forEach(mission => {
				const task = mission.task;
				delete mission.task;

				mission.details = Object.assign(mission.details, task.params.mission);
				mission.location = task.params.from;
				mission.elapsedTime = new Date().getTime() / 1000 - mission.details.t0;
			});
		});
	},
	toggleLoop({ session: { user }, body: { missionId } }, transaction) {
		const container = {};

		tasks.getTaskBySelector("mission:" + missionId, container, transaction);
		dao.getMission(missionId, container, transaction);

		transaction.addAction(op => {

			if (container.task.user !== user) {
				transaction.abort(this.err.unauthorizedUpdate);
			}

			container.task.params.mission.loop = !container.task.params.mission.loop;

			container.mission.details.loop = !container.mission.details.loop;

			tasks.update(container.task, op);
			dao.update(container.mission, op);
		});
	},
	restart(mission, transaction) {
		let { user, village } = mission;
		const location = mission.path[0];
		location.x = parseInt(location.x);
		location.y = parseInt(location.y);

		const summary = core.villageSummary(mission);

		this.execute(mission, summary, transaction);
	},

	supportMission({ mission, supporter, village, supported }, transaction) {
		const location = mission.path[0];
		location.x = parseInt(location.x);
		location.y = parseInt(location.y);
		const summary = core.villageSummary({ user: supporter, village: village.id });
		mission.speed = 1000;

		this.execute(mission, summary, transaction);
	},

	execute(params, summary, transaction) {
		diplomacy.info(summary, transaction);

		transaction.addAction(op => {
			const target = params.objective.owner;

			params.ally = summary.allies.findIndex(a => a === target) !== -1;

			if (summary.king && summary.king === target) {
				params.objective.king = true;
			}

			params.king = summary.ruler;

			if (params.action === core.mission.collect) {
				if (!(params.ally && params.king)) {
					return transaction.abort(this.err.invalidAction);
				} else {
					const terms = summary.terms.find(t => t.subject === target);
					params.taxInfo = {
						materials: terms.whResources,
						food: terms.brnResources
					};
				}
			}
		});

		village.load(summary, transaction);

		let enoughTroops = true;
		let notEmpty = false;

		for (let name in params.troops) {
			if (!params.troops[name] || params.troops[name].count === 0) continue;

			let speed = parseInt(core.profiles[name].speed) / 3600;

			if (speed < params.speed) {
				params.speed = speed;
			}
		}

		transaction.addAction(() => {
			village.adjustModel(summary);
			params.speed = core.speed * params.speed;

			for (let troop in summary.troops) {
				let available = summary.troops[troop] ? summary.troops[troop].count : 0;

				enoughTroops = enoughTroops && params.troops[troop].count <= available;

				notEmpty = notEmpty || params.troops[troop].count > 0;

				summary.troops[troop].count -= params.troops[troop].count;
			}

			Object.assign(summary, { enoughTroops, notEmpty });

			if (!(enoughTroops && notEmpty)) transaction.abort();
		});

		const fph = troops.movingTroops(params, false, transaction);
		village.updatePopulation(fph, summary.village, transaction);
		village.updateResources(summary, undefined, transaction);

		transaction.addAction(op => {
			if (params.action === core.mission.support) {
				params.owner = params.objective.owner;

				if (params.objective.king) {
					diplomacy.updateFiSotK(fph, params.user, op);
				}
			} else {
				params.owner = params.user;
			}

			dao.setMission(params, op);
		});

		transaction.addAction(op => {

			const detail = {
				user: params.user,
				mission: params.id,
				objective: params.objective
			};

			const distance = this.fullpath(params.origin, params.objective);
			params.eta = distance / params.speed;
			params.t0 = new Date().getTime() / 1000;

			map.put(params.origin, params.id, "troops", detail, op);

			const nextCell = this.nextMove(params.origin, params.objective);

			const taskParams = {
				mission: params,
				from: params.origin,
				to: nextCell,
				job: 'move',
				module: 'mission'
			};

			tasks.setTask({
				user: params.owner,
				time: 1,
				params: taskParams,
				visible: false,
				selector: 'mission:' + params.id
			}, op);
		});
	},
	nextMove(origin, objective) {
		let minDistance = maxDistance;

		for (let x = -1; x <= 1; x++) {
			for (let y = -1; y <= 1; y++) {
				if (x === 0 && y === 0) continue;

				const v = {
					d(to) {
						return Math.sqrt(Math.pow(this.x - to.x, 2) + Math.pow(this.y - to.y, 2));
					},
					equals(p) {
						return this.x === p.x && this.y === p.y;
					},

					x: origin.x + x,
					y: origin.y + y
				};

				if (v.d(objective) < minDistance) {
					nextCell = v;
					minDistance = v.d(objective);
				}
			}
		}

		return nextCell;
	},
	fullpath(p, t) {
		p = point(p);
		if (p.d(t) === 0) return 0;

		let curP = this.nextMove(p, t);
		let d = p.d(curP);

		while (!curP.equals(t)) {
			let newP = this.nextMove(curP, t);

			d += curP.d(newP);

			curP = newP;
		}

		return d;
	},
	move({ mission, from, to }, transaction) {
		let continueMission = true;
		let backHome = false;

		from = point(from);
		to = point(to);

		let distance = this.fullpath(to, mission.objective);
		mission.eta = distance / mission.speed;
		mission.t0 = new Date().getTime() / 1000;

		const { x: xa, y: ya } = to;
		const { x: xb, y: yb } = mission.objective;

		let executingAction = false;

		/*Path logic*/
		if (to.equals(mission.objective)) {
			if (mission.objective.action === "return") {
				console.log("Welcome home");
				backHome = true;
				continueMission = false;
			} else {
				executingAction = true;
			}
		}

		const keepMoving = trx => {

			if (executingAction) {
				const tmp = mission.objective;
				mission.objective = mission.origin;
				mission.origin = tmp;

				mission.objective.originalAction = mission.objective.action;
				mission.objective.action = 'return';

				distance = this.fullpath(to, mission.objective);
				mission.eta = distance / mission.speed;
				mission.t0 = new Date().getTime() / 1000;
			}

			if (continueMission) {
				const detail = {
					user: mission.user,
					id: mission.id,
					objective: mission.objective,
					loot: mission.loot !== null,
					eta: mission.eta
				};

				map.move("troops", mission.id, from, to, detail, trx);
				trx.addAction(op => {
					const nextCell = this.nextMove(to, mission.objective);
					const timeOfNextMove = nextCell.d(to) / mission.speed;

					const params = {
						mission,
						from: to,
						to: nextCell,
						job: 'move',
						module: 'mission'
					};

					tasks.setTask({
						user: mission.owner,
						time: timeOfNextMove,
						params,
						selector: 'mission:' + mission.id
					}, op);
				});
			}
		};

		if (backHome) {
			//Back home
			if (mission.loop) {
				transaction.addAction(op => {
					const { village, user, troops, path, loop, speed } = mission;
					const { action, origin: objective, objective: origin } = mission;

					const params = {
						troops, speed, action,
						origin, objective,
						path, loop,
						user, village,
						job: 'restart', module: 'mission'
					};
					tasks.setTask({
						user, time: 0, params,
						selector: 'restart-mission:' + mission.id
					}, op);
				});
			}

			this.endMission(mission, transaction);
			const fph = troops.movingTroops(mission, true, transaction);
			village.updatePopulation(fph, mission.village, transaction);

			const resources = mission.loot || mission.taxes;
			if (resources !== undefined) {

				const summary = core.villageSummary(mission);
				summary.locks.resources = true;

				village.load(summary, transaction);

				transaction.addAction(op => {
					village.adjustModel(summary);

					summary.resources.food.r0 += resources.food;
					summary.resources.iron.r0 += resources.iron;
					summary.resources.stone.r0 += resources.stone;
					summary.resources.wood.r0 += resources.wood;

					village.updateResources(summary, undefined, transaction);
				});
			}
		}

		keepMoving.from = from;

		transaction.addAction(op => {
			if (!executingAction) {
				keepMoving(op);
			} else {
				op.addAction(curOp => {
					if (mission.action === core.mission.raid || mission.action === core.mission.attack) {
						battle.attack(mission, keepMoving, this.endMission, this.saveReport, curOp);
					} else if (mission.action === core.mission.collect) {
						// Todo: !!! Update AllegianceTerms and collect maximum allowed

						village.collectTaxes(mission, curOp, diplomacy.updateTaxes);

						keepMoving(curOp);
					} else if (mission.action === core.mission.support) {

						this.support(mission.objective, mission, curOp);
						this.endMission(mission, curOp);
					}
				});

				console.log(`Mission.${ mission.id }.action executing`);
			}
		});
	},

	support({ user: ally, id: _village }, { user, troops: _troops }, transaction) {
		const fph = troops.movingTroops({ user, village: _village, troops: _troops }, true, transaction);
		village.updatePopulation(fph, _village, transaction);
	},

	endMission(attack, transaction) {
		dao.endMission(attack.id, transaction);
		map.remove("troops", attack.id, transaction);
	},

	saveReport(report, transaction) {
		dao.saveReport(report, transaction);
	}
};

module.exports = trx.transactionalModule(mission);