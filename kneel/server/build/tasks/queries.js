const queryManager = require('../lib/qm');

const startInterval = `COALESCE($2, '0second')::Interval`;

module.exports = queryManager({
	idSpace: 'tasks',
	queries: {
		getId: "select nextval('task_seq')",
		lock: "select * from tasks where id = $1",
		userTasks: `select	id, params, extract(epoch from current_timestamp) as current,
									extract(epoch from (completion_time-start)) as duration,
									extract(epoch from (completion_time-current_timestamp)) as remaining,
									start <= current_timestamp as started,
									start
						 from tasks t 
			 			where visible and "user" = $1 
          			order by completion_time asc`,
		load: `select t.*, extract(epoch from completion_time) as ct
				   from tasks t 
				  where extract( epoch from (completion_time - current_timestamp)::Interval ) < 5
				  order by ct asc;`,
		insert: `insert into tasks values ( $1, current_timestamp + ${ startInterval }, 
					current_timestamp + $3, $4, $5, $6, $7)`,
		delete: 'delete from tasks where id = $1',
		getBySelector: "select * from tasks where selector = $1",
		deleteBySelector: "delete from tasks where selector = $1",
		update: "update tasks set params = $2 where id = $1"
	}
});