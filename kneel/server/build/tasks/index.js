const dao = require('./dao');
const trx = require('../lib/transactional');

const tasks = {
	setTask({ user, time, params, visible, start, selector }, op) {
		let task = { user, params, time, visible, start, selector };

		task.visible = !(task.visible === null || task.visible === undefined) && task.visible;

		dao.setTask(task, op);
	},
	userTasks({ user, village }, callback) {
		dao.getUserTasks(user, village, callback);
	},
	getTaskBySelector(selector, container, transaction) {
		dao.getTaskBySelector(selector, container, transaction);
	},

	deleteTaskBySelector(selector, transaction) {
		dao.deleteTaskBySelector(selector, transaction);
	},

	update(task, transaction) {
		dao.update(task, transaction);
	}
};

module.exports = trx.transactionalModule(tasks);