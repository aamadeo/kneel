const express = require('express');
const router = express.Router();
const url = require('url');
const tasks = require('./index');
const checkAuth = require('../lib/checkAuth');

/* GET users listing. */
router.get('/', checkAuth, function (req, res, next) {

   tasks.userTasks(req.session, function (tasks) {
      res.send(JSON.stringify(tasks));
   });
});

module.exports = router;