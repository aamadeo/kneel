const dao = require('./dao');
const transactions = require('../lib/transactional');
const core = require('../core');

let tasksProcessed = new Map();
let lastTick = 0;

function performTask(task, transaction) {
	let module = require("../" + task.params.module);

	module[task.params.job](task.params, transaction);
}

function processRequests(tasks) {

	if (!(core.profiles.loaded && core.costs.loaded)) {
		return setTimeout(getTasks, 50);
	}

	let noRequests = true;
	let nextCall = 0;

	for (let i = 0; i < tasks.length; i++) {
		if (tasksProcessed.has(tasks[i].id)) continue;
		let task = tasks[i];

		noRequests = false;

		let transaction = transactions.transaction();
		dao.lock(task.id, transaction);

		performTask(task, transaction);

		dao.delete(task.id, transaction);

		let time = task.ct * 1000 - new Date().getTime();
		transaction.finally = status => {
			console.log("Task " + task.id + " ended with status : " + status);
		};

		setTimeout(transaction.run, time);
		tasksProcessed.set(task.id);
	}

	if (noRequests) {
		nextCall = 4000;
	}

	setTimeout(getTasks, nextCall);
}

function getTasks() {
	dao.getTasks(processRequests);
}

getTasks();