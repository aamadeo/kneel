const queryManager = require('../lib/qm');

module.exports = queryManager({
	queries: {
		createUser: 'insert into users values ($1, $2, $3, $4, 0)',
		userCount: 'select count(1) as count from users',
		loadUser: 'select * from users where "user" = $1',
		villages: 'select * from villages where "user" = $1',
		addPoints: 'update users set points = points + $2 where "user" = $1',
		users: `select "user", points from users where "user" <> 'free' order by points desc limit 20`,
		userCapacity: "select building, detail::jsonb->'level' from villages v, village_buildings vb where v.user = $1 and v.id = vb.village and vb.building in ('Barn','Warehouse','TownCenter')"
	}
});