const express = require('express');
const router = express.Router();
const url = require('url');
const user = require('./index');
const checkAuth = require('../lib/checkAuth');
const transactions = require('../lib/transactional');
const validator = require('./request.validator.js');

const respond = (res, object) => res.send(JSON.stringify(object));

/* GET users listing. */
router.get('/params', checkAuth, function (req, res, next) {
	try {
		const transaction = transactions.transaction();
		const config = {};
		user.config(req.session.user, config, transaction);

		transaction.finally = status => respond(res, status === 0 ? config : { errorCode: status });

		transaction.run();
	} catch (err) {
		responde(res, { errorCode: err });
	}
});

router.get('/free', function (req, res, next) {
	try {
		const url_parts = url.parse(req.url, true);
		const query = url_parts.query;
		const userInfo = { user: query.user };

		validator.free(userInfo);

		const transaction = transactions.transaction();

		user.find(userInfo, transaction);

		transaction.finally = status => {
			console.log({ status, userInfo });

			if (status === 0) respond(res, { free: !userInfo.exists });else respond(res, { errorCode: status });
		};

		transaction.run();
	} catch (err) {
		responde(res, { errorCode: err });
	}
});

router.post('/', function (req, res, next) {
	try {
		console.log({ body: req.body });
		validator.newUser(req.body);
		const userInfo = req.body;
		const transaction = transactions.transaction();

		user.newUser(userInfo, transaction);

		transaction.finally = status => {
			if (status == 0) respond(res, { ok: true });else respond(res, { errorCode: status });
		};

		transaction.run();
	} catch (err) {
		responde(res, { errorCode: err });
	}
});

router.get('/', function (req, res, next) {
	try {
		const users = [];
		const transaction = transactions.transaction();

		user.getUsers(users, transaction);

		transaction.finally = status => {
			if (status === 0) {
				respond(res, users);
			} else callback({ errorCode: status });
		};

		transaction.run();
	} catch (err) {
		responde(res, { errorCode: err });
	}
});

router.get('/:user/', checkAuth, function (req, res, next) {
	try {
		const transaction = transactions.transaction();
		const userInfo = { user: req.params.user };
		validator.getUser(userInfo);

		user.getUser(userInfo, transaction);

		transaction.finally = status => {
			if (status === 0) {
				delete userInfo.hash;
				delete userInfo.salt;
				delete userInfo.rounds;

				respond(res, userInfo);
			} else {
				respond(res, { errorCode: status });
			}
		};

		transaction.run();
	} catch (err) {
		responde(res, { errorCode: err });
	}
});

router.post('/login', function (req, res, next) {
	let post = req.body;
	let status = {
		authenticated: false
	};

	console.log("User: " + req.session.user);

	if (req.session.user) {
		status.user = req.session.user;
		status.village = req.session.village;
		status.villageName = req.session.villageName;
		status.authenticated = true;
		status.radius = 5;
		status.location = req.session.location;

		respond(res, status);
	} else {
		console.log("Login request", req.body);

		if (req.body.user === null) {
			return respond(res, status);
		}

		try {
			validator.login(req.body);
			console.log(req.body, "OK");
			const transaction = transactions.transaction();
			const result = {};

			user.auth(req, result, transaction);

			transaction.finally = status => {
				if (status === 0) {
					respond(res, result);
				} else {
					console.log({ status });
					respond(res, { errorCode: status });
				}
			};

			transaction.run();
		} catch (err) {
			console.log(err);
			respond(res, { errorCode: err });
		}
	}
});

router.post('/logout', checkAuth, function (req, res, next) {
	let status = { authenticated: false };

	delete req.session.user;
	delete req.session.village;
	delete req.session.villageName;
	delete req.session.location;

	console.log("Loging out");

	respond(res, status);
});

module.exports = router;