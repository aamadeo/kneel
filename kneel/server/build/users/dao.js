const trx = require('../lib/transactional');
const qm = require('./queries');
const core = require('../core');

module.exports = trx.transactionalModule({
	name: "users.dao",
	auth(username, user, transaction) {
		qm.addQuery({
			name: "loadUser",
			args: [username],
			resultHandler: result => {
				user.exists = result.rows.length > 0;

				if (!user.exists) return;

				user.user = username;
				user.hash = result.rows[0].hash;
				user.salt = result.rows[0].salt;
			}
		}, transaction);
	},

	getUser(userInfo, transaction) {
		qm.addQuery({
			name: "loadUser",
			args: [userInfo.user],
			resultHandler: result => {
				userInfo.exists = result.rows.length > 0;

				if (!userInfo.exists) return;

				userInfo.points = result.rows[0].points;
			}
		}, transaction);
	},

	addPoints(user, points, transaction) {
		qm.addSimpleQuery("addPoints", [user, points], transaction);
	},

	createUser(userInfo, transaction) {
		qm.addSimpleQuery("createUser", [userInfo.user, userInfo.hash, userInfo.salt, userInfo.rounds], transaction);
	},

	userVillages({ user: username, villages }, transaction) {
		qm.addQuery({
			name: "villages",
			args: [username],
			perRow: row => villages.push(row)
		}, transaction);
	},

	userCount(container, transaction) {
		qm.addQuery({
			name: "userCount",
			args: [],
			resultHandler: result => {
				container.count = result.rows[0].count;
			}
		}, transaction);
	},

	userCapacity(container, user, transaction) {
		qm.addQuery({
			name: "userCapacity",
			args: [user],
			perRow: row => {
				if (row.building === 'TownCenter') {
					container.whCapacity = (container.whCapacity || 0) + core.storage.deposit;
					container.brnCapacity = (container.brnCapacity || 0) + core.storage.deposit;
				} else if (row.building === 'Barn') {
					const increment = core.storage.base * Math.pow(core.storage.coeficient, parseInt(row.level));
					container.brnCapacity = (container.brnCapacity || 0) + increment;
				} else if (row.building === 'Warehouse') {
					const increment = core.storage.base * Math.pow(core.storage.coeficient, parseInt(row.level));
					container.whCapacity = (container.whCapacity || 0) + increment;
				}
			}
		}, transaction);
	},

	getUsers(users, transaction) {
		qm.addQuery({
			name: "users",
			args: [],
			perRow: row => users.push(row)
		}, transaction);
	}
});