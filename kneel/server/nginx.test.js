const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const compression = require('compression')
const fs = require('fs')
const http = require('http')
const PORT = process.env.PORT || 8080 
const server = http.createServer(app)

server.listen(PORT, function () {
	console.log("Listening on " + PORT)
})


app.use(compression())
app.use(function (req, res, next) {
	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true)
	// Pass to next layer of middleware
	next()
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: false
}))

app.use('/', (req, res) => {
	res.send(JSON.stringify({
		estado: 'OK'
	}))
})
