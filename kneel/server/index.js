let express = require('express')
let router = express.Router()

// import some new stuff
import React from 'react'
// we'll use this to render our app to an html string
import { renderToString } from 'react-dom/server'

// and these to match the url to routes and then render
import { match, RouterContext } from 'react-router'
import routes from '../modules/routes'
import auth from '../modules/Auth'

router.get('*', (req, res) => {
  console.log(req.url)
  auth.ssLoggedIn = req.session.user !== null
  
  match({ routes: routes, location: req.url }, (err, redirect, props) => {
    // in here we can make some decisions all at once
    if (err) {
      // there was an error somewhere during route matching
      res.status(500).send(err.message)
    } else if (redirect) {
      // we haven't talked about `onEnter` hooks on routes, but before a
      // route is entered, it can redirect. Here we handle on the server.
      res.redirect(redirect.pathname + redirect.search)
    } else if (props) {
      // if we got props then we matched a route and can render
      const appHtml = renderToString(<RouterContext {...props}/>)
      res.send(renderPage(appHtml))
    } else {
      // no errors, no redirect, we just didn't match anything
      res.status(404).send('Not Found')
    }
  })
})

function renderPage(appHtml) {
  console.log("Rendering app on server")
  return `
<!doctype html public "storage">
<html>
    <head>
        <meta charset=utf-8/>
        <title>Kneel</title>
        <link rel="stylesheet" href="/css/kneel.css" />
        <link rel="stylesheet" href="/css/village.css" />
        <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="/js/core.js"></script>
    </head>
    <body>
        <div id=app></div>
        <script src="/bundle.js"></script>
    </body>
</html>
   `
}

module.exports = router