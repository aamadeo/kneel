'use strict';
 
require('greenlock-express').create({
 
  server: 'staging'
 
, email: 'aamadeo@gmail.com'
 
, agreeTos: true
 
, approveDomains: [ 's1.aamadeo.me' ]
 
, app: require('express')().use('/', function (req, res) {
    res.end('Hello, World!');
  })
 
}).listen(80, 443);
