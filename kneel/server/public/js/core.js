var core = {
	user: null,
	village: null,
	speed: 1,
	rps: { base: 0.002014966667, coeficient: 1.3 },
	price: { coeficient: 1.35 },
	cellSize: 9.1,
	troopsNames: ["infantry","pikeman","archer","heavy-archer","scout","knight","ram","catapult","trebuchet"],
	buildingsNames: ["Market","Archery","Barn","Barracks","Stable","Warehouse","SiegeWeaponFactory","University","Blacksmith"],
	troops: {},
	techEnabled( techName, techState ){
		return techState.available.hasOwnProperty(techName)
	},

	techAvailable(techName, techState){
		if ( techState === undefined ) return false

		const { available, all } = techState
		let techAvailable = true

		all[techName].dependencies.forEach( dependency => {
			techAvailable = techAvailable && ! (available[dependency] === undefined)
		})

		return techAvailable
	},
	getCostOf(name, level, costs){
		
		if ( typeof level == "undefined" ) level = 0
		
		var multiplier = Math.pow( core.price.coeficient, level )

		var cost =  {
			food: multiplier * costs[name].food,
			iron: multiplier * costs[name].iron,
			stone: multiplier * costs[name].stone,
			wood: multiplier * costs[name].wood,
			time: (multiplier * parseInt(costs[name].time) / 60),
		}

		return cost
	},
	available(r) {
		let t = new Date().getTime()
		const dt = ( t - r.t0) / 1000
		const tmp = r.r0 + dt * r.rps

		return Math.max(0, Math.min(tmp, r.capacity))
	},
	p2Str: ({x,y}) => `p${x}.${y}`,
	totalResources: resources => Object.keys(resources).reduce( 
		(t,r) => t + resources[r], 
		0
	)
}