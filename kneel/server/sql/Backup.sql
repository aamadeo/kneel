﻿--
-- TOC entry 203 (class 1259 OID 16693)
-- Name: allegiance_terms; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE allegiance_terms (
    king text NOT NULL,
    subject text NOT NULL,
    tax double precision NOT NULL,
    "whResources" double precision,
    "brnResources" double precision,
    fisotk integer NOT NULL,
    "forceAvailable" double precision,
    status text NOT NULL
);


ALTER TABLE allegiance_terms OWNER TO kneel;

--
-- TOC entry 181 (class 1259 OID 16396)
-- Name: costs; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE costs (
    item text NOT NULL,
    food double precision,
    iron double precision,
    stone double precision,
    wood double precision,
    "time" bigint
);


ALTER TABLE costs OWNER TO kneel;

--
-- TOC entry 182 (class 1259 OID 16402)
-- Name: dual; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE dual (
    t text NOT NULL
);


ALTER TABLE dual OWNER TO kneel;

--
-- TOC entry 198 (class 1259 OID 16627)
-- Name: map; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE map (
    xs integer NOT NULL,
    ys integer NOT NULL,
    type text NOT NULL,
    object bigint NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    detail json
);


ALTER TABLE map OWNER TO kneel;

--
-- TOC entry 194 (class 1259 OID 16579)
-- Name: mission_seq; Type: SEQUENCE; Schema: public; Owner: kneel
--

CREATE SEQUENCE mission_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mission_seq OWNER TO kneel;

--
-- TOC entry 183 (class 1259 OID 16414)
-- Name: mission_troops; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE mission_troops (
    mission bigint NOT NULL,
    troop text NOT NULL,
    count integer DEFAULT 0 NOT NULL
);


ALTER TABLE mission_troops OWNER TO kneel;

--
-- TOC entry 184 (class 1259 OID 16421)
-- Name: missions; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE missions (
    "user" text NOT NULL,
    mission bigint NOT NULL,
    action text NOT NULL,
    details json
);


ALTER TABLE missions OWNER TO kneel;

--
-- TOC entry 185 (class 1259 OID 16427)
-- Name: path; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE path (
    mission bigint NOT NULL,
    step integer NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL
);


ALTER TABLE path OWNER TO kneel;

--
-- TOC entry 196 (class 1259 OID 16604)
-- Name: reports; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE reports (
    mission bigint NOT NULL,
    detail json
);


ALTER TABLE reports OWNER TO kneel;

--
-- TOC entry 193 (class 1259 OID 16577)
-- Name: task_seq; Type: SEQUENCE; Schema: public; Owner: kneel
--

CREATE SEQUENCE task_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE task_seq OWNER TO kneel;

--
-- TOC entry 195 (class 1259 OID 16590)
-- Name: tasks; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE tasks (
    id bigint NOT NULL,
    start timestamp without time zone NOT NULL,
    completion_time timestamp without time zone NOT NULL,
    "user" text,
    params json,
    visible boolean DEFAULT false NOT NULL,
    selector text
);


ALTER TABLE tasks OWNER TO kneel;

--
-- TOC entry 199 (class 1259 OID 16636)
-- Name: technologies; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE technologies (
    name text NOT NULL,
    description text
);


ALTER TABLE technologies OWNER TO kneel;

--
-- TOC entry 200 (class 1259 OID 16644)
-- Name: technology_dependencies; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE technology_dependencies (
    technology text NOT NULL,
    dependency text NOT NULL
);


ALTER TABLE technology_dependencies OWNER TO kneel;

--
-- TOC entry 186 (class 1259 OID 16442)
-- Name: troop_profiles; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE troop_profiles (
    troop text NOT NULL,
    profile json
);


ALTER TABLE troop_profiles OWNER TO kneel;

--
-- TOC entry 197 (class 1259 OID 16610)
-- Name: user_reports; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE user_reports (
    "user" text NOT NULL,
    mission bigint NOT NULL
);


ALTER TABLE user_reports OWNER TO kneel;

--
-- TOC entry 201 (class 1259 OID 16667)
-- Name: user_technologies; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE user_technologies (
    technology text NOT NULL,
    "user" text NOT NULL,
    level integer DEFAULT '-1'::integer NOT NULL,
    "nextLevel" integer DEFAULT 0 NOT NULL
);


ALTER TABLE user_technologies OWNER TO kneel;

--
-- TOC entry 187 (class 1259 OID 16454)
-- Name: users; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE users (
    "user" text NOT NULL,
    hash text,
    salt text,
    rounds integer,
    points integer
);


ALTER TABLE users OWNER TO kneel;

--
-- TOC entry 188 (class 1259 OID 16460)
-- Name: village_buildings; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE village_buildings (
    village bigint NOT NULL,
    slot integer NOT NULL,
    building text NOT NULL,
    detail json,
    free_after timestamp without time zone
);


ALTER TABLE village_buildings OWNER TO kneel;

--
-- TOC entry 189 (class 1259 OID 16466)
-- Name: village_occupations; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE village_occupations (
    village bigint NOT NULL,
    occupation text NOT NULL,
    workers integer DEFAULT 0 NOT NULL,
    level integer DEFAULT 0 NOT NULL
);


ALTER TABLE village_occupations OWNER TO kneel;

--
-- TOC entry 190 (class 1259 OID 16474)
-- Name: village_resources; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE village_resources (
    village bigint NOT NULL,
    name text NOT NULL,
    r0 double precision DEFAULT 0 NOT NULL,
    t0 timestamp without time zone NOT NULL
);


ALTER TABLE village_resources OWNER TO kneel;

--
-- TOC entry 202 (class 1259 OID 16691)
-- Name: village_seq; Type: SEQUENCE; Schema: public; Owner: kneel
--

CREATE SEQUENCE village_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE village_seq OWNER TO kneel;

--
-- TOC entry 191 (class 1259 OID 16481)
-- Name: village_troops; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE village_troops (
    village bigint NOT NULL,
    troop text NOT NULL,
    count integer DEFAULT 0,
    upgrades json
);


ALTER TABLE village_troops OWNER TO kneel;

--
-- TOC entry 192 (class 1259 OID 16488)
-- Name: villages; Type: TABLE; Schema: public; Owner: kneel
--

CREATE TABLE villages (
    id bigint NOT NULL,
    "user" text NOT NULL,
    name text NOT NULL,
    x integer,
    y integer,
    population double precision DEFAULT 45 NOT NULL
);


ALTER TABLE villages OWNER TO kneel;

--
-- TOC entry 2090 (class 2606 OID 16496)
-- Name: costs_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY costs
    ADD CONSTRAINT costs_pkey PRIMARY KEY (item);


--
-- TOC entry 2092 (class 2606 OID 16498)
-- Name: dual_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY dual
    ADD CONSTRAINT dual_pkey PRIMARY KEY (t);


--
-- TOC entry 2121 (class 2606 OID 16701)
-- Name: map_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY map
    ADD CONSTRAINT map_pkey PRIMARY KEY (type, object);


--
-- TOC entry 2094 (class 2606 OID 16502)
-- Name: mission_troops_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY mission_troops
    ADD CONSTRAINT mission_troops_pkey PRIMARY KEY (mission, troop);


--
-- TOC entry 2096 (class 2606 OID 16504)
-- Name: missions_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY missions
    ADD CONSTRAINT missions_pkey PRIMARY KEY (mission);


--
-- TOC entry 2098 (class 2606 OID 16506)
-- Name: path_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY path
    ADD CONSTRAINT path_pkey PRIMARY KEY (mission, step);


--
-- TOC entry 2117 (class 2606 OID 16617)
-- Name: reports_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (mission);


--
-- TOC entry 2115 (class 2606 OID 16598)
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 2125 (class 2606 OID 16643)
-- Name: technologies_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY technologies
    ADD CONSTRAINT technologies_pkey PRIMARY KEY (name);


--
-- TOC entry 2127 (class 2606 OID 16651)
-- Name: technology_dependencies_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY technology_dependencies
    ADD CONSTRAINT technology_dependencies_pkey PRIMARY KEY (technology, dependency);


--
-- TOC entry 2100 (class 2606 OID 16512)
-- Name: troops_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY troop_profiles
    ADD CONSTRAINT troops_profile_pkey PRIMARY KEY (troop);


--
-- TOC entry 2119 (class 2606 OID 16619)
-- Name: user_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY user_reports
    ADD CONSTRAINT user_reports_pkey PRIMARY KEY ("user", mission);


--
-- TOC entry 2129 (class 2606 OID 16676)
-- Name: user_technologies_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY user_technologies
    ADD CONSTRAINT user_technologies_pkey PRIMARY KEY ("user", technology);


--
-- TOC entry 2102 (class 2606 OID 16516)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY ("user");


--
-- TOC entry 2104 (class 2606 OID 16518)
-- Name: village_buildings_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_buildings
    ADD CONSTRAINT village_buildings_pkey PRIMARY KEY (village, slot);


--
-- TOC entry 2106 (class 2606 OID 16520)
-- Name: village_occupations_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_occupations
    ADD CONSTRAINT village_occupations_pkey PRIMARY KEY (village, occupation);


--
-- TOC entry 2108 (class 2606 OID 16522)
-- Name: village_resources_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_resources
    ADD CONSTRAINT village_resources_pkey PRIMARY KEY (village, name);


--
-- TOC entry 2110 (class 2606 OID 16524)
-- Name: village_troops_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_troops
    ADD CONSTRAINT village_troops_pkey PRIMARY KEY (village, troop);


--
-- TOC entry 2112 (class 2606 OID 16526)
-- Name: villages_pkey; Type: CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY villages
    ADD CONSTRAINT villages_pkey PRIMARY KEY (id);


--
-- TOC entry 2122 (class 1259 OID 16699)
-- Name: map_primary_index; Type: INDEX; Schema: public; Owner: kneel
--

CREATE INDEX map_primary_index ON map USING btree (xs NULLS FIRST, ys NULLS FIRST);


--
-- TOC entry 2123 (class 1259 OID 16635)
-- Name: map_xs_ys_idx; Type: INDEX; Schema: public; Owner: kneel
--

CREATE INDEX map_xs_ys_idx ON map USING btree (xs, ys);


--
-- TOC entry 2113 (class 1259 OID 16528)
-- Name: villages_user_idx; Type: INDEX; Schema: public; Owner: kneel
--

CREATE INDEX villages_user_idx ON villages USING btree ("user");


--
-- TOC entry 2130 (class 2606 OID 16529)
-- Name: mission_troops_mission_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY mission_troops
    ADD CONSTRAINT mission_troops_mission_fkey FOREIGN KEY (mission) REFERENCES missions(mission) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2131 (class 2606 OID 16534)
-- Name: missions_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY missions
    ADD CONSTRAINT missions_user_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2132 (class 2606 OID 16539)
-- Name: path_mission_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY path
    ADD CONSTRAINT path_mission_fkey FOREIGN KEY (mission) REFERENCES missions(mission) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2137 (class 2606 OID 16599)
-- Name: tasks_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_users_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2139 (class 2606 OID 16657)
-- Name: technology_dependencies_dependency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY technology_dependencies
    ADD CONSTRAINT technology_dependencies_dependency_fkey FOREIGN KEY (dependency) REFERENCES technologies(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2138 (class 2606 OID 16652)
-- Name: technology_dependencies_technology_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY technology_dependencies
    ADD CONSTRAINT technology_dependencies_technology_fkey FOREIGN KEY (technology) REFERENCES technologies(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2141 (class 2606 OID 16682)
-- Name: user_technologies_technology_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY user_technologies
    ADD CONSTRAINT user_technologies_technology_fkey FOREIGN KEY (technology) REFERENCES technologies(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2140 (class 2606 OID 16677)
-- Name: user_technologies_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY user_technologies
    ADD CONSTRAINT user_technologies_user_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2133 (class 2606 OID 16549)
-- Name: village_buildings_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_buildings
    ADD CONSTRAINT village_buildings_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2134 (class 2606 OID 16554)
-- Name: village_occupations_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_occupations
    ADD CONSTRAINT village_occupations_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2135 (class 2606 OID 16559)
-- Name: village_resources_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_resources
    ADD CONSTRAINT village_resources_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2136 (class 2606 OID 16564)
-- Name: village_troops_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kneel
--

ALTER TABLE ONLY village_troops
    ADD CONSTRAINT village_troops_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-11-17 23:04:47

--
-- PostgreSQL database dump complete
--

