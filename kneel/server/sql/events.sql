﻿-- System Tasks
insert into tasks values ( 1, current_timestamp, current_timestamp, null, '{ "module": "village", "job" : "rvUpdate" }');
insert into tasks values ( 0, current_timestamp, current_timestamp, null, '{ "module": "events", "job" : "deleteOldEvents" }')

select * from tasks

-- User Events
insert into event_subscriptions values ('new-report:ui', 'ui')
insert into event_subscriptions values ('update-report:ui', 'ui')
insert into event_subscriptions values ('task-start:ui', 'ui')
insert into event_subscriptions values ('task-end:ui', 'ui')

-- Village Eventes
insert into event_subscriptions values ('resource-update:2', 'ui')
insert into event_subscriptions values ('village-update:2', 'ui')

with a as (select avatar from users where "user" = 'u1')
update map m set detail = detail::jsonb || ('{ "avatar": "' || a.avatar  || '" }' )::jsonb
from a
where object = -1
select a.avatar, m.* from a, map m
