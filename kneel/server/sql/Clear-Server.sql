﻿
delete from village_occupations;
delete from village_troops;
delete from village_buildings;
delete from village_resources;
delete from user_reports;
delete from reports;
delete from villages;
delete from tasks where not ("user" is null);
delete from mission_troops;
delete from path;
delete from missions;
delete from map;
delete from user_technologies;
delete from event_subscriptions;
delete from users where "user" <> 'free';

