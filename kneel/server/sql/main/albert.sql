﻿select * from users;
-- insert into users values ('albert', '$2a$12$YrPiJQr0lSh9ukKgVAV.muP.FFFu2qTTxtjCeMk2BloK4LMQyCA2C', '$2a$12$YrPiJQr0lSh9ukKgVAV.mu', 12);
insert into villages values ( 0, 'albert', 'lcdth', 0, 0, 10);

update villages set population = 12 where id = 0

delete from village_occupations where village = 0;
insert into village_occupations values ( 0, 'builders', 0, 0);
insert into village_occupations values ( 0, 'food', 3, 0);
insert into village_occupations values ( 0, 'iron', 3, 0);
insert into village_occupations values ( 0, 'stone', 3, 0);
insert into village_occupations values ( 0, 'wood', 3, 0);
insert into village_occupations values ( 0, 'unoccupied', 3, 0);

delete from village_buildings where village = 0;
insert into village_buildings values (0, 12, 'TownCenter', '{ "level" : "0" }');

insert into map values (0,0, 'village', 0, 0, 0, '{"owner": "albert", "name" : "lcdth" }');

update village_resources set r0 = 1000, t0 = current_timestamp where village = 0;
insert into village_resources values (0, 'food', 1000, current_timestamp);
insert into village_resources values (0, 'iron', 1000, current_timestamp);
insert into village_resources values (0, 'stone', 1000, current_timestamp);
insert into village_resources values (0, 'wood', 1000, current_timestamp);

update village_troops set count = 0 where village = 0;
insert into village_troops values (0, 'infantry', 10000, '{}');
insert into village_troops values (0, 'pikeman', 100, '{}');
insert into village_troops values (0, 'archer', 100, '{}');
insert into village_troops values (0, 'heavy-archer', 100, '{}');
insert into village_troops values (0, 'scout', 100, '{}');
insert into village_troops values (0, 'knight', 100, '{}');
insert into village_troops values (0, 'ram', 100, '{}');
insert into village_troops values (0, 'catapult', 100, '{}');
insert into village_troops values (0, 'trebuchet', 100, '{}');

/*
delete from path;
delete from mission_troops;
delete from map where type = 'troops';
delete from tasks where not visible;

delete from missions;

select mt.*, m..action, m.details from missions m, mission_troops mt where m.user = 'albert' and m.mission = mt.mission order by mt.mission

select * from tasks where params::jsonb @> '{"type": "ram"}'::jsonb
select * from missions;
select * from path;
select * from mission_troops;
select * from map
select * from village_troops where village = 1

update village_troops set count = 1 where village = 1

update villages set population = 50

select * from reports

select * from tasks

select * from village_occupations
select * from reports
select * from village_troops  where village = 1
"{"time":"2016-08-17T19:20:08.163Z","attack":["albert"],"defense":["trotte"],"troops":{"attack":{"original":{"infantry":{"count":10,"fph":1},"pikeman":{"count":0,"fph":0.8},"archer":{"count":0,"fph":0.8},"heavy-archer":{"count":0,"fph":1.1},"scout":{"count" (...)"


delete from tasks;
update village_buildings set free_after = current_timestamp;
vacuum tasks
update village_resources set r0 = 10000

select current_timestamp, free_after from village_buildings where village = 0 and slot = 7

select	id, extract(epoch from (completion_time-start)) as duration,
	extract(epoch from (completion_time-current_timestamp)) as remaining,
	start,
	completion_time
 from tasks t 
where visible and "user" = 'albert'
order by completion_time asc
*/
