﻿-- "esta 2"
--insert into users ("user", hash, rounds ,salt ) values ('trotte', '$2a$12$GXUcL6a.8sk4LNZGhWe2FuAGxvRO2dA4bDZyJRkceiovfGwAtBIUy',12,'$2a$12$GXUcL6a.8sk4LNZGhWe2Fu');
insert into villages values ( 1, 'trotte', 'kambala', -3, 4, 40);

insert into map values ( 0,0, 'village', 1, -3,4,  '{"owner": "trotte", "name" : "kambala"}');

insert into village_occupations values ( 1, 'builders', 0, 9);
insert into village_occupations values ( 1, 'food', 10, 9);
insert into village_occupations values ( 1, 'iron', 10, 9);
insert into village_occupations values ( 1, 'stone', 10, 9);
insert into village_occupations values ( 1, 'wood', 10, 9);
insert into village_occupations values ( 1, 'unoccupied', 0, 0);

insert into village_buildings values ( 1, 12, 'TownCenter', '{ "level" : "1" }');
insert into village_buildings values ( 1, 0, 'Barn', '{ "level" : "8" }');
insert into village_buildings values ( 1, 1, 'Barn', '{ "level" : "8" }');
insert into village_buildings values ( 1, 2, 'Warehouse', '{ "level" : "8" }');
insert into village_buildings values ( 1, 3, 'Warehouse', '{ "level" : "8" }');
insert into village_buildings values ( 1, 4, 'Archery', '{ "level" : "1" }');
insert into village_buildings values ( 1, 5, 'Stable', '{ "level" : "1" }');
insert into village_buildings values ( 1, 6, 'Barracks', '{ "level" : "1" }');
insert into village_buildings values ( 1, 7, 'SiegeWeaponFactory', '{ "level" : "1" }');


insert into village_resources values ( 1, 'food', 1000, current_timestamp);
insert into village_resources values ( 1, 'iron', 1000, current_timestamp);
insert into village_resources values ( 1, 'stone', 1000, current_timestamp);
insert into village_resources values ( 1, 'wood', 1000, current_timestamp);

insert into village_troops values ( 1, 'infantry', 10000, '{}');
insert into village_troops values ( 1, 'pikeman', 100, '{}');
insert into village_troops values ( 1, 'archer', 100, '{}');
insert into village_troops values ( 1, 'heavy-archer', 100, '{}');
insert into village_troops values ( 1, 'scout', 100, '{}');
insert into village_troops values ( 1, 'knight', 100, '{}');
insert into village_troops values ( 1, 'ram', 100, '{}');
insert into village_troops values ( 1, 'catapult', 100, '{}');
insert into village_troops values ( 1, 'trebuchet', 100, '{}');
