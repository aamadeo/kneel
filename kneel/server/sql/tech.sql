﻿truncate technology_dependencies;
delete from technologies;

-- units
insert into technologies values ('trebuchet','');
insert into technologies values ('catapult',''); 
insert into technologies values ('ram',''); 
insert into technologies values ('infantry','');
insert into technologies values ('pikeman','');
insert into technologies values ('scout',''); 
insert into technologies values ('knight','');
insert into technologies values ('archer','');
insert into technologies values ('heavy-archer','');
insert into technologies values ('heavy-infantry','');
insert into technologies values ( 'peasant','');
insert into technologies values ( 'trade-cart','');

-- buildings
insert into technologies values ('Warehouse','');
insert into technologies values ('Barn','');
insert into technologies values ('Archery','');
insert into technologies values ('Barracks','');
insert into technologies values ('Stable','');
insert into technologies values ('Tower','');
insert into technologies values ('SiegeWeaponFactory','');
insert into technologies values ('University','');
insert into technologies values ('Blacksmith','');
insert into technologies values ('TownCenter','');
insert into technologies values ('Market','');

-- resources technology
insert into technologies values ( 'foodTech','');
insert into technologies values ( 'woodTech','');
insert into technologies values ( 'ironTech','');
insert into technologies values ( 'stoneTech','');
insert into technologies values ( 'buildersTech','');

-- *** R E S E A R C H *** ---
insert into technologies values ('Siege', '');
insert into technologies values ('Archery Research', '');
insert into technologies values ('Stable Research', '');
insert into technologies values ('Blacksmith Research', '');

-- Elite Training
insert into technologies values ('Elite Training', '');
insert into technologies values ('Heavy-Archer Research', '');
insert into technologies values ('Heavy-Infantry Research', '');
insert into technologies values ('Knight Research', '');

-- Siege
insert into technologies values ('Ram Research', '');
insert into technologies values ('Catapult Research', '');
insert into technologies values ('Trebuchet Research', '');

insert into technology_dependencies values ('Ram Research', 'Siege');
insert into technology_dependencies values ('Catapult Research', 'Siege');
insert into technology_dependencies values ('Trebuchet Research', 'Siege');

insert into technology_dependencies values ('SiegeWeaponFactory', 'Siege');
insert into technology_dependencies values ('Archery', 'Archery Research');
insert into technology_dependencies values ('Stable', 'Stable Research');
insert into technology_dependencies values ('Blacksmith', 'Blacksmith Research');

insert into technology_dependencies values ('Heavy-Archer Research', 'Elite Training');
insert into technology_dependencies values ('Heavy-Infantry Research', 'Elite Training');
insert into technology_dependencies values ('Knight Research', 'Elite Training');
insert into technology_dependencies values ('Knight Research', 'Heavy-Infantry Research');

insert into technology_dependencies values ('ram','Ram Research');
insert into technology_dependencies values ('catapult','Catapult Research');
insert into technology_dependencies values ('trebuchet','Trebuchet Research');

insert into technology_dependencies values ('heavy-archer','Heavy-Archer Research');
insert into technology_dependencies values ('heavy-infantry','Heavy-Infantry Research');
insert into technology_dependencies values ('knight','Knight Research');

