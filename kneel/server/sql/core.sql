﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

-- Started on 2016-12-22 15:40:14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 24577)
-- Name: allegiance_terms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE allegiance_terms (
    king text NOT NULL,
    subject text NOT NULL,
    tax double precision NOT NULL,
    "whResources" double precision,
    "brnResources" double precision,
    fisotk integer NOT NULL,
    "forceAvailable" double precision,
    status text NOT NULL
);


--
-- TOC entry 182 (class 1259 OID 24583)
-- Name: costs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE costs (
    item text NOT NULL,
    food double precision,
    iron double precision,
    stone double precision,
    wood double precision,
    "time" bigint
);


--
-- TOC entry 183 (class 1259 OID 24589)
-- Name: dual; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dual (
    t text NOT NULL
);


--
-- TOC entry 206 (class 1259 OID 24870)
-- Name: event_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE event_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 205 (class 1259 OID 24855)
-- Name: event_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE event_subscriptions (
    event_key text NOT NULL,
    "user" text NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 24836)
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE events (
    event_key text,
    id integer NOT NULL,
    details json,
    validity timestamp without time zone,
    owner text
);


--
-- TOC entry 184 (class 1259 OID 24595)
-- Name: map; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE map (
    xs integer NOT NULL,
    ys integer NOT NULL,
    type text NOT NULL,
    object bigint NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    detail json
);


--
-- TOC entry 185 (class 1259 OID 24601)
-- Name: mission_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mission_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 186 (class 1259 OID 24603)
-- Name: mission_troops; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mission_troops (
    mission bigint NOT NULL,
    troop text NOT NULL,
    count integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 187 (class 1259 OID 24610)
-- Name: missions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE missions (
    "user" text NOT NULL,
    mission bigint NOT NULL,
    action text NOT NULL,
    details json
);


--
-- TOC entry 188 (class 1259 OID 24616)
-- Name: path; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE path (
    mission bigint NOT NULL,
    step integer NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL
);


--
-- TOC entry 189 (class 1259 OID 24619)
-- Name: reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE reports (
    mission bigint NOT NULL,
    detail json
);


--
-- TOC entry 190 (class 1259 OID 24625)
-- Name: task_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE task_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1
    CYCLE;


--
-- TOC entry 191 (class 1259 OID 24627)
-- Name: tasks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tasks (
    id bigint NOT NULL,
    start timestamp without time zone NOT NULL,
    completion_time timestamp without time zone NOT NULL,
    "user" text,
    params json,
    visible boolean DEFAULT false NOT NULL,
    selector text
);


--
-- TOC entry 192 (class 1259 OID 24634)
-- Name: technologies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE technologies (
    name text NOT NULL,
    description text
);


--
-- TOC entry 193 (class 1259 OID 24640)
-- Name: technology_dependencies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE technology_dependencies (
    technology text NOT NULL,
    dependency text NOT NULL
);


--
-- TOC entry 194 (class 1259 OID 24646)
-- Name: troop_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE troop_profiles (
    troop text NOT NULL,
    profile json
);


--
-- TOC entry 195 (class 1259 OID 24652)
-- Name: user_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_reports (
    "user" text NOT NULL,
    mission bigint NOT NULL
);


--
-- TOC entry 196 (class 1259 OID 24658)
-- Name: user_technologies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_technologies (
    technology text NOT NULL,
    "user" text NOT NULL,
    level integer DEFAULT '-1'::integer NOT NULL,
    "nextLevel" integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 197 (class 1259 OID 24666)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    "user" text NOT NULL,
    hash text,
    salt text,
    rounds integer,
    points integer
);


--
-- TOC entry 198 (class 1259 OID 24672)
-- Name: village_buildings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE village_buildings (
    village bigint NOT NULL,
    slot integer NOT NULL,
    building text NOT NULL,
    detail json,
    free_after timestamp without time zone
);


--
-- TOC entry 199 (class 1259 OID 24678)
-- Name: village_occupations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE village_occupations (
    village bigint NOT NULL,
    occupation text NOT NULL,
    workers integer DEFAULT 0 NOT NULL,
    level integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 200 (class 1259 OID 24686)
-- Name: village_resources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE village_resources (
    village bigint NOT NULL,
    name text NOT NULL,
    r0 double precision DEFAULT 0 NOT NULL,
    t0 timestamp without time zone NOT NULL
);


--
-- TOC entry 201 (class 1259 OID 24693)
-- Name: village_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE village_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 202 (class 1259 OID 24695)
-- Name: village_troops; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE village_troops (
    village bigint NOT NULL,
    troop text NOT NULL,
    count integer DEFAULT 0,
    upgrades json
);


--
-- TOC entry 203 (class 1259 OID 24702)
-- Name: villages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE villages (
    id bigint NOT NULL,
    "user" text NOT NULL,
    name text NOT NULL,
    x integer,
    y integer,
    population double precision DEFAULT 45 NOT NULL
);


--
-- TOC entry 2102 (class 2606 OID 24710)
-- Name: costs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY costs
    ADD CONSTRAINT costs_pkey PRIMARY KEY (item);


--
-- TOC entry 2104 (class 2606 OID 24712)
-- Name: dual_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY dual
    ADD CONSTRAINT dual_pkey PRIMARY KEY (t);


--
-- TOC entry 2145 (class 2606 OID 24862)
-- Name: event_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_subscriptions
    ADD CONSTRAINT event_subscriptions_pkey PRIMARY KEY (event_key, "user");


--
-- TOC entry 2143 (class 2606 OID 24869)
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 24714)
-- Name: map_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY map
    ADD CONSTRAINT map_pkey PRIMARY KEY (type, object);


--
-- TOC entry 2110 (class 2606 OID 24716)
-- Name: mission_troops_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mission_troops
    ADD CONSTRAINT mission_troops_pkey PRIMARY KEY (mission, troop);


--
-- TOC entry 2112 (class 2606 OID 24718)
-- Name: missions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY missions
    ADD CONSTRAINT missions_pkey PRIMARY KEY (mission);


--
-- TOC entry 2114 (class 2606 OID 24720)
-- Name: path_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY path
    ADD CONSTRAINT path_pkey PRIMARY KEY (mission, step);


--
-- TOC entry 2116 (class 2606 OID 24722)
-- Name: reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (mission);


--
-- TOC entry 2118 (class 2606 OID 24724)
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 24726)
-- Name: technologies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY technologies
    ADD CONSTRAINT technologies_pkey PRIMARY KEY (name);


--
-- TOC entry 2122 (class 2606 OID 24728)
-- Name: technology_dependencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY technology_dependencies
    ADD CONSTRAINT technology_dependencies_pkey PRIMARY KEY (technology, dependency);


--
-- TOC entry 2124 (class 2606 OID 24730)
-- Name: troops_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY troop_profiles
    ADD CONSTRAINT troops_profile_pkey PRIMARY KEY (troop);


--
-- TOC entry 2126 (class 2606 OID 24732)
-- Name: user_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_reports
    ADD CONSTRAINT user_reports_pkey PRIMARY KEY ("user", mission);


--
-- TOC entry 2128 (class 2606 OID 24734)
-- Name: user_technologies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_technologies
    ADD CONSTRAINT user_technologies_pkey PRIMARY KEY ("user", technology);


--
-- TOC entry 2130 (class 2606 OID 24736)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY ("user");


--
-- TOC entry 2132 (class 2606 OID 24738)
-- Name: village_buildings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_buildings
    ADD CONSTRAINT village_buildings_pkey PRIMARY KEY (village, slot);


--
-- TOC entry 2134 (class 2606 OID 24740)
-- Name: village_occupations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_occupations
    ADD CONSTRAINT village_occupations_pkey PRIMARY KEY (village, occupation);


--
-- TOC entry 2136 (class 2606 OID 24742)
-- Name: village_resources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_resources
    ADD CONSTRAINT village_resources_pkey PRIMARY KEY (village, name);


--
-- TOC entry 2138 (class 2606 OID 24744)
-- Name: village_troops_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_troops
    ADD CONSTRAINT village_troops_pkey PRIMARY KEY (village, troop);


--
-- TOC entry 2140 (class 2606 OID 24746)
-- Name: villages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY villages
    ADD CONSTRAINT villages_pkey PRIMARY KEY (id);


--
-- TOC entry 2107 (class 1259 OID 24747)
-- Name: map_primary_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX map_primary_index ON map USING btree (xs NULLS FIRST, ys NULLS FIRST);


--
-- TOC entry 2108 (class 1259 OID 24748)
-- Name: map_xs_ys_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX map_xs_ys_idx ON map USING btree (xs, ys);


--
-- TOC entry 2141 (class 1259 OID 24749)
-- Name: villages_user_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX villages_user_idx ON villages USING btree ("user");


--
-- TOC entry 2158 (class 2606 OID 24863)
-- Name: event_subscriptions_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_subscriptions
    ADD CONSTRAINT event_subscriptions_user_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2146 (class 2606 OID 24750)
-- Name: mission_troops_mission_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mission_troops
    ADD CONSTRAINT mission_troops_mission_fkey FOREIGN KEY (mission) REFERENCES missions(mission) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2147 (class 2606 OID 24755)
-- Name: missions_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY missions
    ADD CONSTRAINT missions_user_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2148 (class 2606 OID 24760)
-- Name: path_mission_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY path
    ADD CONSTRAINT path_mission_fkey FOREIGN KEY (mission) REFERENCES missions(mission) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2149 (class 2606 OID 24765)
-- Name: tasks_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_users_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2150 (class 2606 OID 24770)
-- Name: technology_dependencies_dependency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY technology_dependencies
    ADD CONSTRAINT technology_dependencies_dependency_fkey FOREIGN KEY (dependency) REFERENCES technologies(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2151 (class 2606 OID 24775)
-- Name: technology_dependencies_technology_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY technology_dependencies
    ADD CONSTRAINT technology_dependencies_technology_fkey FOREIGN KEY (technology) REFERENCES technologies(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2152 (class 2606 OID 24780)
-- Name: user_technologies_technology_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_technologies
    ADD CONSTRAINT user_technologies_technology_fkey FOREIGN KEY (technology) REFERENCES technologies(name) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2153 (class 2606 OID 24785)
-- Name: user_technologies_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_technologies
    ADD CONSTRAINT user_technologies_user_fkey FOREIGN KEY ("user") REFERENCES users("user") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2154 (class 2606 OID 24790)
-- Name: village_buildings_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_buildings
    ADD CONSTRAINT village_buildings_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2155 (class 2606 OID 24795)
-- Name: village_occupations_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_occupations
    ADD CONSTRAINT village_occupations_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2156 (class 2606 OID 24800)
-- Name: village_resources_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_resources
    ADD CONSTRAINT village_resources_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2157 (class 2606 OID 24805)
-- Name: village_troops_village_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY village_troops
    ADD CONSTRAINT village_troops_village_fkey FOREIGN KEY (village) REFERENCES villages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2016-12-22 15:40:16

--
-- PostgreSQL database dump complete
--

select * from users
insert into users values ('free','','', 100, 20)