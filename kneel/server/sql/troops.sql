﻿delete from troop_profiles;

insert into troop_profiles (troop, profile) values ( 'trade-cart', '{ "speed":40,"capacity":1000,"fph":5 }' );

insert into troop_profiles (troop, profile) values ( 'infantry', '{"atkToArchers":1.5,"atkToInfantry":1,"atkToCalvary":0.4,"atkToSiege":1.2,"speed":15,"capacity":50,"fph":1}');

insert into troop_profiles (troop, profile) values ( 'pikeman', '{"atkToArchers":1,"atkToCalvary":0.75,"atkToInfantry":0.75,"speed":10,"capacity":30,"fph":0.8}');

insert into troop_profiles (troop, profile) values ( 'archer', '{"atkToArchers":1,"atkToCalvary":0.75,"atkToInfantry":0.75,"speed":10,"capacity":30,"fph":0.8}');


insert into troop_profiles (troop, profile) values ( 'heavy-archer', '{"atkToArchers":1.4,"atkToCalvary":1.2,"atkToInfantry":1.4,"speed":12.5,"capacity":50,"fph":1.1}');


insert into troop_profiles (troop, profile) values ( 'scout', '{"atkToArchers":1.5,"atkToCalvary":1,"atkToInfantry":1.25,"defFromArchers":0.05,"defFromInfantry":0.1,"speed":40,"capacity":100,"fph":2}' );

insert into troop_profiles (troop, profile) values ( 'knight', '{"atkToArchers":1.75,"atkToCalvary":1.25,"atkToInfantry":1.5,"defFromArchers":0.1,"defFromCalvary":0.05,"defFromInfantry":0.15,"speed":30,"capacity":80,"fph":2.9}' );

insert into troop_profiles (troop, profile) values ( 'ram', '{"atkToWall": 2, "defFromArchers":0.15,"speed":3,"capacity":0,"fph":4}' );

insert into troop_profiles (troop, profile) values ( 'catapult', '{"atkToArchers":1,"atkToCalvary":1,"atkToInfantry":1,"atkToBuilding":0.2, "atkToWall": 1, "defFromArchers":0.1,"speed":7.5,"capacity":0,"fph":4}' );

insert into troop_profiles (troop, profile) values ( 'trebuchet', '{"atkToArchers":1,"atkToCalvary":1,"atkToInfantry":1,"atkToBuilding":0.5, "atkToWall": 2, "defFromArchers":0.1,"speed":7.5,"capacity":0,"fph":5, "maxDistance" : 1.42 }' );