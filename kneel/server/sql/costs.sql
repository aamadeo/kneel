﻿delete from costs;
-- units
insert into costs  ( item, food, iron, stone, wood, time ) values ('trebuchet', 2000, 3000, 5000, 3000, 6000); 
insert into costs  ( item, food, iron, stone, wood, time ) values ('catapult', 150, 150, 150, 250, 480); 
insert into costs  ( item, food, iron, stone, wood, time ) values ('ram', 150, 150, 20, 200, 360); 
insert into costs  ( item, food, iron, stone, wood, time ) values ('infantry', 60, 70, 10, 50, 120);
insert into costs  ( item, food, iron, stone, wood, time ) values ('heavy-infantry', 80, 100, 15, 80, 180);
insert into costs  ( item, food, iron, stone, wood, time ) values ('pikeman', 50, 60, 10, 70, 90);
insert into costs  ( item, food, iron, stone, wood, time ) values ('scout', 120, 90, 20, 80, 180); 
insert into costs (item, food, iron, stone, wood, time) values ('knight', 180, 135, 30, 405, 240);
insert into costs (item, food, iron, stone, wood, time) values ('archer',60,40,10,90, 90);
insert into costs (item, food, iron, stone, wood, time) values ('heavy-archer',60,60,15,120, 180);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'peasant', 600, 300, 270, 400, 1200);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'trade-cart', 600, 400, 300, 700, 2400);

-- buildings
insert into costs (item, food, iron, stone, wood, time) values ('Market', 150, 200, 250, 250, 1200);
insert into costs ( item, food, iron, stone, wood, time ) values ('Warehouse', 300, 500, 500, 500, 1200);
insert into costs ( item, food, iron, stone, wood, time ) values ('Barn', 500, 300, 300, 300, 1200);
insert into costs ( item, food, iron, stone, wood, time ) values ('Archery', 400, 500, 300, 700, 2400);
insert into costs ( item, food, iron, stone, wood, time ) values ('Barracks', 200, 700, 500, 400, 2400);
insert into costs ( item, food, iron, stone, wood, time ) values ('Stable', 600, 500, 300, 300, 3600);
insert into costs ( item, food, iron, stone, wood, time ) values ('Tower', 800, 800, 1200, 1200, 4200);
insert into costs (item, food, iron, stone, wood, time) values ('SiegeWeaponFactory',3000, 6000, 4000, 3000, 4200);
insert into costs (item, food, iron, stone, wood, time) values ('University', 1000, 1200, 1500, 1050, 12000);
insert into costs (item, food, iron, stone, wood, time) values ('Blacksmith', 3000, 7000, 3000, 3000, 4200);
insert into costs (item, food, iron, stone, wood, time) values ('TownCenter', 200, 200, 200, 200, 6000);

-- resources technology
insert into costs ( item, food, iron, stone, wood, time ) values ( 'foodTech', 1000, 750, 750, 2250, 4800);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'woodTech', 1500, 750, 2250, 1125, 4800);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'ironTech', 1500, 750, 1125, 1500, 4800);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'stoneTech', 1500, 7500, 2250, 1125, 4800);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'buildersTech', 5000, 5000, 6000, 6000, 21600);

insert into costs ( item, food, iron, stone, wood, time ) values ( 'Archery Research', 4000, 5000, 3000, 7000, 21600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Stable Research', 6000, 5000, 3000, 3000, 21600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Blacksmith Research', 4000, 1000, 1000, 1600, 21600);

insert into costs ( item, food, iron, stone, wood, time ) values ( 'Siege', 10000, 10000, 11000, 11000, 9600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Ram Research', 10000, 10000, 5000, 15000, 21600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Catapult Research', 11000, 10000, 15000, 10000, 9600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Trebuchet Research', 20000, 15000, 20000, 30000, 21600);

insert into costs ( item, food, iron, stone, wood, time ) values ( 'Elite Training', 20000, 25000, 15000, 35000, 21600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Heavy-Archer Research', 6000, 5000, 3000, 3000, 21600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Heavy-Infantry Research', 4000, 1000, 1000, 1600, 21600);
insert into costs ( item, food, iron, stone, wood, time ) values ( 'Knight Research', 15000, 15000, 16000, 16000, 21600);