const core = require('../core')
const qm = require('./queries')
const dao = require('./dao')
const trx = require('../lib/transactional')

const troops = {
	
	calcFph( troops ){
		let fph = 0
		
		for( t in troops ){
			let troop = troops[t]
			
			if ( troop.count === undefined || troop.count === 0 ) continue
			fph += troop.count * core.profiles[t].fph
		}
		
		return fph
	},
	
	trainTroop({village, type}, transaction){
		dao.trainTroop(village, type, transaction)
	},
	
	movingTroops({user, village, troops}, coming, transaction){
		let sign = coming ? 1 : -1
		let names = Object.keys(troops)
		let fph = 0
		
		for ( name in troops ){
			let troop = troops[name]
			
			if ( troop.count === null || troop.count === 0 ) continue
			
			dao.movingTroops(sign * troop.count , village, name, transaction)
			
			fph += sign * core.profiles[name].fph * troop.count
		}
		
		return fph
	},
	
	casualties( {user, village, troops: after, originalTroops: before, type, id}, curOp){
		let fph = 0
		for ( let troop in after ){

			let deaths = before[troop].count - after[troop].count
			fph = fph + deaths * core.profiles[troop].fph
			
			if ( deaths === 0 ) continue
			
			if ( type === "village" ){
				dao.localCasualties(deaths, village, troop, curOp)	
			} else if ( type === "mission" ){
				dao.missionCasualties(deaths, id, troop, curOp)	
			}
		}
		
		return fph
		//village.updatePopulation(-fph, village, curOp) //!!Todo: debe salir afuera -> battle.logic
	},
	
	lock(village, troop, transaction){
		dao.lock(village, troop, transaction)
	}
}

for( let f in troops){
	if( typeof troops[f] !== "function" ) continue
	troops[f] = troops[f].bind(troops)
}

module.exports = trx.transactionalModule(troops)
