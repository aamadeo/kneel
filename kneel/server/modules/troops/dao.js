const qm = require('./queries')
const trx = require('../lib/transactional')

let troops = {
	name: "troops.dao",
	trainTroop(village, type, transaction){
      qm.addSimpleQuery("train", [village, type], transaction)
	},
	
	movingTroops(count, village, troop, transaction){
		qm.addSimpleQuery("moving", [count, village, troop], transaction)
	},
	
	localCasualties(deaths, village, troop, transaction){
		qm.addSimpleQuery("localCasualties", [deaths, village, troop], transaction)
	},
	
	missionCasualties(deaths, id, troop, transaction){
		qm.addSimpleQuery("missionCasualties", [deaths, id, troop], transaction)
	},
	
	lock(village, troop, transaction){
		qm.addSimpleQuery( "lockTroop", [village, troop] , transaction)
	}
}

for( f in troops){
	if( typeof troops[f] !== "function" ) continue
	troops[f] = troops[f].bind(troops)
}

module.exports = trx.transactionalModule(troops)