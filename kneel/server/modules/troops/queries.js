const queryManager = require('../lib/qm')

module.exports = queryManager({
	idSpace: 'troops',
	queries:{
		train: "update village_troops set count = count + 1 where village = $1 and troop = $2",
		moving: "update village_troops set count = count + $1 where village = $2 and troop = $3",
		localCasualties: "update village_troops set count = count - $1 where village = $2 and troop = $3",
		missionCasualties: "update mission_troops set count = count - $1 where mission = $2 and troop = $3",
		lockTroop: "select * from village_troops where village = $1 and troop = $2",
	},
})