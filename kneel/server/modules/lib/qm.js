const trx = require('./transactional')

module.exports = library => ({
	library,
	addQuery({name, args, lock, perRow, resultHandler}, transaction){
		let forUpdate = ( typeof lock === "boolean" && lock ) ? " for update" : ""
		
		if ( ! library.queries[name] ){
			throw ('Query ' + name + " doesn't exists in library " + library.idSpace)
		}
	
		const query = { 
			text: library.queries[name].replace(/\s\s+/g,' ') + forUpdate ,
			name: library.idSpace + "." + name,
			values: args
		}
	
		const op = trx.trxOperation( query,resultHandler )
		if ( perRow !== undefined ) op.perRow = perRow
		
		transaction.add( op )
	},
	
	addSimpleQuery(name, args, transaction){
		this.addQuery({ name, args}, transaction)
	},
	
	singleQueryExec( name, args, handlers){
		const query = { 
			text: library.queries[name],
			name: library.idSpace + "." + name,
			values: args
		}
		
		trx.singleQueryExec(query, handlers)
	}
})