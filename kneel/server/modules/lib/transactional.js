let conn = require('./connection')
let crypto = require('crypto')
let utils = require('./utils')

const EventEmitter = require('events');
const ee = new EventEmitter();

let transactional = {
	singleQueryExec(query, cb){
		conn.run( (client, release) => {
			if ( query === null ) {
				ee.emit('error', new Error('Query is null'))
			}
			
			let _query = client.query(query)
			
			let onErroHandler = typeof cb.onError === "function" ? onError : (error)=>{
				console.log("SQExec.Error: ", error)
				console.log("Query:", query)
			}
			
			let _resultHandler = (result)=>{
				
				if ( typeof cb.resultHandler === "function" ) cb.resultHandler(result)
				else console.log("Result:", result)
				
				release()
			}
			
			let _perRow = typeof cb.perRow === "function" ? cb.perRow : (row, result)=>{
				result.addRow(row)
			}
			
			_query.on('end', _resultHandler)
			_query.on('row', _perRow)
			_query.on('error', onErroHandler)
		})
	},
	trxOperation( query, post ){
		
		post = typeof post === "function" ? post : ()=>{}
		let op = {
			isOperation: true,

			query: query,
			
			next: (client, release) => {
				op.session.commit(client, release)
			},

			onError: null,
			onErrorHandler(error, client, release){
				console.log(error)
				
				if ( this.onError ) {
					this.onError(error, this.session)
				} else {
					console.log(error)
					console.log('\n\nQuery: ', this.query)
					this.session.status = error
					this.session.rollback()
				}
			},
			perRow: (row,result) =>{ result.addRow(row) },
			execute(){
				/*console.log("Op.this:", this)*/
				//try {
					let query = this.query
					if ( ! this.session.continue ) return this.session.rollback()

					if ( query ) {
						//console.log("\nExec query.", query.text, query.values,"\n")

						query = this.session.query(query)
						query.on('error', this.onErrorHandler)
						query.on('row', this.perRow )

						let op = this
						query.on('end', (result) => {
							if ( typeof  op.post === "function" ) op.post(result)

							/*console.log(op)*/

							op.next()
						})
					} else {
						if ( typeof this.post === "function" ) this.post(this)
						this.next()
					}
				//} catch ( err ){
				//	this.session.status = err
				//	return this.session.rollback()
				//}
			},
			
			/*To be compatible with transaction*/
			add(nextOp){
				this.lastAdded.after(nextOp)
				this.lastAdded = nextOp
				
				return nextOp
			},
			
			addQuery(query, post){
				let nextOp = transactional.trxOperation(query, post)

				this.lastAdded.after(nextOp)			
				this.lastAdded = nextOp
				
				return nextOp
			},
			
			addAction( f ){
				return this.addQuery(null, f)
			},
			
			after(op){
				let then = this.next
				this.next = op.execute
				
				op.next = then
				op.session = this.session
			}
		}

		op.lastAdded = op
		op.post = post.bind(op)
		utils.bind(op, op, { next: true, perRow: true })
		
		return op
	},
	mockTransaction(){
		const transaction = this.transaction()
		transaction.run = mockRun(transaction)
		
		return transaction
	},
	transaction(){
		let trx = {
			isTransaction: true,
			operations: [],
			lastOp(){
				let op = this.operations[this.operations.length - 1]
				
				return op
			},
			addQuery( query, post ){
				this.operations.push( transactional.trxOperation(query, post) )
			},
			addAction( action ){
				this.operations.push( transactional.trxOperation(null, action) )
			},
			add( op ){
				this.operations.push(op)
			},
			
			finally: (status)=>{
				console.log("Transaction exited with status: " + status)
			},
			abort(err){
				trx.session.status = err
				trx.session.continue = false
			}
		}
		
		trx.addAction((curOp)=>{})
		trx.run = transactional.run.bind(trx)
		
		return trx
	},
	run: function( ){
/*		console.log("Trx.this", this)*/
		const trx = this
		
		const session = {
			continue: true,
			query(queryObj){
				return session.client.query(queryObj)
			},
			status: 0,
			client: null,
			release: null,
			queries: {
				begin: {name: "begin", text :"begin"},
				end: {name: "end", text :"end"},
				rollback: {name: "rollback", text :"rollback"},
				commit: {name: "commit", text :"commit"},
			},
			commit(){
				session.query(this.queries.commit).on('end', (result)=>{
					session.end()
				})
			},
			rollback(client, release){
				session.query(this.queries.rollback).on('end',(result)=>{
					session.end()
				})
				session.continue = false
			},
			begin( firstOp ){
				session.query(this.queries.begin).on('end',(result)=>{
					firstOp()
				})
			},
			end(){
				session.query(this.queries.end).on('end',(result)=>{
					session.release()
					
					/*console.log("Session.this: ", this)*/
					
					trx.finally(session.status)
				})
			}
		}
		
		this.session = session
		
		for( let op = 0; op < this.operations.length ; op++ ){
			this.operations[op].session = session
			
			if ( op > 0 ) this.operations[op-1].next = this.operations[op].execute
		}
		
		conn.run((client, release)=>{
			session.client = client
			session.release = release
			
			/*console.log( "Op.0:", this.operations[0] )*/
			
			session.begin( this.operations[0].execute )
		})
	},
	readMap(container, map){
		let it = map.keys()
		let item = it.next()

		while(! item.done ){
			container[item.value] = map.get(item.value)
			item = it.next()
		}
	},
	toMap( o ){
		
		let keys = Object.keys(o)
		let map = new Map()
		
		for( let k = 0 ; k < keys.length ; k++ ){
			
			let attrVal = o[keys[k]]
			let string = typeof attrVal === "object" ? JSON.stringify(attrVal) : "" + attrVal
			
			map.set(keys[k], string)
		}
		
		return map
	},
	
	transactionalMethod( method, module ){
		const regex= /.+\((.*)\)/
		const source = method.toString()
		
		const args = source.match(regex)[1].split(",").map( p => p.split(" ").join("") )
		let newMethod = method
		
		args.forEach(arg => {
			if ( arg === "transaction" ){
				newMethod = function(...args){
					const trx = args[method.length-1]
					
					if ( trx === undefined || ! (trx.isTransaction || trx.isOperation) ){
						throw (`[${module.name}.${method.name}] \nInvalid transaction object :  ${JSON.stringify(trx)}`)
					} else {
						//console.log(`[${module.name}.${method.name}] Transaction parameter is Correct`)
					}
					
					method.apply(module, args)
				}
			}
		})
		
		return newMethod
	},
	
	transactionalModule( module ){
		for( let method in module ){
			if( typeof module[method] !== "function" ) continue

			module[method] = this.transactionalMethod(module[method], module)
			
			//console.log(module.name + "." + method + " is transactional")
		}
		
		return module
	}
}

module.exports = transactional