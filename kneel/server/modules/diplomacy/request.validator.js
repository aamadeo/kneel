const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number)

const positiveNumber = (number, title, code) => {
	if ( ! validNumber(number) || number < 1 ){
		throw { msg: `Invalid ${title}: ${number}`, code }
	}
}

const validString = (_string, title, code) => {
	if ( typeof _string !== 'string' || _string.length === 0 ){
		throw {
			msg: title + " must be a valid string : " + _string,
			code
		}
	}
}

const validator = {
	err: {
		invalidTax: -1,
		invalidFISOTK: -2,
		invalidKing: -3,
		invalidFPH: -4,
		invalidSubject: -5
	},
	
	surrender( { tax, fisotk, king }){
		positiveNumber(tax, 'tax', validator.err.invalidTax)
		positiveNumber(fisotk, 'fisotk', validator.err.invalidFISOTK)
		validString(king, 'king', validator.err.invalidKing)
	},
	
	updateTerms({ tax, fisotk}){
		positiveNumber(tax, 'tax', validator.err.invalidTax)
		positiveNumber(fisotk, 'fisotk', validator.err.invalidFISOTK)		
	},
	
	claimTroops({ fph, subject }){
		positiveNumber(fph, 'fph', validator.err.invalidFPH)
		validString(subject, 'subject', validator.err.invalidSubject)
	}
}

module.exports= validator