const trx = require('../lib/transactional')
const qm = require('./queries')

const diplomacy = {
	name: "diplomacy.dao",
	
	surrender({user, tax, fisotk, king}, transaction ){
		qm.addSimpleQuery("surrender", [king, user, tax, fisotk], transaction)
	},
	
	info(userInfo, transaction){
		userInfo.terms = []

		qm.addQuery({
			name: "info",
			args: [userInfo.user],
			perRow: row => userInfo.terms.push(row)
		}, transaction)
	},
	
	loadTerm(term, transaction){
		qm.addQuery({
			name: "loadTerm",
			args: [term.king, term.subject],
			perRow: row => Object.assign(term, row)
		})
	},
	
	subjectsOf( king, allies, transaction ){
		qm.addQuery({
			name: 'subjectsOf',
			args: [king],
			perRow: row => allies.push(row.name)
		}, transaction)
	},
	
	updateTerms({user, tax, fisotk, king}, transaction){
		const params = [tax, fisotk, king, user]
		
		qm.addSimpleQuery("updateSurrender", params, transaction )
	},
	
	updateFiSotK(fph, subject, transaction){
		qm.addSimpleQuery("updateFiSotK", [fph, subject], transaction)
	},
	
	updateTaxes({materials, food}, subject, transaction){
		qm.addSimpleQuery("updateTaxes", [materials, food, subject], transaction)
	},
	
	confirm({king, subject, response}, transaction){
		console.log({ king, subject, response })
		
		if ( response === "cancel"){
			qm.addSimpleQuery("cancelSurrender", [king, subject], transaction)
		} else if ( response === "accept" ){
			qm.addSimpleQuery("acceptSurrender", [king, subject], transaction)
		}
	},
	
	deleteUserTerms(user, transaction){
		qm.addSimpleQuery("deleteUserTerms", [user], transaction)
	},
	
	updateCounters({king, subject, forceAvailable, whResources, brnResources}, transaction){
		qm.addSimpleQuery("updateCounters", [forceAvailable, whResources, brnResources, king, subject], transaction)
	}
}

module.exports = trx.transactionalModule(diplomacy)