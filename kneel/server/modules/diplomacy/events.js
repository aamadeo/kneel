//@flow
const diplomacy = require('./index')
const dao = require('./dao')
const transactions = require('../lib/transactional')

const diploEvents = {
	handler(socket, event){
		const { user } = socket.handshake.session
		const { king, subject } = event
		const term = {
			isKing: subject !== undefined && king === undefined,
			king: king || user,
			subject: subject || user
		}
		
		if ( type.match(/allegiance-(denied|accepted)/) ){
			return socket.emit(type, term)
			
		}
		
		const transaction = transactions.transaction()
		diplo.loadTerm(term, transaction)

		transaction.addAction( op => {
			socket.emit(type, { term, id: event.id })
		})

		transaction.finally = status => {
			if ( status !== 0 )	console.log("\n\nError:" + status)
		}

		transaction.run()
	}
}

module.exports = diploEvents