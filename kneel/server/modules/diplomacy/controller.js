//@flow weak

const express = require('express')
const router = express.Router()
const url = require('url')
const logic = require('./index')
const checkAuth = require('../lib/checkAuth')
const transactions = require('../lib/transactional')
const validator = require('./request.validator.js')

const respond = (response, object) => response.send(JSON.stringify(object))

router.post('/surrender', checkAuth, function(req, res, next) {

	const session = req.session
	console.log("Surrendering", req.body)

	try{
		 validator.surrender(req.body)

		 const transaction = transactions.transaction()
		 logic.surrender(session, req.body, transaction)

		 transaction.finally = status => {
			 respond(res, status === 0 ? { status: "pending" }
													 : { errorCode: status }
			 )
		 }

		 transaction.run()		 
	} catch (err){
		 respond(res, { errorCode: err })
	}
})

router.put('/surrender', checkAuth, function(req, res, next) {

	const session = req.session
	console.log("Update Surrender: ", req.body)
	const transaction = transactions.transaction()
	const { response } = req.body
	let action = null

	switch(response){
		case "accept" : 
		case "cancel" : action = logic.confirm ; break
		case "update" : 
			try {
				 validator.updateTerms(req.body)
				 action = logic.updateTerms				 
			} catch ( err ){
				 return respond(res, { errorCode: err })
			}

			break
		default: action = () => transaction.abort("Invalid response : " + response)
	}

   action(session, req.body, transaction )
	
	transaction.finally = status => {
		respond(res, status === 0 ? { ok: true }
				  						        : { errorCode: status }
		)
	}
				  
	transaction.run()
})

router.get('/', checkAuth, function(req, res, next) {

	const transaction = transactions.transaction()
	const info = { user: req.session.user }
	
	console.log("Diplomacy Info")
   
   logic.info(info, transaction)
	
	transaction.finally = status =>{
		respond(res, status === 0 ? info
				  						  : { errorCode: status }
		)
	}

	transaction.run()
})

router.post('/troopsCalling', checkAuth, function(req, res, next) {

	const session = req.session
	console.log("Calling Troops", req.body)

	try {
		 validator.claimTroops(req.body)
		 const transaction = transactions.transaction()
		 logic.callTroops(session, req.body, transaction)

		 transaction.finally = status => {
			 respond(res, status === 0 ? { ok: true }
													 : { errorCode: status }
			 )
		 }

		 transaction.run()
	} catch (err){
		 
	}
})

module.exports = router