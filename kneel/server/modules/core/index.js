const dao = require('./dao')

const core = {
	rps: { 
		base: 0.003014966667, //Coeficiente en rps
		coeficient: 1.5 
	},
	distance: {
		user: 2,
		raidv: 6
	},
	
	market: { topPrice: 2.5 },
   price: {
      coeficient: 1.3
   },
	storage: {
		deposit: 1500,
		base: 1500,
		coeficient: 1.759026
	},
	techAvailable(userTech, tech){
		for( d in core.technologies[tech].dependencies ){
			dependency = core.technologies[tech].dependencies[d]
			if ( userTech[dependency] === null ) return false
		}

		return true 
	},
	villageSummary(session){
		let summary = { 
			userTech: {},
			occupations: {},
			buildings: [],
			troops: {},
			resources: {},
			locks:{
				occupations: false,
				buildings: false,
				troops: false,
				resources: false
			},
			ignore: {},
			user: session.user,
			village: session.village
		}

		return summary
	},
	diameter: 300,
	speed: 100,
	costs: {
		loaded: false
	},
	profiles: {
		loaded: false
	},
	technologies: {
		loaded: false
	},
	mission: {
		raid: "raid",
		attack: "attack",
		conquer: "conquer",
		support: "support",
		collect: "collect",
		sendResources: "send resources"
	},
	spawningBuildings: {
		Barracks: ["infantry","pikeman"],
		Archery: ["archer","heavy-archer"],
		Stable: ["scout","knight"],
		SiegeWeaponFactory: ["ram","catapult","trebuchet"],
		TownCenter: ["peasant"],
		Market: ["trade-cart"]
	},
	spawningBuilding( troop ){
		for( let b in core.spawningBuildings ){
			const spawns = core.spawningBuildings[b].findIndex( type => type === troop ) > -1
			
			if ( spawns ) return b
		}
		
		return null
	},
	
	totalResources: resources => Object.keys(resources).reduce( 
		(t,r) => t + resources[r], 
		0
	),
	
	validBuilding: building => core.buildingsNames.find(b => b === building) !== undefined,
	
	validAction: action => Object.keys(core.mission).reduce(
		(v, a) => v || core.mission[a] === action 
	, false),
	
	validNumber: number => !isNaN(parseFloat(number)) && isFinite(number),
	
	positiveNumber: number => core.validNumber(number) && number > 0,
	
	negativeNumber: number => core.validNumber(number) && number < 0,
	
	buildingsNames: ["Market", "Archery", "Barn", "Barracks", "Stable", "Warehouse", "SiegeWeaponFactory", "University", "Blacksmith", "TownCenter"],
	resources:["food","iron","stone","wood"],
	occupations:["food","iron","stone","wood","builders","unoccupied"],
	troops:["infantry","pikeman","archer","heavy-archer","scout","knight","ram","catapult","trebuchet","trade-cart"],
	elements: ["resources","occupations","troops"],
	radius: 5
}

/* Transactional load core params */
dao.costs(core.costs)
dao.profiles(core.profiles)
dao.technologies(core.technologies)

module.exports = core
