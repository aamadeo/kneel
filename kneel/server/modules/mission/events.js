//@flow
const mission = require('./index')
const core = require('../core')
const transactions = require('../lib/transactional')

const missionEvents = {
	handler(socket, event){
		const { user } = socket.handshake.session
		const transaction = transactions.transaction()
		const missionId = event.mission
		const { type } = event
		const report = {}
		const container = {}
		
		if ( type.match(/(new|update)-report/) ){
			mission.getReport(event.mission, report, transaction)
		} else if ( type.match(/(new|update)-mission/) ){
			mission.getMissionById(missionId, container, transaction)
		} else if ( type.match(/end-mission/) ){
			const mission = { id: event.mission }
			console.log('Ending mission ', event.mission)
			return socket.emit(`end-mission`, { mission, id: event.id })
		}

		transaction.addAction( op => {
			console.log(type)
			if ( type.match(/(new|update)-report/) ){
				socket.emit(type, { report, id: event.id })
			} else if ( type.match(/(new|update)-mission/) ){
				console.log(type, event.mission)
				socket.emit(type, { mission: container, id: event.id })
			}
		})
				
		transaction.finally = status => {
			if ( status !== 0 )	console.log("\n\nError:" + status)
		}

		transaction.run()
	}
}

module.exports = missionEvents