const transactions = require('../lib/transactional')
const checkAuth = require('../lib/checkAuth')
const router = require('express').Router()
const mission = require('./index')
const core = require('../core')
const validator = require('./request.validator')

const _respond = (res,object) => res.send(JSON.stringify(object))
const respond = (status, res, object) => {
	const error = { errorCode: status }
	_respond(res, status === 0 ? object : error )
}

router.post('/', checkAuth, function(req, res, next) {

	try {
		console.log("Req.body : ", req.body)
		
		validator.execute(req.body)

		const transaction = transactions.transaction()
		const { user, village, location, radius, avatar } = req.session

		console.log("Avatar == ", avatar)

		const params = Object.assign({}, req.body, { user, village, avatar })
		const summary = core.villageSummary(params)
		params.speed = 1000
		params.origin = location

		console.log( "body:", req.body )
		mission.execute(params, summary, transaction)
		transaction.finally = status => respond(status, res, summary)

		transaction.run()
	} catch(err){
		console.log(err)
	}
})

// !?
router.get('/', checkAuth, (req, res, next) => {

	const { user } = req.session
	const missions = []
	const transaction = transactions.transaction()

	mission.getMissions(user, missions, transaction)

	transaction.finally = status => {
		//console.log("Missions", missions)
		respond(status, res, missions)
	}

	transaction.run()
})


// !?
router.get('/reports', checkAuth, function(req, res, next){
	const transaction = transactions.transaction()
	const reports = []
	
	mission.getReports(req.session.user, reports, transaction)
	transaction.finally = status => respond(status, res, reports)
	
	transaction.run()
	
})

router.put('/toggleLoop', checkAuth, function(req, res, next){
	const transaction = transactions.transaction()
	
	console.log( "ToggleLoop body:", req.body )
	
	mission.toggleLoop(req, transaction)
	transaction.finally = status => respond(status, res, {ok : true})

	transaction.run()
})

router.put('/abort', checkAuth, function(req, res, next){
	const transaction = transactions.transaction()
	
	console.log( "body:", req.body )
	
	mission.abort(req, transaction)
	transaction.finally = status => respond(status, res, {ok : true})

	transaction.run()
})


module.exports = router