//@flow
const core = require('../core')

const INVALID_BUILDING = -1
const INVALID_WEIGHT = -2
const INVALID_DAMAGE = -3
const INVALID_TIME_LEFT = -4
const INVALID_THRESHOLD = -5

const validBuilding = building => {
	if ( ! core.validBuilding(building) ){
		throw {
			code: INVALID_BUILDING,
			msg: 'Invalid building : ' + building
		}
	}
}

class Target {
	building: string
	weight: number
	
	constructor({building, weight = 1} : {building: string, weight: number} ){
		validBuilding(building)
		
		if ( ! core.positiveNumber(weight) ){
			throw {
				code: INVALID_WEIGHT,
				msg: 'Invalid weight number : ' + weight
			}
		}
		
		this.building = building
		this.weight = weight
	}

}

class CatapultSiegeInfo {
	targets: Target[]
	type: string
	
	constructor(targets: Target[]) {
		this.targets = []
		this.type = 'Catapult'
		this.targets = targets.map( t => new Target(t) )
	}

	addTarget(target: Target){
		this.targets.push( new Target(target) )
	}
}

class TrebuchetSiegeInfo {
	timeLeft: number
	destroyAll: boolean
	start: number
	damageThreshold: number
	targets: Target []
	type: string
	
	constructor(timeLeft: number, damageThreshold: number, destroyAll: boolean){
		
		if ( timeLeft !== undefined && ! core.positiveNumber(timeLeft) ){
			throw {
				code: INVALID_TIME_LEFT,
				msg: 'Invalid timeLeft : ' + timeLeft
			}
		}

		if ( damageThreshold !== undefined &&! core.positiveNumber(damageThreshold) ){
			throw {
				code: INVALID_THRESHOLD,
				msg: 'Invalid damage threshold : ' + damageThreshold
			}
		}
		
		this.timeLeft = timeLeft
		this.damageThreshold = damageThreshold
		this.targets = []
		this.type = 'Trebuchet'
		this.destroyAll = destroyAll
		
		if ( this.timeLeft ){
			this.start = new Date().getTime()
		}
	}

	addTarget(target: { building: string, weight: number }){
		this.targets.push(new Target(target))
	}
}

module.exports = {
	CatapultSiegeInfo,
	TrebuchetSiegeInfo
}