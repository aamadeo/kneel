//@flow
const dao = require('./dao')

const tasks = require('../tasks')
const village = require('../village')
const map = require('../map')
const battle = require('./battle')
const siege = require('./siege')
const diplomacy = require('../diplomacy')
const troops = require('../troops')
const tech = require('../technology')

const core = require("../core")
const utils = require("../lib/utils")
const trx = require('../lib/transactional')
const { CatapultSiegeInfo, TrebuchetSiegeInfo } = require('./model')
const { Event } = require('../events/model')
const events = require('../events')

const maxDistance = core.diameter * Math.sqrt(2)

const trebSiegeCondition = (mission, distance) => mission.siege &&
			mission.siege.type === 'Trebuchet' &&
			distance <= mission.siege.maxDistance &&
			mission.action === 'attack' &&
			mission.objective.action !== 'return'

function point(p){
	return {
		d(to){
			return Math.sqrt(Math.pow(this.x-to.x,2) + Math.pow(this.y-to.y,2))
		},
		equals(p){
			return this.x === p.x && this.y === p.y
		},

		x: p.x,
		y: p.y
	}
}

//Cells per secondf 
let troopSpeed = 1 / 100
 
const mission = {
	name: "mission",
	err: {
		unauthorizedUpdate: "User does not own mission",
		invalidAction: "You can't perform this action to this user",
		noTroopSelected: "Mission doesn't specify any troop",
		notEnoughTroops: "There aren't enough troops to start the mission",
		siegeError: "Error in siege parameters",
		notEnoughResources: "There aren't enough resources to send",
		noResourcesSpec: "Error in resources spec",
		noCapacity: "The mission doesn't have enough capacity"
	},
	
	/*******************************************************
	 * Main EntryPoint
	 *
	 *******************************************************/
	execute(params, summary, transaction){
		console.log("mission.execute [init]")
		
		
		/*Siege configuration Parsing*/
		if ( params.objective.type === 'village' && params.siege !== undefined ){
			let type = params.troops.trebuchet.count > 0 ? 'Trebuchet': undefined
			type = type || (params.troops.catapult.count > 0 ? 'Catapult' : undefined)
			
			if ( type === 'Catapult' ){
				params.siege = new CatapultSiegeInfo(params.siege)
			} else if ( type === 'Trebuchet'){
				const { targets, timeLeft, damageThreshold, destroyAll } = params.siege
				params.siege = new TrebuchetSiegeInfo(timeLeft, damageThreshold, destroyAll)

				targets.forEach( t => params.siege.addTarget(t))
			}
		}
		
		/*
		 * Diplomatic Validations
		 */
		diplomacy.info( summary, transaction )
		transaction.addAction( op => {
			const target = params.objective.owner
			
			params.isAlly = summary.allies.find( a => a === target )
			
			if ( summary.king && summary.king === target ){
				params.objective.king = true
			}
			
			params.king = summary.ruler
			
			if ( params.action === core.mission.collect ){
				 if ( !(params.isAlly && params.king) ){
					return transaction.abort(this.err.invalidAction)
				 } else {
					 const terms = summary.terms.find( t => t.subject === target )
					 params.taxInfo = {
						 materials: terms.whResources,
						 food: terms.brnResources
					 }
				 }
			}
		})
		

		/*
		 * Calculating speed mission
		 */
		summary.locks.troops = true
		summary.locks.resources = true
		village.load(summary, transaction)
		
		let enoughTroops = true
		let notEmpty = false
		params.speed = 1000
		params.capacity = 0
		for ( let name in params.troops ){
			if ( ! params.troops[name] || params.troops[name].count === 0 ) continue
			
			let speed = parseInt(core.profiles[name].speed) / 3600
			params.capacity += parseInt(core.profiles[name].capacity) * params.troops[name].count
			if ( speed < params.speed ) {
				params.speed = speed
			}
		}
		
			
		/*
		 * Troops Validations
		 */
		transaction.addAction(()=>{
			village.adjustModel(summary)


			console.log("troops:", summary.troops)			

			params.speed = core.speed * params.speed

			
			for( let troop in summary.troops ){
				let available = summary.troops[troop] ? summary.troops[troop].count : 0
				
				if ( ! params.troops[troop] ) continue

				enoughTroops = enoughTroops && params.troops[troop].count <= available
				
				notEmpty = notEmpty || params.troops[troop].count > 0
				
				summary.troops[troop].count -= params.troops[troop].count
			}

			Object.assign(summary, {enoughTroops, notEmpty})
			
			if ( !enoughTroops ) return transaction.abort(mission.err.notEnoughTroops)
			if ( !notEmpty ) return transaction.abort(mission.err.noTroopSelected)
			
			
			/*
			 * Send Resources - Validation
			 */
			if ( params.action === core.mission.sendResources ){
				if ( params.resources ){
					if ( ! village.enoughResources(summary.resources, params.resources)){
						return transaction.abort(mission.err.notEnoughResources)
					}
					
					const totalResources = core.totalResources(params.resources)
					
					if ( totalResources === 0 ){
						return transaction.abort(mission.err.noResourcesSpec)
					} else if ( totalResources > params.capacity ){
						console.log("Mission capacity :", params.capacity)
						return transaction.abort(mission.err.noCapacity)
					}
					
					//Substract Resources
					core.resources.forEach( r => {
						summary.resources[r].r0 -= ( params.resources[r] || 0 )
					})
					
				} else {
					return transaction.abort(mission.err.noResourcesSpec)				
				}
			}
		})
		
		/*
		 * Calculating mission troops fph (food consumption)
		 * Updating fisotk if mission is to support
		 */
		const fph = troops.movingTroops(params, false, transaction)
		village.updatePopulation(fph, summary.village, transaction)
		village.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('village', summary.user, summary.village, transaction)
		
		transaction.addAction( op => {
			if ( params.action === core.mission.support ){
				params.owner = params.objective.owner
				
				if ( params.objective.king ){
					diplomacy.updateFiSotK(fph, params.user, op)
				}
			} else {
				params.owner = params.user
			}
		})
		
		/* In case of a trebuchet siege. Setting maxDistance*/
		if ( params.siege && params.siege.type === 'Trebuchet'){
			const technologies = {}
			tech.available(params.user, technologies, transaction)

			transaction.addAction( op => {
				console.log("Profile Trebuchet", core.profiles.trebuchet.maxDistance)
				params.siege.maxDistance = core.profiles.trebuchet.maxDistance
			})
		}
		
		/*
		 * Setting task 
		 */
		dao.getNewMissionId( params, transaction)
		transaction.addAction( op => {
			console.log( { objective: params.objective })
			const detail = { 
				user: params.user,
				mission: params.id,
				objective: params.objective,
				origin: params.origin
			}
			
			/*
			 * Calculating estimated time of arrival
			 */
			const travel = this.fullpath(params.origin, params.objective)
			const distance = travel.d
			const start = new Date().getTime()
			
			travel.path.forEach( step => {
				step.t = (travel.t || start) + (step.d / params.speed) * 1000
				travel.t = step.t
			})
			
			params.travel = travel
			params.totalTime = travel.t - new Date().getTime()
			params.t0 = new Date().getTime()
			params.from = params.origin

			dao.setMission(params, op)
			map.put(params.origin, params.id, "troops", detail, op)
			this.setEvent('new-mission', params.user, params.id, op)
			
			const nextCell = this.nextMove(params.origin, params.objective)
			
			const taskParams = {
				id: params.id,
				from: params.origin,
				avatar: params.avatar,
				job: 'advance',
				module: 'mission'
			}

			tasks.setTask({ 
				user: params.owner,
				time: 1,
				params: taskParams,
				visible: false,
				selector: 'mission:' + params.id 
			}, op)
			
			
		})
	},
	/*******************************************************/
	
	/*******************************************************
	 * Moving Task EntryPoint
	 *
	 *******************************************************/
	advance( { id, from, avatar }, transaction){
		let continueMission = true, 
				backHome = false,
				executingAction = false
		
		from = point(from)
		const params = { }
		
		mission.getMissionById(id, params, transaction)
		
		transaction.addAction( baseOp => {
			params.id = id
			params.from = from
			
			const abortMission = params.abortMission
			const returning = params.objective.action === "return"
			
			let to = params.travel.path.splice(0,1)[0].p
			to = point(to)
			const distance = to.d(params.objective)

			Object.assign(params, { from, to })

			// objective is reached
			if ( trebSiegeCondition(params, distance) ){
				console.log("Trebuchet Siege Start")
				siege.trebuchet(params, baseOp)
				return
			} else if ( distance === 0 ){
				if ( returning ) {
					console.log("Welcome home")
					backHome = true
				} else {
					executingAction = true
				}
			} else if ( abortMission && !returning ){
				console.log("Aborting mission")
			}

			if ( backHome ){
				//Loop if configured
				if ( params.loop ){
					mission.loadTroops(params, baseOp)
					baseOp.addAction( op => {
						const { village, user, troops, path, loop, speed } = params
						const { action, origin: objective, objective: origin } = params

						const taskParams = { 
							troops, speed, action, 
							origin, objective,
							path, loop, 
							user, village, 
							job: 'restart', module: 'mission'
						}
						tasks.setTask({ 
							user,	
							time: 0,
							params: taskParams,
							selector: 'restart-mission:' + params.id
						}, op)
					})
				}
				// End Mission
				this.loadTroops(params, baseOp)
				baseOp.addAction( op => {
					//console.log("Params Returning Home", params)
					
					this.returningHome(params, op)
				})
			}

			if ( params.siege && params.siege.type === 'Trebuchet'){
				const technologies = {}
				tech.available(params.user, technologies, baseOp)

				baseOp.addAction( op => {
					params.siege.maxDistance = core.profiles.trebuchet.maxDistance
				})
			}

			if ( ! executingAction ){
				if ( ! backHome ) this.move(params, baseOp)
				mission.updateMission(params, baseOp)
				
			} else if ( abortMission) {
				this.turnBack(params, to)
				
				baseOp.addAction( op => {
					mission.updateMission(params, baseOp)
				})
			} else {
				// turning back
				console.log("Executing Action")
				this.turnBack(params, to)
				this.loadTroops(params, baseOp)

				baseOp.addAction( op => {
					this.actionExecution(params, op)
					console.log(`Mission.${params.id}.action executing`)
				})
				
				baseOp.addAction( op => {
					mission.updateMission(params, baseOp)
				})
			}
		})
		
			
	},
	/*******************************************************/
	/*******************************************************
	 * TrebuchetSiege Task EntryPoint
	 *
	 *******************************************************/
	trebuchetSiege(mission, transaction){
		siege.trebuchet(mission, transaction)
	},
	/*******************************************************/
	
	/* GET Reports Entry Point*/
	getReports(user, reports, transaction){
		dao.getReports(user, reports, transaction)
	},
	
	getReport(missionId, report, transaction){
		dao.getReport(missionId, report, transaction)
	},
	
	getMissionById(id, mission, transaction){
		const container = {}
		
		dao.getMission(id, mission, transaction)
		
		transaction.addAction( op => {
			mission.elapsedTime = new Date().getTime() - mission.t0 
		})
	},
	
	/* GET Missions Entry point*/
	getMissions(user, missions, transaction){		
		dao.getMissions(user, missions, transaction)
		
		transaction.addAction( op => {
			missions.forEach( mission => {
				mission.elapsedTime = new Date().getTime() - mission.t0 
			})
		})
	},
	
	abort({ session: { user }, body: { missionId }}, transaction){
		const container = {}
		dao.getMission(missionId, container, transaction)
		
		transaction.addAction( op => {
			 console.log("Mission.abort Container:", container)
			 container.abortMission = true
			 dao.update({ mission: container.id, detail : container }, op)
		})
	},
	
	/*PUT MISSION Entry Point*/
	toggleLoop( { session: {user}, body: {missionId} }, transaction ){
		const container = {}
		
		dao.getMission(missionId, container, transaction)
		
		transaction.addAction( op => {
			console.log("Mission.toggleLoop Container:", container)
			 container.loop = ! container.loop
			 dao.update({ mission: container.id, detail : container }, op)
		})
	},
	
	/* Task Restart Entry Point*/
	restart( mission, transaction ){
		let {user, village } = mission
		
		const summary = core.villageSummary(mission)

		const { x, y } = mission.origin
		const { x: x2, y: y2 } = mission.objective
		mission.path = [ {x, y}, { x: x2, y: y2 } ]
		
		this.execute( mission, summary, transaction )
	},
	
	/**/
	loadTroops( mission, transaction ){
		delete mission.troops
		mission.troops = {}
		
		dao.loadTroops( mission, transaction )
	},
	
	/* Initiate Support Mission Entry Point */
	supportMission({ mission, supporter, village, supported }, transaction ){
		const location = mission.path[0]
		location.x = parseInt(location.x)
		location.y = parseInt(location.y)
		const summary = core.villageSummary({ user: supporter, village: village.id })
		mission.speed = 1000
		
		this.execute( mission, summary, transaction )
	},
	
	turnBack(mission, to){		
		const tmp = mission.objective
		mission.objective = mission.origin
		mission.origin = tmp
		
		mission.objective.originalAction = mission.action
		mission.objective.action = 'return'
		
		const travel = this.fullpath(to, mission.objective)
		const distance = travel.d
		const start = new Date().getTime()

		travel.path.forEach( step => {
			step.t = (travel.t || start) + 1000 * (step.d / mission.speed)
			travel.t = step.t
		})

		mission.travel = travel
		mission.eta = travel.t
		mission.t0 = new Date().getTime()
	},
	
	/* Auxiliary */
	nextMove( origin, objective ){
		let minDistance = maxDistance
		let nextCell = undefined

		for ( let x = -1 ; x <= 1 ; x++ ){
			for( let y = -1 ; y <= 1; y++ ){
				if ( x === 0 && y === 0 ) continue

				const v = {
					d(to){
						return Math.sqrt(Math.pow(this.x-to.x,2) + Math.pow(this.y-to.y,2))
					},
					equals(p){
						return this.x === p.x && this.y === p.y
					},

					x: origin.x + x,
					y: origin.y + y
				}

				if ( v.d(objective) < minDistance ){
					nextCell = v
					minDistance = v.d(objective)
				}
			}
		}

		return nextCell
	},
	
	/* Auxiliary */
	fullpath( p, t ){
		const path = []
		p = point(p)
		if ( p.d(t) === 0 ) return 0
		
		let curP = this.nextMove(p,t)
		let d = p.d(curP)
		
		path.push({ p: curP, d })
		
		while( ! curP.equals(t) ){
			let newP = this.nextMove(curP, t)
			
			path.push({ p: newP, d: curP.d(newP) })
			d += path[ path.length - 1 ].d
			
			curP = newP
		}
		
		return { d, path }
	},
	
	/* Auxiliary */
	actionExecution(mission, transaction){
		switch(mission.action){
			case core.mission.raid:
			case core.mission.attack:
				battle.attack(mission, this.move, this.endMission, this.saveReport, transaction)
			
				this.setEvent('new-report', mission.user, mission.id, transaction)
				this.setEvent('new-report', mission.origin.owner, mission.id, transaction)
				
				break;
			case core.mission.collect:
				village.collectTaxes(mission, diplomacy.updateTaxes, transaction)
				this.move(mission, transaction)
				
				break;
			case core.mission.support:
				this.support(mission.objective, mission, transaction)
				this.endMission(mission, transaction)
				
				break;
			case core.mission.sendResources:
				this.sendResources(mission, this.move, transaction)
				
				break;
		}
	},
	
	/* Auxiliary */
	move(mission, transaction){
		const detail = {
			user: mission.user,
			id: mission.id,
			objective: mission.objective,
			loot: mission.loot !== null,
			eta: mission.eta,
			avatar: mission.avatar
		}
		
		map.move("troops", mission.id, mission.from, mission.to, detail, transaction)
		
		transaction.addAction( op => {
			const { travel, to } = mission
			
			const nextStep = travel.path[0]
			const timeOfNextMove = (nextStep.t - new Date().getTime()) / 1000

			const params = {
				id: mission.id,
				from: to,
				job: 'advance',
				module: 'mission'
			}

			tasks.setTask({ 
				user: mission.owner,
				time: timeOfNextMove,
				params,
				selector: 'mission:' + mission.id 
			}, op)
		})
	},
	
	sendResources( mission, move, transaction){
		const recipient = core.villageSummary({
			user: mission.origin.owner,
			village: mission.origin.id
		})
		
		recipient.locks.resources = true
		
		village.load(recipient, transaction)
		
		transaction.addAction( op => {
			village.adjustModel(recipient)
			
			core.resources.forEach( r => {
				recipient.resources[r].r0 += parseInt(mission.resources[r])
			})
			
			delete mission.resources
		})
		
		village.updateResources(recipient, undefined, transaction)
		move(mission, transaction)
	},
	
	/* Auxiliary */
	support( { user: ally, id: _village }, { user, troops: _troops }, transaction ){
		const fph = troops.movingTroops({ user, village: _village, troops: _troops }, true, transaction)
		village.updatePopulation(fph, _village, transaction)
		village.setUpdateEvent('village', user, _village, transaction)
	},
	
	/* Auxiliary */
	endMission( mission, transaction ){
		this.setEvent('end-mission', mission.user, mission.id, transaction)
		dao.endMission(mission.id, transaction)
		map.remove("troops", mission.id, transaction)
	},
	
	updateMission(params, transaction){
			dao.update({ mission: params.mission, detail : params }, transaction)
	},
	
	/* Auxiliary */
	returningHome(mission, transaction){
		this.endMission(mission, transaction)
		
		const resources = mission.loot || mission.taxes
		const summary = core.villageSummary(mission)
		summary.ignore.resources = ! resources
		summary.ignore.buildings = ! resources
		summary.ignore.occupations = !resources
		summary.locks.resources = ! resources
		summary.locks.troops = true

		village.load(summary, transaction)
		
		village.setUpdateEvent('village', mission.user, mission.village, transaction)
		
		const fph = troops.movingTroops(mission, true, transaction)
		village.updatePopulation(fph, mission.village, transaction)
		
		if ( resources ){
			

			transaction.addAction(op => {
				village.adjustModel(summary) 

				summary.resources.food.r0 += resources.food
				summary.resources.iron.r0 += resources.iron
				summary.resources.stone.r0 += resources.stone
				summary.resources.wood.r0 += resources.wood

				village.updateResources(summary, undefined, op)
			})
		} else {
			console.log("No resources")
		}
	},
	
	/* Auxiliary */
	saveReport(report, transaction){
		dao.saveReport(report, transaction)
	},
	
	/* Auxiliary */
	saveSiegeReport(report, transaction){
		if ( report.new ){
			delete report.new
			dao.createSiegeReport(report, transaction)
		} else {
			dao.updateSiegeReport(report, transaction)
		}
	},
	
	setEvent(type, user, missionId, transaction){
		const event = new Event(`${type}:${user}`)
		event.owner = user
		event.lifespan = 10
		event.details = { mission: missionId, type }
		events.setEvent(event, transaction)
		
		//console.log("Setting Event", event)
	}
}

module.exports = trx.transactionalModule(mission)
