const village = require('../village')
const buildings = require('../buildings')
const troops = require('../troops')
const users = require('../users')
const core = require('../core')
const siege = require('./siege')
const CatapultSiegeInfo = require('./model').CatapultSiegeInfo
const TrebuchetSiegeInfo = require('./model').TrebuchetSiegeInfo

let mission = undefined
const getMissionLogic = () => {
	if ( ! mission ) mission = require('../mission')
	
	return mission
}

const battle = {

	troopType( troop ){
		if ( troop === "infantry" || troop === "pikeman" ) return "Infantry"
		if ( troop === "scout" || troop === "knight" || troop === "trade-cart" ) return "Calvary"
		if ( troop === "archer" || troop === "heavy-archer" ) return "Archers"
		if ( troop === "ram" || troop === "catapult" || troop === "trebuchet" ) return "Siege"
	},
	/*
	 * Rounding casualty numbers
	 */
	adjustCasualties( group ){
		if ( group.casualties === undefined ) return
		
		const names = Object.keys(group.troops)
		group.count = 0
		
		for( let t = 0 ; t < names.length ; t++ ){
			let troop = group.troops[names[t]]
			
			troop.count = Math.round(troop.count)
			group.casualties[names[t]] = Math.round(group.casualties[names[t]])
			group.count += troop.count
		}
	},
	damageFrom( troops, type ){
		let atk = {
			toArchers: 0,
			toCalvary: 0,
			toInfantry: 0,
			toSiege: 0,
		};
		
		let types = Object.keys(troops)
		
		for ( let t = 0 ; t < types.length ; t++ ){
			let troop = troops[types[t]]
			
			if ( this.troopType(types[t]) !== type ) continue
			
			atk.toArchers += core.profiles[types[t]].atkToArchers * troop.count || 0
			atk.toCalvary += core.profiles[types[t]].atkToCalvary * troop.count || 0
			atk.toInfantry += core.profiles[types[t]].atkToInfantry * troop.count || 0
			atk.toSiege += core.profiles[types[t]].atkToSiege * troop.count || 0
		}
		
		return atk;
	},
	calcDamage( attack, defense ){
		attack.damageGiven = {
			fromArchers: this.damageFrom(attack.troops, "Archers"),
			fromCalvary: this.damageFrom(attack.troops, "Calvary"),
			fromInfantry: this.damageFrom(attack.troops, "Infantry"),
			fromSiege: this.damageFrom(attack.troops, "Siege"),
			from( from, to ){
				to = battle.troopType(to)
				return this[`from${from}`][`to${to}`] * attack.weight
			}
		}
		
/*		console.log("Attack.damageGiven = ")
		console.log(attack.damageGiven)*/
		
		if ( defense.casualties === undefined ) {
			defense.casualties = {}
		}
		
		defense.damage = {}
		let dNames = Object.keys(defense.troops)
		for( let d = 0 ; d < dNames.length; d++){
			let dTroop = defense.troops[dNames[d]]
			
			let damage = attack.damageGiven.from("Archers", dNames[d])
			damage += attack.damageGiven.from("Calvary", dNames[d])
			damage += attack.damageGiven.from("Infantry", dNames[d])
			
			defense.damage[dNames[d]] = Math.min(dTroop.count, damage * (dTroop.count / defense.count))
			
			if ( defense.casualties[dNames[d]] === undefined ) defense.casualties[dNames[d]] = 0
			defense.casualties[dNames[d]] += defense.damage[dNames[d]]
		}
		
/*		console.log("Defense.damage: ")
		console.log(defense.damage)*/
	},
	inflictDamage( group ){
		group.count = 0
		
		core.troops.forEach( t => {
			group.troops[t].count -= group.damage[t]
			group.count += group.troops[t].count
		})

	},
	troopCount(group){
		if ( group.troops === null ) return 0
		
		let names = Object.keys(group.troops)
		group.count = 0
		
		for( let a = 0; a < names.length ; a++ ){
			group.troops[names[a]].count = parseInt(group.troops[names[a]].count)
			group.count += parseInt(group.troops[names[a]].count)
		}
	},
	prepare( group ){
		group.count = 0
		group.originalTroops = group.troops
		group.troops = {}
		
		if ( group.troops === null ) return
		
		let names = Object.keys(group.originalTroops)
	
	
		for ( let t = 0; t < names.length ; t++ ){
			group.troops[names[t]] = { count: group.originalTroops[names[t]].count }
			group.count += parseInt(group.troops[names[t]].count)
			group.originalTroops[names[t]].fph = core.profiles[names[t]].fph
		}
	},
	
	/*
	 * Troops Fight
	 */
	fight( attack, defense, result ){
		
		//Backing up original troops counts
		//Calculating general count
		this.prepare(attack)
		this.prepare(defense)
		
		while ( attack.count > 0 && defense.count > 0 ){
/*			console.log("\n---Start round---\n")*/
			
			attack.weight = defense.count === 0 ? 1 : attack.count / defense.count
			defense.weight  = 1 / attack.weight 
			
/*			console.log("Attack -> Defense")*/
			//Calculating Damage Attack to Defense
			this.calcDamage( attack, defense )
			
/*			console.log("Defense -> Attack")*/
			//Calculating Damage Defense to Attack
			this.calcDamage( defense, attack )
			
/*			console.log({
				attack: {
					count: attack.count,
					damage: attack.damage,
					casualties: attack.casualties
				}, defense : {
					count: defense.count,
					damage: defense.damage,
					casualites: defense.casualties
				}
			})*/
			
			//Inflicting damage on both sides
			this.inflictDamage(attack)
			this.inflictDamage(defense)
			
			if ( attack.action === core.mission.raid ) break
			
			/*console.log("\n---End round---\n")*/
		}
		
		//Rounding counts
		this.adjustCasualties(attack)
		this.adjustCasualties(defense)

		result.winner = attack.count === 0 ? defense : attack
		result.looser = attack.count === 0 ? attack : defense		
		result.winnerSide = attack.count === 0 ? "defense" : "attack"
		
		/*console.log("result :", result)*/
	},
	
	/*
	 *
	 * Main Entry Point
	 */
	attack( attack, move, endMission, saveReport, transaction ){
		
		let result = {}
		let defense = null
		
		attack.type = "mission"
		
		/*
		 * Loading Defense
		 *
		 */
		const objective = attack.origin
		
		if ( objective.type === "village" ){
			defense = core.villageSummary({ user: objective.owner, village: objective.id })
			defense.type = "village"
			defense.locks.troops
			defense.locks.resources
			if ( attack.siege ) defense.locks.buildings
			
			village.load(defense, transaction)
		}
		
		transaction.addAction( operation => {
			village.adjustModel( defense )
			
			//Troop Fight Logic
			battle.fight( attack, defense, result )
			
			//Raiding
			battle.loot(attack, defense, operation)

			//Siege
			if ( attack.siege && attack.siege.type === 'Catapult' ){
				siege.catapult(attack, defense, operation)
				console.log("Attack PostSiege", attack)
				console.log("Victim.delete", defense.delete)
			}

			//Report
			const report = battle.battleConclusion(attack.id, result, operation)
			saveReport(report, operation)
		})

		// Advance or End Mission depending on result
		transaction.addAction( op => {
			console.log("Winner:", result.winnerSide)
			
			if ( result.winnerSide === "attack" ) {
				getMissionLogic().setEvent('update-mission', attack.user, attack.id, transaction)
				move(attack, op)
			} else {
				endMission(attack.id, op)
			}
		})
	},

	/*
	 * Updates troops in both sides. Adds points per kill
	 */
	battleConclusion( missionId, result, transaction ){
		let defense = result.winnerSide === "defense" ? result.winner : result.looser
		let attack = result.winnerSide === "defense" ? result.looser : result.winner
		
		if ( defense.casualties ){
			let deathCount = 0
			for( let t in defense.troops ){
				const troopDeaths = defense.originalTroops[t].count - defense.troops[t].count 
				deathCount += core.profiles[t].fph * troopDeaths
			} 
			
			troops.casualties( defense, transaction )
			village.setUpdateEvent('village', defense.user, defense.village, transaction)
			users.addPoints(attack.user, deathCount, transaction)
		}
		
		if ( attack.casualties ){
			let deathCount = 0
			for( let t in attack.troops ){
				const troopDeaths = attack.originalTroops[t].count - attack.troops[t].count
				deathCount += core.profiles[t].fph * troopDeaths
			} 
			
			troops.casualties( attack, transaction )
			users.addPoints(defense.user, deathCount, transaction)
		}

		return battle.battleReport( missionId, result )
	},
	
	/*
	 * Looting village's resources
	 */
	loot(robber, victim, transaction){
		village.collect(robber, victim, 'loot', transaction)
	},
	
	/*
	 * Create report based on fight, loot and siege
	 * report = {
	 *   id, time, attack: [], defense: [], loot: Loot, village: Objective
	 *   siegeDamage: SiegeDamage
	 *   troops: { attack : Troops, defense: Troops }
	 * }
	 *
	 * SiegeDamage: []
	 *
	 *
	 */
	battleReport( missionId, result ){
		let report = {}
		let reporting = []
		
		let defense = result.winnerSide === "defense" ? result.winner : result.looser
		let attack = result.winnerSide === "defense" ? result.looser : result.winner

		console.log( "DefDel", defense.delete )	
		report.defensedDeleted = defense.delete	
		report.id = missionId
		report.time = new Date()
		report.attack = [attack.user]
		report.defense = [defense.user]
		report.loot = attack.loot
		report.village = attack.origin
		report.siegeDamage = attack.siegeDamage
		report.troops= {
			attack: {
				original: attack.originalTroops,
				after: attack.troops
			},
			defense: {
				original: defense.originalTroops,
				after: defense.troops
			}
		}
		
		return report
	}
}

module.exports = battle
