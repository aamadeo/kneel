// @flow

const trx = require('../lib/transactional')
const core = require('../core')
const buildings = require('../buildings')
const tasks = require('../tasks')

let mission = undefined

const getMissionLogic = () => {
	if ( mission === undefined ){
		mission = require('../mission')
	}
	
	return mission
}

let village = undefined
const getVillageLogic = () => {
	if ( village === undefined ){
		village = require('../village')
	}
	
	return village
}

const siege = {
	name: "siege",
	
	trebuchetDamage( {troops, siege}, victim){
		const trebuchets = troops.trebuchet.count
		
		const allBuildings =  victim.buildings.filter(b => b !== undefined ).map( b =>({
			village: victim.village,
			name: b.name,
			detail: b.detail,
			slot: b.slot,
			damage: 0,
			weight: 1
		}))
		
		const slotsPerBuilding = {}
		allBuildings.forEach( b => {
			let slots = slotsPerBuilding[b.name]
			
			if ( ! slots ){
				slots = []
				slotsPerBuilding[b.name] = slots
			}
			
			slots.push(b)
		})
		
		siege.targets.forEach( t => {
			const spb = slotsPerBuilding[t.building]
			
			let i = 0
			while( spb && spb[i].weight > 1 && i < spb.length ) i++
			
			if ( spb && i < spb.length ){
				spb[i].weight += t.weight*100
			}
		})
		
		for( let i = 0 ; i < trebuchets ; i++ ){
			let t = this.selectTarget( allBuildings )
			let target = allBuildings[t]
			
			target.damage += core.profiles.trebuchet.atkToBuilding
			
			if ( parseInt(target.damage) > target.detail.level ){
				
				if ( target.weight > 1 ){
					siege.targetsDestroyed = (siege.targetsDestroyed || 0) + 1
				}
				
				target.weight = 0
				siege.buildingsDestroyed = (siege.buildingsDestroyed || 0) + 1

				if ( siege.buildingsDestroyed === allBuildings.length )	break
			}
		}
		
		return allBuildings
	},
	
	selectTarget( targets ){
		
		const total = targets.reduce( (sum, t) => sum + t.weight, 0 )
		targets = targets.map( t => Object.assign({}, t, { weight: t.weight / total }))
		
		let i = 0
		let target = undefined
		let rnd = undefined
		
		while( target === undefined || target.weight == 0 ){
			rnd = Math.random()
			let floor = 0	
			i = 0

			while( rnd > floor + targets[i].weight ){
				floor = floor + targets[i].weight
				i++
			}
			
			target = targets[i]
		}
		
		return i
	},
	
	catapultDamage(totalDamage, siege, victim){
		const buildingsInDanger = {}
		
		const allBuildings =  victim.buildings.filter(b => b !== undefined ).map( b =>({
			village: victim.village,
			name: b.name,
			detail: b.detail,
			slot: b.slot,
			damage: 0
		}))
		
		siege.targets.forEach( t => {
			if ( buildingsInDanger.hasOwnProperty(t.building)) return
		
			buildingsInDanger[t.building] = allBuildings.filter( b => b.name === t.building)
		})
		
		const total = siege.targets.reduce((total, t) => {
			return total + parseInt(t.weight)
		}, 0)

		siege.targets.forEach( t => {
			if ( t.damage && t.damage == 0 ) return
			
			return t.damage = t.weight / total 
		})

		siege.targets.forEach( t => {
			let i = 0
		
			console.log("Attacking target", t)	
			//building in danger's slots
			const bids = buildingsInDanger[t.building]
			
			while( i < bids.length && bids[i].damage > 0 ) i++
			
			if ( i === bids.length ){
				let randomSlot = parseInt(allBuildings.length * Math.random())
				console.log("randomSlot:",randomSlot)
				
				allBuildings[randomSlot].damage += t.damage * totalDamage
			} else {
				bids[i].damage += t.damage * totalDamage
			}
		})
	

		return allBuildings.filter( b => b.damage >= 1)
	},
	
	doDamage(siegeDamage, victim, transaction){
		siegeDamage.forEach( b => {
			const village = getVillageLogic()
			
			b.damage = Math.floor(b.damage)
			
			if ( b.damage < 1 ) return
			
			b.detail.level = b.detail.level - b.damage
			console.log({ b })
			buildings.lock(b, transaction)
			buildings.damage(b, transaction)

			let selector = b.name == 'Building' ? ':build:' : ':upgradeBuilding:'

			selector = victim.village + selector + b.slot

			tasks.deleteTaskBySelector(selector, transaction)
			village.setUpdateEvent('village', victim.user, victim.village, transaction)

			if ( b.detail.level < 1 ){
				if ( core.spawningBuildings[b.name] ){
					selector = victim.village + ':spawn:' + b.slot + ':%'
					tasks.deleteTaskBySelector(selector, transaction)

					if ( b.name === 12 ) {
						selector = victim.village + ':spawnArmy:' + b.slot + ':%'
						tasks.deleteTaskBySelector(selector, transaction)
					}
				}

				if ( b.slot === 12 ){
					selector = victim.village + ':upgradeOccupation%'
					tasks.deleteTaskBySelector(selector, transaction)
				}

				if ( b.name === 'University'){
					selector = victim.village + ':research:' + b.slot + ':%'
					tasks.deleteTaskBySelector(selector, transaction)
				}

				if ( b.name === 'Blacksmith'){
					selector = victim.village + ':upgradeTroop:' + b.slot + ':%'
					tasks.deleteTaskBySelector(selector, transaction)
				}

				/* Tower ??? */
			}
		})
	},
	
	catapult(attack, victim, transaction){
		const totalDamage = attack.troops.catapult.count * core.profiles.catapult.atkToBuilding
		
		attack.siegeDamage = siege.catapultDamage(totalDamage, attack.siege, victim)

		this.doDamage(attack.siegeDamage, victim, transaction)
		
		const destroyed = ! victim.buildings.reduce( (r, b) => r || b && b.detail.level >= 0, false ) 
		
		
		// if no building left
		if ( destroyed ){
			console.log("Village Destroyed")
			const villageLogic = getVillageLogic()
			villageLogic.deleteVillage(victim, transaction)
		}
	},
	
	trebuchet( mission, transaction){
		const missionLogic = getMissionLogic()
		const village = getVillageLogic() 
		
		let objective = core.villageSummary({
			user: mission.objective.owner,
			village: mission.objective.id
		})
		
		objective.locks.buildings = true
		objective.ignore.troops = true
		objective.ignore.resources = true
		objective.ignore.occupations = true
		
		village.load(objective, transaction)
		missionLogic.loadTroops(mission, transaction)

		transaction.addAction( op => {
			const trebuchets = mission.troops.trebuchet.count
			
			mission.siege.buildingsDestroyed = undefined
			const damage = this.trebuchetDamage(mission, objective)

			this.doDamage(damage, objective, op)
			
			if ( ! mission.siegeDamage ){
				mission.siegeDamage = damage
			} else {
				damage.forEach( b => {
					const buildingDamage = mission.siegeDamage.find( prevDamage => prevDamage.slot === b.slot )
					
					if ( buildingDamage ){
						buildingDamage.detail.level -= parseInt(b.damage)
						buildingDamage.damage += parseInt(b.damage)
					} else {
						mission.siegeDamage.push(b)
					}
				})
			}
			
			console.log("siegeDamage", mission.siegeDamage)
			
			const totalBuildings = objective.buildings.filter(b => b !== undefined).length
			const { buildingsDestroyed, targetsDestroyed, targets, destroyAll } = mission.siege
			
			const allBuildingsDestroyed = buildingsDestroyed === totalBuildings
			const allTargetsDestroyed = targets.length > 0 && targets.length === targetsDestroyed
			
			mission.siege.over = allBuildingsDestroyed || !destroyAll && allTargetsDestroyed
			
			if ( mission.siege.timeLeft ){
				const { timeLeft, start } = mission.siege
				const now = new Date().getTime()
				mission.siege.over = mission.siege.over || (now-start) > timeLeft * 60000
			}
			
			if ( allBuildingsDestroyed ){
				const villageLogic = getVillageLogic()
				villageLogic.deleteVillage(objective, op)
			}
		})

		transaction.addAction( op => {
			
			const newReport = mission.report === undefined
			mission.report = this.trebuchetSiegeReport(mission, objective)
			mission.report.new = newReport
			
			if ( newReport ){
				missionLogic.setEvent('new-report', mission.user, mission.id, transaction)
				missionLogic.setEvent('new-report', objective.user, mission.id, transaction)
			} else {
				missionLogic.setEvent('update-report', mission.user, mission.id, transaction)
				missionLogic.setEvent('update-report', objective.user, mission.id, transaction)
			}
			
			missionLogic.saveSiegeReport(mission.report, op)
			missionLogic.setEvent(mission.user, mission.id, op)
			
			if ( mission.siege.over ){
				missionLogic.turnBack(mission, mission.to)
				missionLogic.move(mission, op)
			} else {
				//Task Setting
				Object.assign(mission,{
					job: 'trebuchetSiege',
					module: 'mission'
				})

				const time = 60 // core.speed
				
				tasks.setTask({ 
					user: mission.user,
					time,
					params: mission,
					visible: false,
					selector: 'mission:' + mission.id 
				}, op)
			}
		})

	},
	
	trebuchetSiegeReport(mission, objective){
		const report = {}
		
		report.id = mission.id
		report.time = new Date()
		report.attack = [mission.user]
		report.defense = [objective.user]
		report.village = mission.objective
		report.siegeDamage = mission.siegeDamage
		report.troops = {
			attack: {
				original: mission.troops,
				after: mission.troops
			},
			defense: {
				original: {},
				after: {}
			}
		}
		report.trebuchetSiege = true
		
		return report
	}

}

module.exports = trx.transactionalModule(siege)
