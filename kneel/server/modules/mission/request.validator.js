const core = require('../core')

const positiveNumber = (number, title, code) => {
	if ( !core.validNumber(number) || core.negativeNumber(number) ){
		throw {
			msg: `Invalid ${title} count : ` + number,
			code
		}
	}
}

const validTroop = troop => {
	if ( ! core.profiles[troop] ){
		throw {
			msg: "Troop unknown: " + troop,
			code: validator.err.invalidTroopName
		}
	}
}

const validAction = action => {
	if ( ! core.validAction(action) ){
		throw {
			code: validator.err.InvalidAction,
			msg: 'Invalid action: ', action
		}
	}
}

const validObject = object => typeof object === 'object'

const validPoint = ({x,y}) => {
	if ( !core.validNumber(x) ) {
		throw {
			code: validator.err.invallidPoint,
			msg: "Invalid value for x " + x
		}
	}

	if ( !core.validNumber(y) ) {
		throw {
			code: validator.err.invallidPoint,
			msg: "InvalidValue for y " + y 
		}
	}
}

const validString = (_string, title, code) => {
	if ( typeof _string !== 'string' || _string.length === 0 ){
		throw {
			msg: title + " must be a valid string : " + _string,
			code
		}
	}
}


const validator = {
	err:{
		invalidMissionId: -1,
		invalidAction: -2,
		invalidObjectiveType: -3,
		onlyVillageCanBeSieged: -4,
		invalidTroopCount: -5,
		invalidPath: -6,
	},
	
	getMissions({}){
		
	},
	
	getReports({}){
		
	},
	
	toggleLoop({ missionId }){
		if ( ! validNumber(missionId) ){
			throw {
				msg: 'Invalid Mission ID: ' + missionId,
				code: validator.err.invalidMissionId
			}
		}
	},
	
	execute({ troops, action, objective, path, siege }){	
		
		for( let troop in troops ){
			validTroop(troop)
			
			positiveNumber(troops[troop].count, "Troop Count", validator.err.invalidTroopCount )
			
			validRequest = true
		}
		
		validAction(action)
		validPoint(objective)
		
		let idx = ['village','troops'].findIndex(e => e === objective.type)
		if ( idx === -1 ){
			throw {
				msg: "Invalid type of objective : " + objective.type,
				code: validator.err.invalidObjectiveType
			}
		}
		
		positiveNumber(objective.id)
		
		if ( siege !== undefined){
			if ( objective.type !== 'village' ){
				throw {
					msg: "Only a village can be sieged" + objective.type,
					code: validator.err.onlyVillageCanBeSieged
				}
			}
		}

		/* PATH */
		throwInvalidPath = () => {
			throw {
				msg: "Invalid path" + path,
				code: validator.err.invalidPath
			}
		}
		if ( ! path ) {
			throwInvalidPath()
		} else if ( path.length < 2 ){
			throwInvalidPath()
		}

		path.forEach( p => {
			validPoint(p)
		})
	}
}

module.exports = validator