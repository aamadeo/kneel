const subject = require('./index')
const core = require('../core')
const transactions = require('../lib/transactional')
const utils = require('../lib/utils')
const async = require('async')


const EMPTY_TROOPS = {
	infantry: { count: 0 },
	pikeman: { count: 0 },
	archer: { count: 0 },
	scout: { count: 0 },
	"heavy-archer": { count: 0 },
	knight: { count: 0 },
	ram: { count: 0 },
	catapult: { count: 0 },
	trebuchet: { count: 0 },
}

const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number)

const validResource = r => (
	r.resource.length > 0 &&
	validNumber(r.r0) &&
	validNumber(r.capacity) &&
	validNumber(r.rps)
) 

const validResources = resources => validResource(resources.food) &&
		validResource(resources.iron) &&
		validResource(resources.stone) &&
		validResource(resources.wood)

const validOccupation = o => o.occupation.length > 0 && 
		validNumber(o.workers) &&
		validNumber(o.level)


const validOccupations = occupations => validOccupation(occupations.unoccupied) &&
		validOccupation(occupations.food) &&
		validOccupation(occupations.iron) &&
		validOccupation(occupations.stone) &&
		validOccupation(occupations.wood)

const validBuilding = b => b.name.length > 0 &&
		validNumber(b.slot) && 0 <= b.slot && b.slot <= 16 &&
		validNumber(b.detail.level) &&
		validNumber(b.free_after)
		
const reduceAnd = (a,b) => a && b

const validBuildings = buildings => buildings.map( b => b === null || validBuilding(b)).reduce( reduceAnd )

const validTroop = t => t.troop.length > 0 &&
		validNumber(t.count) &&
		t.upgrades !== undefined
const validTroops = troops => Object.keys(troops).map( t => validTroop(troops[t]) ).reduce( reduceAnd )

const validVillageSummary = summary => validResources(summary.resources) &&
			validOccupations(summary.occupations) &&
			validBuildings(summary.buildings) &&
			validTroops(summary.troops)

const setUpMissionTroops = missionId => {
	const transaction = transactions.transaction()

	transaction.addQuery({
		text: `delete from mission_troops where mission = $1`,
		name: "testMission.loadTroops.clearTroops",
		values: [missionId]
	})	
	
	core.troops.forEach( (troop, idx) => {
		transaction.addQuery({
			text: `insert into mission_troops values ($1, $2, $3)`,
			name: "testMission.loadTroops.insertTroops",
			values: [missionId, troop, idx]
		})	
	})
	
	return transaction
}

const setUpTrebuchetMission = (attack, target) => {
	const transaction = transactions.transaction()
	//Delete all missions tasks
	transaction.addQuery({
		text: `delete from tasks where "user" = $1 and selector like 'mission%'`,
		name: "testMission.setUpTrebuchetMission.clearTasks",
		values: [attack.user]
	})
	
	transaction.addQuery({
		text: `delete from mission_troops`,
		name: "testMission.setUpTrebuchetMission.clearMissionTroops",
		values: []
	})
	
	transaction.addQuery({
		text: `delete from path`,
		name: "testMission.setUpTrebuchetMission.clearMissionPaths",
		values: []
	})
	
	transaction.addQuery({
		text: `delete from map where type = 'troops'`,
		name: "testMission.setUpTrebuchetMission.clearMissionMapObjects",
		values: []
	})
	
	transaction.addQuery({
		text: `delete from missions where "user" = $1`,
		name: "testMission.setUpTrebuchetMission.clearMissions",
		values: [attack.user]
	})
	
	//10 Trebuchets Ready to go
	transaction.addQuery({
		text: "update village_troops set count = 10 where troop = 'trebuchet' and village = $1",
		name: "testMission.setUpTrebuchetMission.trebuchets",
		values: [attack.village]
	})
	
	//Buildings in Target
	const buildings = [
		[ 12, 'TownCenter', { level: 10 }],
		[ 0, 'Barracks', { level: 10 }],
		[ 1, 'Barn', { level: 10 }],
		[ 2, 'Warehouse', { level: 10 }]
	]
	
	transaction.addQuery({
		text: "delete from village_buildings where village = $1",
		name: "testMission.setUpTrebuchetMission.buildings",
		values: [target]
	})
	
	console.log("delete from village_buildings where village = ", target)
	
	buildings.forEach( b => {
		transaction.addQuery({
			text: "insert into village_buildings values ($1, $2, $3, $4)",
			name: "testMission.setUpTrebuchetMission.buildingsInsert",
			values: [ target, ...b]
		})
		
		console.log("insert into village_buildings values (", [target, ...b].reduce(
			(a,b) => (a + ", " + JSON.stringify(b))
		),")")
	})
	
	return transaction
}

const checkTask = ( selector, task ) => {
	const transaction = transactions.transaction()
	
	transaction.addQuery({
		text: `select * from tasks where "user" = 'JonSnow' and selector = $1`,
		name: "testVillage.startingCondition.checkTask",
		values: [selector]
	}, result => {
		Object.assign(task, result.rows[0])
	})
	
	return transaction
}

const tests = {
	testExecuteMission( callback ){
		const series = utils.series()
		
		const transaction = transactions.transaction()
		
		const params = { 
			user: 'test',
			village: 117,
			troops: { infantry: { count: 2 } },
			objective: { 
				x: -9,
				y: -3,
				type: 'village',
				id: 40,
				name: 'free village',
				owner: 'free',
				raidable: true,
				action: 'raid'
			},
			path: [ { x: -13, y: -3 }, { x: -9, y: -3 } ],
			loop: false,
			origin: { x: -13, y: -3 } 
		}
		
		const summary = core.villageSummary({ village: 117, user: 'test'})
		
		subject.execute(params, summary, transaction)
		series.add(transaction)
		
		series.execute( () => {
			console.log("Y DESPUES?")
			console.log(params)
			callback(true)
		})
	},
	
	testLoadTroops(callback){
		const series = utils.series()
		series.add(setUpMissionTroops(1))
		
		const transaction = transactions.transaction()
		const mission = { id: 1, troops: { invalid: true } }
		let result = true
		
		subject.loadTroops(mission, transaction)
		
		transaction.addAction( op => {
			core.troops.forEach( (troop,idx) => {
				result = result && mission.troops[troop].count === idx
			})
		})
		
		series.add(transaction)
		series.execute( () => {
			console.log("MissionTroops", mission.troops)
			callback(result)
		})
	},
	
	
	/*
	user	vid	x	y	W
	test 	117 4 4	H
	testv 119 7 0 H
	
	*/
	
	testTrebuchetMission(callback){
		const series = utils.series()
		const attack = core.villageSummary({ user: 'test', village: 117 })
		const target = { village: 119 }
		
		series.add(setUpTrebuchetMission(attack, target.village))
		
		const transaction = transactions.transaction()
		
		const troops = Object.assign({}, EMPTY_TROOPS)
		troops.trebuchet.count = 10
		
		const params = { 
			user: 'test',
			village: 117,
			troops,
			action: 'attack',
			objective: { 
				 x: 7,
			   y: 0,
			   type: 'village',
			   id: 119,
			   name: 'testv',
			   owner: 'test-v'
			},
			siege: {
				timeLeft: undefined,
				damageThreshold: undefined,
				destroyAll: true,
				targets: [
					{ building: 'TownCenter' },
					{ building: 'Warehouse' }
				]
			},
			path: [ { x: 4, y: 4 }, { x: 7, y: 0 } ],
			loop: false,
			origin: { x: 4, y: 4 } 
		}
		
		try {
			subject.execute(params, attack, transaction)
			series.add(transaction)
		} catch ( err ){
			console.log("err", err)
			return callback(false)
		}
		
		
		series.execute( () => {
			console.log(params)
			callback(true)
		})
	}
}


const executeTest = args => {
	if ( ! ( core.profiles.loaded && core.costs.loaded ) ){
		return setTimeout( () => executeTest(args), 1000 )
	}
	
	const results = {}
	const testCases = []
	const testName = testFunction => {
		let name = testFunction.replace("test","")
		name = name.charAt(0).toLowerCase() + name.substring(1)
		return name
	}
	
	for( let testFunction in tests ){
		
		console.log({ name: testName(testFunction), args } )
		
		if ( args && ! args.find( t => t === testName(testFunction) ) ) continue
		
		console.log("Testing: ", testFunction)
		
		let name = testName(testFunction)

		testCases.push( callback => {
			tests[testFunction]( result =>{
				const testCase = name
				
				console.log(" Done ", testCase)
				results[testCase] = result ? "success" : "error"
				
				callback(null)
			})
			
		})
	}

	async.series( testCases, (err) => {
		if ( err ) console.log(err)
		else console.log(results)
	} )
}

const targets = process.argv.filter( (e,i) => i > 1 )
console.log("Targets", targets)
executeTest( targets )
