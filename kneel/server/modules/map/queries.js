const queryManager = require('../lib/qm')

module.exports = queryManager({
	idSpace: 'map',
	queries:{		
		delete: 'delete from "map" where type = $1 and object = $2',
		insert: 'insert into map values( $1, $2, $3, $4, $5, $6, $7)',
		sectionCells: 'select x, y, object, type, detail from map where xs = $1 and ys = $2',
		cellByCoordinates: 'select * from map where x = $1 and y = $2',
		cellByIdentifier: 'select * from map where type = $1 and object = $2',
		updateCell: 'update map set detail = $1 where type = $2 and object = $3',
		move: `with a as (select avatar from users where "user" = $1)
			update "map" m 
         set x = $2, y = $3, xs = $5, ys = $6,
             detail = $4::jsonb || ('{ "avatar": "' || a.avatar  || '" }' )::jsonb
        from a
		   where type = 'troops' and object = $7`,
		inserTroops: `insert into "map"
			select $1, $2, 'troops', $3, $4, $5, $6::jsonb || ('{ "avatar": "' || u.avatar  || '" }' )::jsonb
        from "users" u
       where "user" = $7
		`
	},
})