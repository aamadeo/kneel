const map = require('./index')
const transactions = require('../lib/transactional')

const mapEvents = {
	handler: (socket, event) => {
		const { point } = event
		const { radius } = socket.handshake.session
		const transaction = transactions.transaction()
		const cells = []

		map.getSectionCells(point, cells, transaction)

		transaction.addAction( op => {
			socket.emit('update-map-section', { point, cells, radius, id: event.id })
		})

		transaction.finally = status => {
			if ( status !== 0 )	console.log("\n\nError:" + status)
		}

		transaction.run()
	},
	
		
	subscriptionValidator: subscription => subscription.match(/map-section\:-?(\d+):-?(\d+)/)
}

module.exports = mapEvents