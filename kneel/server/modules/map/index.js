const transactions = require('../lib/transactional')
const dao = require('./dao')
const core = require('../core')
const Event = require('../events/model').Event
const events = require('../events')
 
const map = {
	randomLocation( center, distance, occupied ){
		let x = parseInt(center.x + 2 * distance * Math.random()) - distance
		let y = parseInt(center.y + 2 * distance * Math.random()) - distance
		
		while ( occupied.has(`${x},${y}`) ){
			x = parseInt(center.x + 2 * distance * Math.random()) - distance
			y = parseInt(center.y + 2 * distance * Math.random()) - distance
		}
		
		if ( distance == 2 ){
			console.log("p:", {x, y})
		}
		
		return {x, y}
	},

	getCenter( gameRadius ){
		const s = gameRadius/3
		const k = Math.floor(Math.sqrt(s))
		//UserPerRing
		const upr = 2*k + 1
		const t = k * k

		// gameRadius*gameRadius

		// Vuelta entera => d = 2x
		const alpha = 2 * Math.PI * (s - t) / upr
		
		const x = parseInt( 7 * k * Math.cos(alpha))
		const y = parseInt( 7 * k * Math.sin(alpha))

		return {x, y}
	},
	
	sectionSize(radius){
	  return radius*2 + 1
	},
	
	rangeBySection(sx,sy, radius) { 
	  return { 
			left: sx * map.sectionSize(radius),
			bottom: sy * map.sectionSize(radius),      
			right: sx * map.sectionSize(radius) + map.sectionSize(radius),
			top: sy * map.sectionSize(radius) + map.sectionSize(radius)
	  };
	},
	getSection(p,radius){
	  return {
			x: Math.floor( (radius + p.x) / map.sectionSize(radius)),
			y: Math.floor( (radius + p.y) / map.sectionSize(radius)),
			equals(section){
				return this.x === section.x && this.y === section.y
			}
	  }
	},
	getRangeCenterAt(center, radius){
	  return {
			left: center.x - radius,
			bottom: center.y - radius,
			right: center.x + radius,
			top:  center.y + radius
	  };
	},
	sectionsInRange(range, radius) {
	  let sections = []
	  const alreadyInRange = new Set()

	  let ss = map.sectionSize(radius);

	  for(let x = range.left ; x < range.right ; x++ ){
			for(let y = range.bottom ; y < range.top ; y++ ){
				let section = map.getSection({x,y} , radius)
				let sid = section.x + "," + section.y

				if ( alreadyInRange.has(sid) ) continue
				alreadyInRange.add(sid)

				sections.push(section)
			}  
	  }

	  return sections
	},
	
	getSectionCells(section, cells,  transaction){
		dao.cellsInSection(section, cells, transaction)
	},

	getCells({location, radius, cells}, {x, y}, transaction){
		let center = location
		radius = parseInt(radius)

		if ( x !== undefined && y !== undefined ) {
			x = parseInt(x)
			y = parseInt(y)
			center = { x, y }
		}
		
		let range = map.getRangeCenterAt(center, radius)
		let sections = map.sectionsInRange(range, radius)

		for( let s = 0 ; s < sections.length; s++){
			dao.cellsInSection(sections[s], cells, transaction)
		}
	},
	
	getCell( object, transaction ){
		if ( object.x !== undefined && object.y !== undefined ){
			dao.getCellByCoordinates(object, transaction)
		} else if ( object.type !== undefined && object.id !== undefined ){
			dao.getCellByIdentifier(object, transaction)
		} else {
			throw "Can't get cells without {x,y} or {type, id} : " + JSON.stringify(object)
		}
	},
	
	updateCell( object, transaction ){
		dao.updateCell(object, transaction)
	},
	
	setMapSectionEvent({ section }, transaction){
		const event = new Event('map-section:'+section.x+":"+section.y)
		event.details = { point: section }
		
		/* 5min */
		event.lifespan = 30
		
		events.setEvent(event, transaction)
	},

	put(point, object, type, detail, transaction){
		point.section = map.getSection(point, core.radius)
		
		dao.put(point, object, type, detail, transaction)
		
		map.setMapSectionEvent(point, transaction)
	},

	move(type, object, from, to, detail, transaction){
		from.section = map.getSection(from, core.radius)
		to.section = map.getSection(to, core.radius)
		
		dao.move(type, object, from, to, detail, transaction)
		
		map.setMapSectionEvent(from, transaction)
		
		if ( from.section.x !== to.section.x || from.section.y !== to.section.y ){
			map.setMapSectionEvent(to, transaction)
		}
	},
	
	remove(type, object, transaction){
		
		const cells = []
		dao.getCellByIdentifier({type, id: object, cells }, transaction)
		
		transaction.addAction(op => {
			if ( cells.length === 0 ) return
			
			const section = map.getSection(cells[0])
			map.setMapSectionEvent({section}, op)
		})
		
		dao.remove(type, object, transaction)
	}
}

module.exports = transactions.transactionalModule(map)