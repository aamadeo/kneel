
const validString = (_string, title, code) => {
	if ( typeof _string !== 'string' || _string.length === 0 ){
		throw {
			msg: title + " must be a valid string : " + _string,
			code
		}
	}
}

const validator = {
	err: {
		invalidUser: -1,
		invalidPasswd: -2,
		invalidVillage: -3
	},
	
	free({ user }){
		validString(user, "User", validator.err.invalidUser)
	},
	
	newUser({ user, password, villageName }){
		validString(user, "User", validator.err.invalidUser)
		validString(password, "Password", validator.err.invalidPasswd)
		validString(villageName, "Village", validator.err.invalidVillage)
	},
	
	getUsers({}){
		
	},
	
	getUser({ user }){
		validString(user, "User", validator.err.invalidUser)
	},
	
	login({ user, pass }){		
		validString(user, "User", validator.err.invalidUser)
		validString(pass, "Password", validator.err.invalidPasswd)
	}
}

module.exports = validator