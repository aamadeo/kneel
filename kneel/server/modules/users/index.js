const map = require('../map')
const core = require('../core')
const bcrypt = require('bcryptjs')
const dao = require('./dao')
const diplomacy = require('../diplomacy')
const techonology = require('../technology')
const events = require('../events')
const village = require('../village')
let missions = undefined
const transactions = require('../lib/transactional')

const getMissionsLogic = () => {
	if ( ! missions ){
		missions = require('../mission')
	}
	
	return missions
}

const user = {
	name: "users",
	err: {
		userExistsError: "The user already exist",
		userDoesntExist: "User doesn't exist"
	},
	
	config(user, config, transaction){
		for ( attr in core ){
			if ( typeof core[attr] === "function" ) continue
			config[attr] = core[attr]
		}
		
		config.userTech = {}
		
		techonology.available(user, config.userTech, transaction)
	},
	
   auth({body, session}, result, transaction){
		let status = {}
		
		let user = {}
		let villages = []
		let authenticated = false
		
		dao.auth(body.user, user, transaction)
		
		transaction.addAction( op => {
			let clientHash = bcrypt.hashSync(body.pass, user.salt)
		
			if ( user.exists ) authenticated = clientHash === user.hash

			if ( ! authenticated ) transaction.abort()
		})
		
		dao.userVillages({ user: body.user, villages}, transaction)
		
		transaction.addAction( op => {
			result.authenticated = authenticated
	
			if ( authenticated ) {
				console.log("---Avatar=", user.avatar)
				result.user = body.user
				result.village = villages[0].name
				result.radius = core.radius
				result.location = {
					x: villages[0].x,
					y: villages[0].y
				}
				result.avatar = user.avatar

				session.location = result.location
				session.radius = result.radius // !!!
				session.user = result.user
				session.village = villages[0].id
				session.villageName = villages[0].name
				session.avatar = user.avatar
			}
		})
   },
	
	addPoints(user, points, transaction){
		dao.addPoints(user, points, transaction)
	},
	
	find( userInfo, transaction ){		
		dao.getUser( userInfo, transaction)
	},
	
	update( userInfo, transaction ){
		dao.update(userInfo, transaction)
	},
	
	newUser( userInfo, transaction ){
		const userLookup = {}
		const raidv = { 
			raidable: true, 
			user: "free"
		}
		let center = {}
		
		dao.getUser(userInfo, transaction)
		
		transaction.addAction( op => {
			if ( userInfo.exists ) {
				transaction.abort(user.err.userExistsError)
			}
		})
				
		userInfo.rounds = 5 // !! Todo: Prod 15
		userInfo.salt = bcrypt.genSaltSync(userInfo.rounds)
		userInfo.hash = bcrypt.hashSync(userInfo.password, userInfo.salt)
		
		const container = {}
		
		dao.userCount(container, transaction)
		
		//To Village
		village.newVillageId(userInfo, transaction)
		village.newVillageId(raidv, transaction)
		
		transaction.addAction( op => {
			Object.assign( userInfo.village, {
				name: userInfo.villageName,
				population: 21,
				infantryCount: 5,
				workersPerOccupation: 6,
				toBuild: [{name: "TownCenter", slot: 12}]
			})

			Object.assign(raidv.village, {
				name: "free village",
				population: 43,
				infantryCount: 0,
				workersPerOccupation: 10,
				toBuild: [
					{ name: "Barn", slot: 4},
					{ name: "Warehouse", slot: 8}
				]
			})

			if ( userLookup.exists ) return transaction.abort(this.err.userExistsError)

			center = map.getCenter(container.count)

			const { radius } = core
			map.getCells({ cells, radius }, center, op)
		})
		
		const cells = []
									 
		transaction.addAction( op => {
			const occupied = new Set()
			
			cells.forEach( cell => {
				if ( cell.type === "village" ) occupied.add(`${cell.x},${cell.y}`)
			})
			
			let p = map.randomLocation( center, core.distance.user, occupied )
			userInfo.village.position = p
			occupied.add(`${p.x},${p.y}`)
			
			raidv.village.position = 
				map.randomLocation( p, core.distance.raidv, occupied)

			dao.createUser( userInfo, op )
			village.initVillage( userInfo, op )
			village.initVillage( raidv, op )
			
			map.put(
				userInfo.village.position,
				userInfo.village.id,
				"village",
				{ name: userInfo.village.name, owner: userInfo.user },
				op
			)
			
			map.put(
				raidv.village.position,
				raidv.village.id,
				"village", 
				{ 
					name: raidv.village.name, 
					owner: raidv.user, 
					raidable: true 
				}, 
				op
			)
		})
	},
	
	getUsers( users, transaction ){
		dao.getUsers( users, transaction )
		
		transaction.addAction( op => {
			users.forEach( u => {
				diplomacy.info(u, op)
			})
		})
	},
	
	userVillages(userInfo, transaction){
		dao.userVillages(userInfo, transaction)
	},
	
	getUser( userInfo, transaction ){
		userInfo.villages = []
		
		dao.getUser(userInfo, transaction)
		
		transaction.addAction(op => {
			if ( ! userInfo.exists ){
				transaction.abort(village.err.userDoesntExist)
			}
		})
		
		dao.userVillages( userInfo, transaction )
		
		diplomacy.info(userInfo, transaction)
	},
	
	userCapacity(container, user, transaction){
		dao.userCapacity(container, user, transaction)
	},
	
	deleteUser(user, transaction){
		diplomacy.deleteUserTerms(user, transaction)
		techonology.deleteUserTechnologies(user, transaction)
		missionsLogic = getMissionsLogic()
		//missionsLogic.deleteUserMissions(user, transaction)
		events.deleteUserSubscriptions(user, transaction)
		
		dao.deleteUser(user, transaction)
	}
}

module.exports = transactions.transactionalModule(user)
