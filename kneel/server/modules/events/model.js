//@flow
const utils = require('../lib/utils')


class Event {
	key: string
	details
	owner: string
	lifespan: number
	
	constructor(key, owner){
		this.key = key
		this.owner = owner
		this.details = {}
	}
}

module.exports = {
	Event
}