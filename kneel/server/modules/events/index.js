const dao = require('./dao')
const transactions = require('../lib/transactional')
const tasks = require('../tasks')

const events = {
	loadEventsByUser( user, events, transaction ){
		dao.loadEventsByUser(user, events, transaction )
	},
	
	setEvent(event, transaction){
		dao.setEvent(event, transaction)
	},

	deleteUserSubscriptions(user, transaction){
		dao.deleteUserSubscriptions(user, transaction)
	},
	
	subscribe(user, subscription, transaction){
		dao.subscribe(user, subscription, transaction)
	},
	
	//Manage and validate subscriptions
	subscriptionsHandler(socket, getSubscriptionValidator){
		socket.on('subscribe', subscription => {
			const { user } = socket.handshake.session
			if ( ! user ) return
			
			console.log(`@${user} subscribing: `, subscription)

			const validator = getSubscriptionValidator(subscription)
			let validSubscription = false
			
			try {
				console.log({ validator, subscription })
				validSubscription = validator(subscription)
			} catch( err ){
				validSubscription = false
				console.log("\nError on subscribe:\n", err)
			}
			
			if ( validSubscription ) {
				const transaction = transactions.transaction()
				dao.subscribe(user, subscription, transaction)
				transaction.run()
			}
		})
		
		socket.on('unsubscribe', subscription => {
			const { user } = socket.handshake.session
			if ( ! user ) return
			
			console.log(`@${user} unsubscribing: `, subscription)
			dao.unsubscribe(user, subscription)
		})
	},
	
	eventsHandler(socket){		
		socket.on('remove-event', eventId => {
			const { user } = socket.handshake.session
			if ( ! user ) return

			dao.removeEvent(user, eventId)
		})
	},
	
	deleteOldEvents(params, transaction){
		dao.deleteOldEvents(transaction)
		
		//insert into tasks values ( 0, current_timestamp, current_timestamp, 'system', '{ "module": "events", "job" : "deleteOldEvents" }'
		tasks.setTask({
			user: null,
			time: 30,
			params,
			selector: 'deleteOldEvents'
		}, transaction)
	}
}

module.exports = transactions.transactionalModule(events)
