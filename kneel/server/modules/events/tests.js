const subject = require('./index')
const emitter = require('./emitter')
const core = require('../core')
const transactions = require('../lib/transactional')
const utils = require('../lib/utils')
const async = require('async')
const mapHandler = require('../map/eventHandler')


const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number)
		
const reduceAnd = (a,b) => a && b

const setUpEmitter = () => {
	const transaction = transactions.transaction()
	
	const sql = [
		`delete from event_subscriptions`,
		`insert into event_subscriptions values ( 'get-cells:0:0', 'test' )`,
		`delete from events`,
		`insert into events values ( 'get-cells:0:0', 1, '{ "point" : { "x": 0, "y": 0 } }', current_timestamp + '2h'::interval)`,
	].forEach( (text, step) => {
		transaction.addQuery({
			text,
			name: "testEvents.startingCondition."+step,
			args: []
		})	
	})

	return transaction
}

const prepareEvents = () => {
	const transaction = transactions.transaction()
	
	const sql = [
		`delete from event_subscriptions;`,
		`insert into event_subscriptions values ( 'attack-to-test', 'test' );`,
		`insert into event_subscriptions values ( 'attack-from-test', 'test' );`,
		`insert into event_subscriptions values ( 'resources-119', 'test' );`,

		`delete from events;`,
		`insert into events values ( 'resources-119', 1, '{ "resource" : 0 }', current_timestamp + '2h'::interval );`,
		`insert into events values ( 'attack-from-test', 2, '{ "attack" : "xyz" }', current_timestamp + '2h'::interval );`,
	].forEach( (text, step) => {
		transaction.addQuery({
				text,
				name: "testEvents.startingCondition."+step,
				args: []
			})	
	})

	return transaction
}

const tests = {
	emitter( callback ){
		const series = utils.series()
		series.add(setUpEmitter())
		
		try {
			const events = []
			const socket = { 
				emit: (event, obj) => {
					console.log({ event, obj })
				},
				handshake: {
					session: { user: 'test', radius: 5 }
				}
			}
			
			const handlers = {
				get: () => mapHandler
			}
			
			const transaction = transactions.transaction()
			
			transaction.addAction( op => {
				emitter(socket, handlers, 1000)	
			})
			
			series.add(transaction)

		} catch ( err ){
			console.log("err", err)
			return callback(false)
		}
		
		series.execute(() => {
			callback(true)
		})
	},
	
	loadEvents(callback){
		const series = utils.series()
		series.add(prepareEvents())
		
		try {
			const transaction = transactions.transaction()
			const events = []
			
			subject.loadEventsByUser('test', events, transaction)
			
			transaction.addAction( op => {
				events.forEach( e => console.log('Event', e))
			})
			
			series.add(transaction)
		} catch ( err ){
			console.log("err", err)
			return callback(false)
		}
		
		series.execute( () => {
			callback(true)
		})
	}
}


const executeTest = args => {
	if ( ! ( core.profiles.loaded && core.costs.loaded ) ){
		return setTimeout( () => executeTest(args), 1000 )
	}
	
	const results = {}
	const testCases = []
	const testName = testFunction => {
		let name = testFunction.replace("test","")
		name = name.charAt(0).toLowerCase() + name.substring(1)
		return name
	}
	
	for( let testFunction in tests ){
		
		console.log(testFunction)
		
		if ( args.length > 0 && ! args.find( t => t === testName(testFunction) ) ) continue
		
		console.log("Testing: ", testFunction)
		
		let name = testName(testFunction)

		testCases.push( callback => {
			tests[testFunction]( result =>{
				const testCase = name
				
				console.log(" Done ", testCase)
				results[testCase] = result ? "success" : "error"
				
				callback(null)
			})
			
		})
	}

	async.series( testCases, (err) => {
		if ( err ) console.log(err)
		else console.log(results)
	} )
}

const targets = process.argv.filter( (e,i) => i > 1 )
console.log("Targets", targets)
executeTest( targets )
