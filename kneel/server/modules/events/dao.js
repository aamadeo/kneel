const transactions = require('../lib/transactional')
const utils = require('../lib/utils')

const qm = require('../lib/qm')({
	idSpace: 'subscriptions',
	queries: {
		loadEventsByUser: `
			select e.*
			  from event_subscriptions es, events e
			 where es.user = $1
				and es.event_key = e.event_key
				and ( validity > current_timestamp )`,
		deleteOldEvents: `delete from events where validity < current_timestamp`,
		deleteUserSubscriptions: `delete from event_subscriptions where "user" = $1`,
		hasSubscription: `select 1 from event_subscriptions where "user" = $1 and event_key = $2`,
		subscribe: `insert into event_subscriptions values ($2, $1)`,
		unsubscribe: `delete from event_subscriptions where "user" = $1 and event_key = $2`,
		getId: `select nextval('event_seq') as id`,
		setEvent: `insert into events values ($1, $2, $3, current_timestamp + $4, $5)`,
		removeEvent: `delete from events where owner = $1 and id = $2`
	}
})

const voidHandlers = { resultHandler: () => {} }

const events = {	
	loadEventsByUser(user, events, transaction){
		qm.addQuery({
			name: "loadEventsByUser",
			args: [user],
			perRow: row => events.push(Object.assign({ key: row.event_key, id: row.id }, row.details))
		}, transaction)
	},
	
	subscribe(user, subscription, transaction){
		let hasSubscription = false
		
		qm.addQuery({
			name: "hasSubscription",
			args: [user, subscription],
			perRow: row => {
				hasSubscription = true
			}
		}, transaction)
		
		transaction.addAction( op => {
			if ( hasSubscription ) return
			
			qm.addSimpleQuery("subscribe", [user, subscription], op)
		})
	},
	
	unsubscribe(user, subscription){
		qm.singleQueryExec("unsubscribe", [user, subscription], voidHandlers)
	},

	deleteUserSubscriptions(user, transaction){
		qm.addSimpleQuery('deleteUserSubscriptions', [user], transaction)
	},

	setEvent({ key, owner, lifespan, details }, transaction){
		let eventId = null
		
		qm.addQuery({
			name: 'getId',
			perRow: row => { 
				eventId = row.id
			}
		}, transaction)
		
		transaction.addAction( op => {
			qm.addSimpleQuery("setEvent", [key, eventId, details, utils.toPgInterval(lifespan) , owner], op)
		})
	},
	
	removeEvent(user, eventId){
		qm.singleQueryExec("removeEvent", [user, eventId], voidHandlers)
	},
	
	deleteOldEvents(transaction){
		qm.addSimpleQuery("deleteOldEvents", [], transaction)
	}
}

module.exports = transactions.transactionalModule(events)
