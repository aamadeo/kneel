const events = require('./index')
const transactions = require('../lib/transactional')

let emmiter = (socket , getHandler) => {
	if ( ! socket.handshake.session.user ) return false
	if ( ! socket.eventsHandled ){
		socket.eventsHandled = new Set()
	}
	
	const transaction = transactions.transaction()
	const { user } = socket.handshake.session
	
	try {
		const subscriptions = []
		const userEvents = []
		events.loadEventsByUser(user, userEvents, transaction)

		transaction.finally = status => {
			if ( status !== 0 ){
				console.log('[WSE] Error : ', status)
				return
			}
			
			userEvents.forEach( e => {
				const eventHandled = socket.eventsHandled.has(e.id)
				if ( eventHandled  ) return
				
				const handler = getHandler(e.key)
				
				try {
					console.log("Emitting event: ", e.key)
					handler(socket, e)
					socket.eventsHandled.add(e.id)
				} catch ( err ){
					if ( handler ) console.log(`[WSE][${handler.name}] Error :`, err)
					else console.log(`[WSE] No handler matched for EventKey: ${e.key}`)
				}
			})
		}

		transaction.run()
	} catch (err){
		console.log( { err })
	}
	
	return true
}

const recurringCall = (socket, getHandler, interval) => {
	if ( emmiter(socket, getHandler) ) {
		setTimeout( () => recurringCall(socket, getHandler, interval), interval)
	}
}

module.exports = recurringCall