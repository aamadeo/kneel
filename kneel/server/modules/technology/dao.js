const queryManager = require('../lib/qm')
const qm = queryManager({ 
	idSpace: 'tech',
	queries: {
		tech: `select * from user_technologies where "user" = $1`,
		delete: `delete from user_technologies where "user" = $1`
	}
})
const trx = require('../lib/transactional')

module.exports = trx.transactionalModule({
	name: "tech.dao",
	available(user, technologies, transaction){
		qm.addQuery({
			name: "tech", 
			args: [user],
			perRow: row => { technologies[row.technology] = row }
		}, transaction)
	},
	
	deleteUserTechnologies(user, transaction){
		qm.addSimpleQuery("delete", [user], transaction)
	}
})