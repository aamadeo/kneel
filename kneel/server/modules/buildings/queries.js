const queryManager = require('../lib/qm')

module.exports = queryManager({
	idSpace: 'buildings',
	queries:{		
		/*Buildings*/
		lock: 'select * from village_buildings where village = $1 and slot = $2 for update',
		startBuilding: 'insert into village_buildings values ($1, $2, $3, $4, current_timestamp)',
		finishBuilding: 'update village_buildings set building = $1, free_after = current_timestamp where village = $2 and slot = $3',
		upgrade: 'update village_buildings set detail = $1, free_after = COALESCE($2, current_timestamp) where village = $3 and slot = $4',
		update: 'update village_buildings set free_after = COALESCE(current_timestamp + $1, current_timestamp) + $2, detail = $3 where village = $4 and slot = $5',
		damage: 'update village_buildings set detail = $3 where village = $1 and slot = $2',
		destroy : 'delete from village_buildings where village = $1 and slot = $2'
	},
})