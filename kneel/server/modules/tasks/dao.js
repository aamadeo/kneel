let transactions = require('../lib/transactional')
let qm = require('./queries')
let utils = require('../lib/utils')

let tasks = {
	name: "tasks.dao",
	newTaskId(task, transaction){
		qm.addQuery({
			name: "getId",
			args: [],
			resultHandler: result => { task.id = result.rows[0].nextval }
		}, transaction)
	},
	
   setTask( task, transaction ){
		let time = task.time
		
		if ( task.start != null ) time += task.start
		
		let interval = utils.toPgInterval( time )
		const args = [ task.id, task.start, interval, task.user, task.params, task.visible, task.selector]

		qm.addSimpleQuery( "insert", args, transaction)
   },
	
   delete(id, transaction){
		qm.addSimpleQuery("delete",[id], transaction)
   },
	
	getTasks(resultHandler){
		let _resultHandler =  result => resultHandler(result.rows)
		
		qm.singleQueryExec( "load", [], { resultHandler: _resultHandler } )
	},
	
	getTaskBySelector( selector, container, transaction ){
		qm.addQuery({
			name: "getBySelector",
			args: [selector],
			resultHandler: result => { container.task = result.rows[0] }
		}, transaction)
	},
	
	getTaskById( id, task, transaction ){
		qm.addQuery({
			name: "getById",
			args: [id],
			perRow: row => Object.assign(task, row)
		}, transaction)
	},
	
	deleteTaskBySelector( selector, transaction ){
		qm.addSimpleQuery("deleteBySelector", [selector], transaction)
	},
	
	update( { id, params }, transaction ){
		qm.addSimpleQuery("update", [id, params], transaction)
	},
	
	getUserTasks(user, village, callback){
		let slots = []
		
		let handler = {
			resultHandler( result ){
				callback(slots)
			},
			perRow(row, result){
				if ( row.params.village !== village ) return
				
				if ( slots[row.params.slot] === undefined ) slots[row.params.slot] = []
				
				let tasks = slots[row.params.slot]
				row.index = tasks.length
				tasks.push(row)
			}
		}
		
		qm.singleQueryExec("userTasks", [user], handler )
	},
	
	lock(id, transaction){
		qm.addSimpleQuery("lock",[id], transaction)
	}
}

module.exports = transactions.transactionalModule(tasks)