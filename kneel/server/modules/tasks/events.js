const tasks = require('./index')
const core = require('../core')
const transactions = require('../lib/transactional')

const taskEvents = {
	handler(socket, e){
		if ( e.key.match(/task-end/) ){
			console.log("TaskEvent task-end", e)
			socket.emit('task-end', { task: e.task , id: e.id })
			
		} else if ( e.key.match(/task-start/) ){
			const transaction = transactions.transaction()
			const task = {}
			console.log("TaskEvent", e)
			tasks.getTaskById(e.task.id, task, transaction)
			
			transaction.addAction( op => {
				socket.emit('task-start', { task, id: e.id },)
			})
			
			transaction.finally = status => {
				if ( status !== 0 )	console.log("\n\nError:" + status)
			}

			transaction.run()
		}
	}
}

module.exports = taskEvents