const dao = require('./dao')
const Event = require('../events/model').Event
const trx = require('../lib/transactional')

let events = undefined

function getEventsLogic(){
	if ( ! events ){
		events = require('../events')
	}
	
	return events
}

const tasks = {
	taskEvent(task, type){
		const event = new Event(type)
		const { id, params: { slot }, user } = task
		event.owner = user
		event.details = { task: { id, slot  } }
		event.lifespan = 10
		
		return event
	},
	
   setTask({ user, time, params, visible, start, selector }, transaction){
		let task = { user, params, time, visible, start, selector }
      
		//Explicit conversion of undefined and null to false
		task.visible = !! task.visible
		
		dao.newTaskId(task, transaction)
		transaction.addAction( op => {
			if ( task.visible ){
				getEventsLogic().setEvent(tasks.taskEvent(task, 'task-start:'+user), op)
			}
					
			dao.setTask(task, op)
		})
   },
	
	userTasks({user, village}, callback){
		dao.getUserTasks(user, village, callback)
	},
	
	getTaskBySelector( selector, container, transaction){
		dao.getTaskBySelector(selector, container, transaction)
	},
	
	getTaskById( id, task, transaction ){
		dao.getTaskById(id, task, transaction)
	},
	
	deleteTaskBySelector( selector, transaction ){
		dao.deleteTaskBySelector(selector, transaction)
	},
	
	deleteTaskById(task, transaction){
		if ( task.visible ){
			const event = tasks.taskEvent(task, 'task-end:'+task.user)
			console.log("Setting task-end.Event : ", event)
													
			getEventsLogic().setEvent(event, transaction)
		}
		
		dao.delete(task.id, transaction)
	},
	
	update( task, transaction){
		dao.update( task, transaction)
	}
}

module.exports = trx.transactionalModule(tasks)