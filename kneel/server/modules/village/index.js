const core = require('../core')
const tasks = require('../tasks')
const troops = require('../troops')
const buildings = require('../buildings')
const technologies = require('../technology')
const trx = require('../lib/transactional')
const map = require('../map')
const events = require('../events')
const { Event } = require('../events/model')

const dao = require('./dao')

//circular dependency
let users = null
const getUserLogic = () => {
	if ( users === null ) users = require('../users')
	
	return users
}

const addPoints = (user, points, transaction) => {
	const userLogic = getUserLogic()
	userLogic.addPoints(user, points, transaction)
}

const village = {
	name: "village",
	err:{
		notEnoughResources: "Not enough resources",
		noUnoccupiedPeasant: "There's no peasant available",
		techNoAvailable: "The technology is not available",
		wrongSlot: "This operation isn't available on this slot",
		noMarket: "You can't trade in a village with no market",
		cantSpawnTroopWithoutBuilding: "You can't spawn troops without the correspondent building",
		slotOccupied: "This slot has already a building"
	},
	
	enoughResources( resources, price ){
      let rr = {
         stone: resources.stone.r0-price.stone,
         iron: resources.iron.r0-price.iron,
         wood: resources.wood.r0-price.wood,
         food: resources.food.r0-price.food,
         time: price.time
      };
      
      rr.charge = () => {
         resources.stone.r0 = rr.stone
         resources.iron.r0 = rr.iron
         resources.wood.r0 = rr.wood
         resources.food.r0 = rr.food
      }
      
      rr.enough = rr.stone >= 0 && rr.food >= 0 && rr.iron >= 0 && rr.wood >= 0
      
      return rr;
   },
	
	buyItem(summary, {item, multiplier}){
		
		let costs = core.costs
		let price = {
			stone: Math.round(costs[item].stone * multiplier),
			wood: Math.round(costs[item].wood * multiplier),
			food: Math.round(costs[item].food * multiplier),
			iron: Math.round(costs[item].iron * multiplier)
		}
		
/*		console.log({ price })*/

		let resourceReqs = village.enoughResources(summary.resources, price)

		if ( resourceReqs.enough ){
			resourceReqs.charge()

			let time = parseInt(costs[item].time) * 1000
			time = Math.round(time * multiplier)

			return true

		} else {
			summary.shortage = resourceReqs
			//console.log(resourceReqs)

			return false
		}
	},
	
	adjustModel(summary){
		const rps = core.rps
		const names = Object.keys(summary.resources)
		const storage = summary.buildings.filter((b) => {
			return b.name === "Warehouse" || b.name === "Barn"
		})

		core.resources.map((r)=>{
			summary.resources[r].capacity = core.storage.deposit
		})

		for ( let s = 0 ; s < storage.length ; s++ ){
			let building = storage[s]
			let increment = core.storage.base * Math.pow(core.storage.coeficient, building.detail.level)
			
			increment = Math.round(increment / 10) * 10

			if ( building.name === "Warehouse"){
				summary.resources.iron.capacity += increment
				summary.resources.wood.capacity += increment
				summary.resources.stone.capacity += increment
			} else {
				summary.resources.food.capacity += increment
			}
		}
		
		core.resources.map((rName)=>{
			let r = summary.resources[rName]
			let o = summary.occupations[rName]
			
			r.rps = o.workers * rps.base * Math.pow(rps.coeficient, o.level)
			
			if ( rName === "food" ){
				r.rps -= summary.population / 3600
			}
			
			let dt = r.dt.days !== undefined ? r.dt.days * 24 * 3600 : 0
			dt += r.dt.hours !== undefined ? r.dt.hours*3600 : 0
			dt += r.dt.minutes !== undefined ? r.dt.minutes*60 : 0
			dt += r.dt.seconds !== undefined ? r.dt.seconds : 0

			r.r0 = r.r0 + ( dt ) * r.rps
			r.r0 = Math.max(0, Math.min( r.capacity, r.r0 ))

			delete r.dt
		})
	},
	
	buildingSlot( buildings, building ){
		return buildings.findIndex( b => b !== undefined && b.name === building )
	},
	
	getVillageTroops(summary, data, transaction){
		summary.ignore.occupations = true
		summary.ignore.resources = true
		summary.ignore.buildings = true
		
		dao.load(summary, transaction)
		
		transaction.addAction( op => {
			console.log("Troops: ", summary.troops)
		})
	},
	
	/* Service Handlers */
	// GET https://host/villages/
	getVillage(summary, body, transaction){
		dao.load(summary, transaction)
		
		if ( ! summary.ignore.adjustment ){
			transaction.addAction(() => {
				this.adjustModel(summary)
			})	
		}
	},
	
	// POST https://host/villages/sell
	sell(summary, {buy, sell}, transaction){
		summary.locks.resources = true
		
		dao.load(summary, transaction)
		
		transaction.addAction((curOp) => {
			this.adjustModel(summary)
			
			let hasMarket = false
			summary.buildings.forEach( b => { hasMarket = hasMarket || b.name === "Market"})
			
			if ( ! hasMarket ){
				return transaction.abort(village.err.noMarket)
			}
			
			let selling = summary.resources[sell.resource]
			let buying = summary.resources[buy]
			
			if ( selling.r0 < sell.count ) return transaction.abort(village.err.notEnoughResources)
			
			const sellMax = selling.r0 / selling.capacity
			const buyMax = buying.r0 / buying.capacity
			const price = Math.min( core.market.topPrice, sellMax / buyMax )
			const buyCount = sell.count / price
			
			console.log({
				actual: selling.r0,
				selling: sell.count
			})
			
			selling.r0 -= sell.count
			buying.r0 += buyCount
			
			village.updateResources( summary, sell.resource, curOp )
			village.updateResources( summary, buy, curOp )
			village.setUpdateEvent('resources', summary.user, summary.village, transaction)
		})
	},
	
	// POST https://host/villages/spawnArmy
	spawnArmy(summary, troopList, transaction ){
		const specs = new Map()
		
		technologies.available(summary.user, summary.userTech, transaction)
	
		const villageName = summary.village
		
		transaction.addAction(() => {
			for( let troop in troopList ){
				if ( troopList[troop] > 0 && ! core.techAvailable(summary.userTech, troop) ){
					transaction.abort(village.err.techNoAvailable)
				}
				
				troops.lock(summary.village, troop, transaction)
				let building = core.spawningBuilding( troop )
				let slot = this.buildingSlot( summary.buildings, building )
				buildings.lock( { slot, village: villageName } , transaction)
			}
		})
		
		summary.locks.resources = true
		dao.load(summary, transaction)
		
		transaction.addAction( curOp => {
			
			for ( let troop in troopList ){
				if ( troopList[troop] < 1 ) continue
				
				let duration = parseInt(core.costs[troop].time)
				let building = core.spawningBuilding(troop)
				
				let unit = {
					troop,
					count: troopList[troop]
				}

				if ( ! specs.has(building) ){
					let item =  duration
					let batchDuration = duration * troopList[troop]
					
					let slot = this.buildingSlot( summary.buildings, building )
					
					if ( slot === -1 ) {
						return transaction.abort(village.err.cantSpawnTroopWithoutBuilding)
					}
					
					let units = [ unit ]
					
					specs.set(building, { item, batchDuration, building, slot, units })
				} else {
					let spec = specs.get(building)
					
					spec.units.push({ troop, count: troopList[troop] })
					spec.batchDuration += duration * troopList[troop]
					specs.set(building, spec)
				}

				let expense = {
					multiplier: troopList[troop],
					item: troop
				}

				let enough = this.buyItem(summary, expense)

				if ( ! enough ) {
					return transaction.abort(village.err.notEnoughResources)
				}
			}
			
			specs.forEach( spec => {
				let duration = spec.batchDuration
				
				let start = null
				let b = summary.buildings[spec.slot]
				if ( ! b.free ) start = b.free_after
				
				spec.start = start
				
				const detail = b.detail	
				buildings.update({ 
					slot: spec.slot, 
					village: villageName,
					detail, 
					duration, 
					start 
				}, curOp)
			})
		})
		
		this.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('resources', summary.user, summary.village, transaction)
		
		transaction.addAction( op => {
			specs.forEach( spec => {
				let params = {
					user: summary.user,
					slot: spec.slot,
					village: summary.village,
					units: spec.units,
					desc: 'Spawning ' + spec.units[0].troop,
					job: 'spawnArmyUnit',
					module: 'village'
				}

				tasks.setTask({
					user: summary.user,
					time: spec.item,
					visible: true,
					start: spec.start,
					params,
					selector: summary.village + ":spawnArmy:" + spec.slot
				}, op)
			})
		})
	},
	
	// POST https://host/villages/spawn
	spawn(summary, { slot, type, name }, transaction){
		summary.locks.resources = true
		
		const spec = { 
			village: summary.village,
			slot,
			type,
			item: name,
			start: null,
			multiplier: 1,
			duration: parseInt(core.costs[name].time)
		}
		
		dao.load(summary, transaction)
		buildings.lock(spec, transaction)
		
		if ( spec.name === "peasant" ){
			dao.lockOccupation(summary.village, 'unoccupied', transaction)
		} else {
			troops.lock(summary.village, name, transaction)
		}
		
		technologies.available(summary.user, summary.userTech, transaction)
		
		transaction.addAction(() => {
			this.adjustModel(summary)
			
			if ( ! core.techAvailable(summary.userTech, name) ){
				transaction.abort(village.err.techNoAvailable)
			}
			
			const sBuilding = core.spawningBuilding( name )
			
			if ( summary.buildings[slot].name !== sBuilding ){
				console.log(name, sBuilding)
				transaction.abort(village.err.wrongSlot)
			}
		})
		
		transaction.addAction((curOp) => {
			
			let enough = this.buyItem(summary, spec)
			
			if ( ! enough ) {
				return transaction.abort(village.err.notEnoughResources)
			}
			
			let b = summary.buildings[slot]

			if ( ! b.free ) spec.start = b.free_after
			spec.detail = summary.buildings[slot].detail
			
			buildings.update(spec, curOp)
		})
		
		this.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('resources', summary.user, summary.village, transaction)

		transaction.addAction ( op =>{
			let params = {
				user: summary.user,
				slot,
				village: summary.village,
				type: name,
				desc: 'Spawning ' + name,
				job: 'spawnUnit',
				module: 'village'
			}
			
			tasks.setTask({ 
				user: summary.user,
				time: spec.duration,
				visible: true,
				start: spec.start,
				params,
				selector: summary.village + ":spawn:" + slot + ":" + name
			}, op)
		})
	},

	// POST https://host/villages/build
	newBuilding(summary, spec, transaction){
		if ( spec.slot === 12 && spec.name !== "TownCenter"){
			//Todo: Mejor Error
			transaction.abort("Only TownCenter can be built in slot 12")
			return
		}
		
		const rps = core.rps

		if ( spec.slot > 12 && spec.name !== "Tower"){
			//Todo: Mejor Error
			transaction.abort("Only Tower can be built in slot " + spec.slot)
			return
		}

		summary.locks.resources = true
		summary.start = new Date().getTime()
		spec.village = summary.village
		
		dao.load(summary, transaction)
		technologies.available(summary.user, summary.userTech, transaction)
		buildings.lock(spec, transaction)
		dao.lockOccupation(summary.village, 'unoccupied', transaction)
		dao.lockOccupation(summary.village, 'builders', transaction)
		
		transaction.addAction(()=>{
			if ( summary.buildings[spec.slot] !== undefined ){
				summary.slotOccupied = true
				transaction.abort(village.err.slotOccupied)
			}
			
			if ( ! core.techAvailable(summary.userTech, spec.name) ){
				transaction.abort(village.err.techNoAvailable)
			}
		})
		
		transaction.addAction(() => {
			this.adjustModel(summary)
			
			console.log({ b: spec.builders, u: summary.occupations.unoccupied.workers })
			if ( spec.builders > summary.occupations.unoccupied.workers ) transaction.abort(village.err.noUnoccupiedPeasant)
		})
		
		dao.lockOccupation(summary.village, 'unoccupied', transaction)
		dao.lockOccupation(summary.village, 'builders', transaction)
		
		transaction.addAction(() => {
			spec.multiplier = 1
			spec.item = spec.name 
			
			let enough = this.buyItem(summary, spec)
			
			if ( ! enough ) transaction.abort("")
			
		})
		
		dao.moveWorker(summary.village, 'builders', parseInt(spec.builders), transaction)
		village.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('resources', summary.user, summary.village, transaction)
		let start = null

		let params = {
			village: summary.village,
			user: summary.user,
			building: spec.name,
			builders: spec.builders,
			desc: "Building " + spec.name + " on slot " + spec.slot,
			slot: spec.slot,
			job: 'build',
			module: 'village'
		}

		transaction.addAction( op => {
			summary.occupations.unoccupied.workers = summary.occupations.unoccupied.workers - spec.builders
			summary.occupations.builders.workers = summary.occupations.unoccupied.workers + spec.builders
			
			const buildSpeed = Math.pow( rps.coeficient, summary.occupations.builders.level / 2 ) * spec.builders
			let time = core.costs[spec.name].time / buildSpeed
			
			let b = summary.buildings[spec.slot]
			
			console.log({ spec })
			
			tasks.setTask({ 
				user: summary.user,
				time,
				visible: true,
				start: null,
				params,
				selector: summary.village + ":build:" + spec.slot
			}, op)
		})
		
		buildings.startBuilding( {
			village: summary.village,
			building: "Building",
			slot: params.slot,
			detail: { level: 0, nextLevel: 1, builders: 0}
		}, transaction )
	},

	// POST https://host/villages/upgrade
	upgrade(summary, {slot, builders}, transaction){
		const rps = core.rps
		summary.locks.resources = true
		
		const spec = { 
			village: summary.village,
			slot,
			builders
		}
		
		dao.load(summary, transaction)
		buildings.lock(spec, transaction)
		dao.lockOccupation(summary.village, 'unoccupied', transaction)
		dao.lockOccupation(summary.village, 'builders', transaction)
		
		transaction.addAction(curOp => {
			this.adjustModel(summary)
			
			builders = summary.buildings[slot].detail.builders ||  builders
			
			const buildSpeed = Math.pow( rps.coeficient, summary.occupations.builders.level / 2 ) * builders
			
			spec.detail = summary.buildings[slot].detail
			spec.detail.nextLevel = spec.detail.nextLevel || 1
			spec.detail.builders = builders //To update BuildingInfo
			spec.item =  summary.buildings[slot].name
			spec.multiplier = Math.pow(core.price.coeficient, spec.detail.level)
			spec.duration = spec.multiplier * core.costs[spec.item].time / buildSpeed
					
			let enough = this.buyItem(summary, spec)
			
			if ( ! enough ) transaction.abort(village.err.notEnoughResources)
			
			if ( spec.detail.nextLevel - spec.detail.level == 1 ){
				 if ( spec.builders > summary.occupations.unoccupied.workers ) {
					 transaction.abort(village.err.noUnoccupiedPeasant)
				 }
				
				dao.moveWorker(summary.village, 'builders', parseInt(builders), curOp)
			}
			
			spec.detail.nextLevel = parseInt(spec.detail.nextLevel) + 1
		})
		
		village.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('resources', summary.user, summary.village, transaction)
		
		transaction.addAction(curOp => {
			let params = {
				village: summary.village,
				slot: slot,
				module: "village",
				job: "upgradeBuilding",
				desc: "Upgrading " + spec.item + " to Lv " + (spec.detail.level+1)
			}
			
			let b = summary.buildings[spec.slot]
			spec.start = null
			
			if ( ! b.free ) spec.start = b.free_after
			
			buildings.update(spec, curOp)
			tasks.setTask({ 
				user: summary.user,
				time: spec.duration,
				visible: true,
				start: spec.start,
				params,
				selector: summary.village + ":upgradeBuilding:" + spec.slot
			}, curOp)
		})
	},
	
	// POST https://host/villages/research
	research(summary, {technology, slot, icon}, transaction){
		summary.locks.resources = true
		
		buildings.lock({ village: summary.village, slot }, transaction)
		dao.load(summary, transaction)
		technologies.available(summary.user, summary.userTech, transaction)
			
		transaction.addAction( op => {
			this.adjustModel(summary)
			
			//Check that technology is available
			if ( ! core.techAvailable(summary.userTech, technology) ){
				transaction.abort(village.err.techNoAvailable)
			}
			
			if ( summary.buildings[slot].name !== "University" ) {
				return transaction.abort(village.err.wrongSlot)
			}//Check that slot has University
			
			const enough = this.buyItem( summary, { item: technology, multiplier: 1 })
			
			if ( ! enough ){
				transaction.abort(village.err.notEnoughResources)
			}
		})
		
		village.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('resources', summary.user, summary.village, transaction)
		village.setTechEvent('technology', summary.user, technology, transaction)
		
		transaction.addAction( op =>{
			const spec = {
				village: summary.village,
				slot,
				duration: core.costs[technology].time,
				start: null
			}
			
			if ( ! summary.buildings[slot].free ){
				spec.start = summary.buildings[slot].free_after
				
			}
			spec.detail = summary.buildings[slot].detail
			
			const params = {
				village: summary.village,
				user: summary.user,
				technology,
				desc: 'Researching: ' + technology,
				job: 'enableTech',
				icon,
				module: 'village',
				slot
			}
			
			buildings.update(spec, op)
			
			console.log({ duration: spec.duration })
			
			tasks.setTask({
				user: summary.user,
				time: spec.duration,
				visible: true,
				start: spec.start,
				params,
				selector: summary.village + ":research:" + slot + ":" +technology
			}, op)
		})
	},
	
	// POST https://host/villages/upgradeTech
	upgradeOccupation(summary, {occupation}, transaction){
		console.log("Upgrading occupation", occupation)
		summary.locks.resources = true
		
		dao.load(summary, transaction)
		buildings.lock({village: summary.village, slot: 12}, transaction)
		dao.lockOccupation(summary.village, occupation, transaction)
		
		transaction.addAction(() => {
			this.adjustModel(summary)
		})
		
		let spec = { item: occupation + "Tech" }
		
		transaction.addAction(() => {
			spec.multiplier = Math.pow(core.price.coeficient, summary.occupations[occupation].level)
			
			let enough = this.buyItem(summary, spec)
			
			if ( ! enough ) transaction.abort(village.err.notEnoughResources)
		})
		
		village.updateResources(summary, undefined, transaction)
		village.setUpdateEvent('village', summary.user, summary.village, transaction)
		
		transaction.addAction((curOp)=>{
			spec.duration = core.costs[spec.item].time * spec.multiplier
			spec.start = null
			
			if ( ! summary.buildings[12].free ){
				spec.start = summary.buildings[12].free_after
			}
			
			let params = {
				village: summary.village,
				user: summary.user,
				occupation,
				desc: 'Upgrading ' + occupation + " technology",
				job: '_upgradeOccupation',
				module: 'village',
				slot: 12
			}
			
			buildings.update(spec, curOp)
			tasks.setTask({ 
				user: summary.user,
				time: spec.duration,
				visible: true,
				start: spec.start,
				params,
				selector: summary.village + ":upgradeOccupation:" + occupation
			}, curOp)
		})
	},
	
	// PUT https://host/villages/moveWorker
	moveWorker(summary, {occupation, workers}, transaction){
		console.log("moveWorker", {occupation, workers})
		
		if ( occupation === 'unoccupied' || occupation === 'builders' ){
			mwCallback({ error: "Can't assign worker to " + occupation})
		}
		
		summary.locks.resources = true
		summary.locks.occupations = true
		
		dao.load(summary, transaction)
		dao.lockOccupation(summary.village, 'unoccupied', transaction)
		dao.lockOccupation(summary.village, occupation, transaction)
		
		transaction.addAction(() => {
			this.adjustModel(summary)
		})
		
		let delta = 0
		
		village.updateResources(summary, occupation, transaction)
		village.setUpdateEvent('village', summary.user, summary.village, transaction)

		transaction.addAction((curOp)=>{
			let o = summary.occupations[occupation]
			
			console.log( {workers : o.workers })
			
			delta = workers - o.workers

			if ( delta < 0 || delta > 0 && delta <= summary.occupations.unoccupied.workers ){
				summary.occupations.unoccupied.workers -= delta
				o.workers = o.workers + delta;
			} else {
				transaction.abort(village.err.noUnoccupiedPeasant)
			}
			
			dao.moveWorker(summary.village, occupation, delta, curOp)
		})		
	},

	/* Task Handlers */
	build(params, transaction){
      console.log("Executing build")
      buildings.finishBuilding(params, transaction)
		addPoints(params.user, 1, transaction)
		dao.moveWorker(params.village, 'builders', -params.builders, transaction)
		village.setUpdateEvent('village', params.user, params.village, transaction)
   },
	
	enableTech({ technology, user}, transaction){
		dao.enableTech({user, technology}, transaction)
		addPoints(user, 2, transaction)
	},
	
	_upgradeOccupation(params, transaction){
		console.log("Executing upgradeTech")
		addPoints(params.user, 1, transaction)
		let summary = core.villageSummary(params)
		
		summary.locks.resources = true
		summary.locks.occupations = true
		
		dao.load(summary, transaction)
		
		transaction.addAction(() => {
			this.adjustModel(summary)
		})
		
		village.updateResources( summary, params.occupation, transaction )
      dao.upgradeOccupation( params, transaction )
		village.setUpdateEvent('village', summary.user, summary.village, transaction)
	},
	
	upgradeBuilding({ user, village: vId, slot }, transaction){
		console.log("Executing upgradeBuilding")
		
		const summary = core.villageSummary({user, village: vId})
		summary.ignore.troops = true
		summary.ignore.resources = true
		summary.ignore.occupations = true
		summary.locks.buildings = true
		
		village.load(summary, transaction)
		
		transaction.addAction( op => {
			const building = { user, village: vId, slot }
			let { builders, level, nextLevel } = summary.buildings[slot].detail
			level++
			
			const detail= { level, nextLevel }
			
			if ( nextLevel - level === 1 ){
				dao.moveWorker(vId, 'builders', -builders, op)
				detail.builders = 0
			}

			building.detail = detail
    	buildings.upgrade( building, op )
		})
		
		addPoints(user, 1, transaction)
		village.setUpdateEvent('village', user, vId, transaction)
	},

	spawnArmyUnit(params, transaction){
		console.log("Executing spawnArmyUnit")
		const { units, user, village, slot } = params
		const type = units[0].troop
		const time = core.costs[type].time
		
		this.spawnUnit({type, village, user}, transaction)
		
		units[0].count--
		
		if( units[0].count === 0 ){
			params.units = units.splice(1)
		}
		
		if ( params.units.length === 0 ) return
		
		tasks.setTask({
			user,
			time,
			visible: true,
			start: null,
			params,
			selector: village + ":spawnArmy:" + slot
		}, transaction)
	},
	
	spawnUnit({village: villageId, user, type}, transaction){
		console.log("Executing spawn")
		
		if ( type === "peasant"){
			dao.addWorker(villageId, transaction)
			addPoints(user, 1, transaction)
		} else {
			const fph = core.profiles[type].fph
			addPoints(user, Math.floor(fph), transaction)
			troops.trainTroop({ village: villageId, type }, transaction)
			this.updatePopulation(fph, villageId, transaction)
		}
		
		village.setUpdateEvent('troops', user, villageId, transaction)
	},
	
	/* Exposed functions */
	load( summary, transaction ){
		dao.load(summary, transaction)
	},
	
	collectTaxes(mission, updateTaxes, transaction){
		const payer = core.villageSummary({ user: mission.origin.owner, village: mission.origin.id })
		payer.locks.resources
	
		console.log({ payer })	
		dao.load(payer, transaction)
		
		transaction.addAction( op => {
			this.adjustModel( payer )
			this.collect(mission, payer, "taxes", op)
			updateTaxes( mission.taxInfo, payer.user, op )
		})
	},
	
	collect(collector, payer, title, transaction){
				
		let bagSize = 0

		for( name in collector.troops ) {
			bagSize += collector.troops[name].count * parseInt(core.profiles[name].capacity)
		}
		
		if ( bagSize === 0 )	return

		let rTotal = payer.resources.food.r0 + payer.resources.iron.r0
		rTotal += payer.resources.wood.r0 + payer.resources.stone.r0
		
		if ( rTotal === 0 ) return

		let ratio = {
			food: payer.resources.food.r0 / rTotal,
			wood: payer.resources.wood.r0 / rTotal,
			iron: payer.resources.iron.r0 / rTotal,
			stone: payer.resources.stone.r0 / rTotal
		}

		collector[title] = {
			food: Math.min(payer.resources.food.r0, ratio.food * bagSize),
			wood: Math.min(payer.resources.wood.r0, ratio.wood * bagSize),
			iron: Math.min(payer.resources.iron.r0, ratio.iron * bagSize),
			stone: Math.min(payer.resources.stone.r0, ratio.stone * bagSize)
		}
		
		if ( title === 'taxes' ){
			const { taxes, taxInfo } = collector
			taxes.food = Math.min(taxes.food, taxInfo.food)
			taxInfo.food -= taxes.food
			
			const totalMaterials = taxes.iron + taxes.stone + taxes.wood
			
			console.log({ totalMaterials, taxes })
			
			if ( totalMaterials > taxInfo.materials ){
				const factor = taxInfo.materials / totalMaterials
				
				for( let r in taxes ) taxes[r] *= factor
				
				taxInfo.materials = 0
			} else {
				taxInfo.materials -= totalMaterials
			}
		}

		payer.resources.food.r0 -= collector[title].food
		payer.resources.iron.r0 -= collector[title].iron
		payer.resources.stone.r0 -= collector[title].stone
		payer.resources.wood.r0 -= collector[title].wood

		village.updateResources( payer, undefined, transaction )
		village.setUpdateEvent('resources', payer.user, payer.village, transaction)
	},
	
	updatePopulation(count, village, transaction){
		dao.updatePopulation(count, village, transaction)
	},
	
	updateResources( summary, resource, transaction ){
		dao.updateResources(summary, resource, transaction)
	},
	
	setUpdateEvent(element, user, villageId, transaction){
		const event = new Event(element + '-update:' + villageId)
		event.owner = user
		event.lifespan = 10
		event.details = { village: villageId }
		events.setEvent(event, transaction)
	},
	
	setTechEvent(element, user, technology, transaction){
		const event = new Event('new-technology')
		event.owner = user
		event.lifespan = 10
		event.details = { technology }
		console.log("Set Event", event)
		events.setEvent(event, transaction)
	},
	
	newVillageId(container, transaction){
		dao.newVillageId(container, transaction)
	},
	
	initVillage( specifications, transaction ){
		const { 
			user,
			village: { 
				name,
				id,
				position: {x, y},
				population,
				toBuild,
				infantryCount,
				workersPerOccupation,
			},
		} = specifications
		
		dao.newVillage(id, user, name, x, y, population, transaction)

		core.resources.forEach(
			r => dao.insertResource(id, r, transaction)
		)
		
		core.troops.forEach( t => {
			const count = t === "infantry" ? infantryCount : 0
			dao.insertTroop(id, t, count, transaction)
		})
		
		core.occupations.forEach( o => {
			const workers = 
				(o === "builders" || o === "unoccupied") ? 0
																	  : workersPerOccupation

			dao.insertOccupation(id, o, workers, transaction)
		})
	
		toBuild.forEach( b =>
			buildings.startBuilding({ 
				village: id,
				slot: b.slot,
				building: b.name,
				detail: { level: 0, nextLevel: 1 }
			}, transaction)
		)

		const subscription = `resources-update:`+id
		events.subscribe(user, subscription, transaction)
	},
	
	deleteVillage( userInfo, transaction ){
		const users = getUserLogic()
		userInfo.villages = []
		userInfo.village = userInfo.id 
		
		users.userVillages(userInfo, transaction)
		dao.deleteTroops(userInfo.id, transaction)
		dao.deleteResources(userInfo.id, transaction)
		dao.deleteBuildings(userInfo.id, transaction)
		dao.deleteOccupations(userInfo.id, transaction)
		dao.deleteVillage(userInfo.id, transaction)
		map.remove('village', userInfo.id, transaction)

		transaction.addAction( op => {
			
			console.log({ userInfo })
			
			if ( userInfo.villages.length === 1 ){
				console.log("Deleting user", userInfo.user)
				users.deleteUser(userInfo.user, op)
				userInfo.deleted = true
				
			}
		})
	},
	
	/*Raidable Village Update*/
	rvUpdate(params, transaction){
		params.start = params.start || new Date().getTime()
		const now = new Date().getTime()
		
		//Number of miliseconds in a week
		const week = 1000 * 60 * 60 * 24 * 7
		const pastWeeks = parseInt(( now - params.start ) / week)
		const storageLevel = 3 + pastWeeks
		
		dao.rvResources(pastWeeks * 1500, transaction)
		dao.rvOccupation(transaction)
		dao.rvBuildings(storageLevel, transaction)
		
		if ( pastWeeks > 5 ){
			const count = parseInt(pastWeeks * 3 / 5)
			dao.rvTroops('infantry', count, transaction)
			dao.rvTroops('pikeman', count, transaction)
		}
		
		tasks.setTask({
			user: null,
			time: week / 1000,
			params,
			selector: 'rvUpdate'
		}, transaction)
	}
}

module.exports = trx.transactionalModule(village)
