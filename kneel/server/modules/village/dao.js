const transactions = require('../lib/transactional')
const qm = require('./queries')
const core = require('../core')

const village = {
	name: "village.dao",
	load(summary, transaction){
		const args = [summary.village]
		qm.addQuery({
			resultHandler: result => { summary.population = result.rows[0].population },
			name: "population",
			args
		}, transaction)

		
		// Load Buildings
		if ( ! summary.ignore.buildings ){
			qm.addQuery({
				perRow: row => { summary.buildings[row.slot] = row },
				lock: summary.locks.buildings,
				name: "buildings",
				args
			}, transaction)
		}
		
		core.elements.map((name) => {
			if ( summary.ignore[name] ) return
			
			//"objects" => "object"
			const type = name.substring(0, name.length - 1)
			
			qm.addQuery({
				perRow: row => { summary[name][row[type]] = row },
				lock: summary.locks[name],
				name,
				args
			}, transaction)
		})
	},
	
	newVillageId(container, transaction){
		qm.addQuery({
			name: "newVillageId",
			args: [],
			resultHandler: result => { 
				container.village = { id: result.rows[0].id }
			}
		}, transaction)
	},
	
	newVillage(village, user, villageName, x,y, population, transaction){
		qm.addQuery({
			name: "insertVillage",
			args: [village, user, villageName, x, y, population]
		}, transaction)
	},
	
	insertResource(village, resource, transaction){
		qm.addQuery({
			name: "insertResource",
			args: [village, resource]
		}, transaction)
	},
	
	insertTroop(village, troop, count, transaction){
		qm.addQuery({
			name: "insertTroop",
			args: [village, troop, count, {}]
		}, transaction)
	},
	
	insertOccupation(village, occupation, workers, transaction){
		qm.addQuery({
			name: "insertOccupation",
			args: [village, occupation, workers, 0]
		}, transaction)
	},
	
	updatePopulation(count, villageName, transaction){
		qm.addQuery({
			name: "updatePopulation",
			args: [count, villageName]
		}, transaction)
	},
	
   addWorker(name, transaction){
		this.updatePopulation(1, name, transaction)
		
		qm.addQuery({
			name: "addWorker", 
			args: [name]
		}, transaction)
   },
   
	moveWorker(villageId, occupation, delta, transaction){
		transaction.addAction( op => {
			qm.addQuery({
				name: "moveWorker",
				args: [ delta, parseInt(villageId), occupation ]
			}, op)
			
			qm.addQuery({
				name: "moveWorker",
				args: [ -delta, parseInt(villageId), "unoccupied" ]
			}, op)
		})
	},

   updateResources( { village: name, resources }, resource, transaction ){
		transaction.addAction( op => {
			core.resources.map( r => {
				
				if( typeof resource === "string" && resource !== r ) return
				
				qm.addQuery({
					name: "updateResource",
					args: [ resources[r].r0, name, r]
				}, op)
			})
		})
   },

	upgradeOccupation( {village, occupation}, transaction ){
		qm.addQuery({
			name: "upgradeOccupation",
			args: [village, occupation]
		}, transaction)
	},
	
	lockOccupation(village, occupation, transaction){
		qm.addQuery({
			name: "lockOccupation",
			args: [village, occupation]
		}, transaction)
	},
	
	enableTech({ user, technology }, transaction){
		qm.addQuery({
			name: "enableTech",
			args: [technology, user]
		}, transaction)
	},
	
	deleteTroops(id, transaction){
		qm.addSimpleQuery('deleteTroops', [id], transaction)
	},
	deleteResources(id, transaction){
		qm.addSimpleQuery('deleteResources', [id], transaction)
	},
	deleteBuildings(id, transaction){
		qm.addSimpleQuery('deleteBuildings', [id], transaction)
	},
	deleteOccupations(id, transaction){
		qm.addSimpleQuery('deleteOccupations', [id], transaction)
	},
	deleteVillage(id, transaction){
		qm.addSimpleQuery('deleteVillage', [id], transaction)
	},
	
	rvResources(r0, transaction){
		qm.addSimpleQuery('RVResources', [r0], transaction)
	},
	
	rvOccupation(transaction){
		qm.addSimpleQuery('RVOccupations', [], transaction)
	},
	
	rvBuildings(level, transaction){
		qm.addSimpleQuery('RVBuildings', [{ level }], transaction)
	},
	
	rvTroops(troop, count, transaction){
		qm.addSimpleQuery('RVTroops', [count, troop], transaction)
	},
}

for( f in village){
	if( typeof village[f] !== "function" ) continue
	village[f] = village[f].bind(village)
}

module.exports = transactions.transactionalModule(village)
