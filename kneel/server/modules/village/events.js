const village = require('./index')
const core = require('../core')
const transactions = require('../lib/transactional')

const villageEvents = {
	handler(socket, event){
		const { user } = socket.handshake.session
		const transaction = transactions.transaction()
		const summary = core.villageSummary({ user, village: event.village })
		const element = event.key.match(/^(.+)-update\:\d+/)[1]
		
		if ( element === 'village'){
			village.getVillage(summary, undefined, transaction)

			transaction.addAction( op => {
				console.log("Emitting update-village")
				socket.emit('update-village', { summary, id: event.id })
			})
		} else {
			if ( element !== 'resources' ) throw "There is no " + element + "Handler"
			
			summary.ignore = { 
				occupations: true,
				buildings: true,
				troops: true,
				adjustment: true 
			}

			village.getVillage(summary, undefined, transaction)

			transaction.addAction( op => {
				console.log("Emitting update-resources")
				socket.emit('update-resources', { 
					resources : summary.resources,
					id: event.id
				})
			})		
		}
				
		transaction.finally = status => {
			if ( status !== 0 )	console.log("\n\nError:" + status)
		}

		transaction.run()
	}
}

module.exports = villageEvents