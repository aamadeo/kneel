const core = require('../core')

const validNumber = number => !isNaN(parseFloat(number)) && isFinite(number)

const validTroop = troop => {
	if ( ! core.profiles[troop] ){
		throw {
			msg: "Troop unknown: " + troop,
			code: validator.err.invalidTroopName
		}
	}
}

const validBuilding = building => {
	if ( ! core.buildingsNames.find( b => b === building ) ){
		throw {
			msg: "Building unknown: " + building,
			code: validator.err.invalidBuilding
		}
	}	
}

const validOccupation = (occupation) => {
	if ( occupation == 'unoccupied' || !core.occupations.find( o => o === occupation ) ){
		throw {
			msg: "Invalid occupation: " + occupation,
			code: validator.err.invalidOccupation
		}
	}
}

const validResource = (resource, title) => {
	if ( ! core.resources.find( r => r === resource ) ){
		throw {
			msg: `Invalid ${title}: ${resource}`, 
			code: validator.err.invalidResource
		}
	}
}

const validTechnology = technology => {
	if ( ! core.technologies[technology] ){
		throw {
			msg: `Invalid ${title}: ${resource}`, 
			code: validator.err.invalidResource
		}
	}	
}

const positiveNumber = (number, title, code) => {
	if ( ! validNumber(number) || number < 1 ){
		throw { msg: `Invalid ${title}: ${number}`, code }
	}
}

const notNegativeNumber = (number, title, code) => {
	if ( ! validNumber(number) || number < 0 ){
		throw { msg: `Invalid ${title}: ${number}`, code }
	}	
}

const validSlot = slot => {
	if ( ! validNumber(slot) || slot < 0 || slot > 16 ){
		throw {
			code: validator.err.invalidSlot,
			msg: "Invalid slot"
		}
	}
}

const validator = {
	err:{
		invalidTroopName: 	-1,
		invalidTroopCount: 	-2,
		noTroop: 				-3,
		invalidSlot: 			-4,
		invalidBuilding: 		-5,
		invalidBuildersCount:-6,
		invalidOccupation:	-7,
		invalidSellSpec:		-8,
		invalidSellCount:		-9,
		invalidWorkerCount:	-10
	},
	
	getVillage(){  },
	getVillageTroops(){ },
	spawnArmy: body => {
		let validRequest = false
		
		for( let troop in body ){
			validTroop(troop)
			positiveNumber(body[troop], "Troop Count", validator.err.invalidTroopCount )
			
			console.log( troop )
			
			validRequest = true
		}
		
		if ( ! validRequest ) throw {
			code: noTroop,
			msg: "No troop selected to train"
		}
	},
	
	spawn({slot, name}){
		if ( parseInt(slot) === 12 && name === 'peasant') return
		
		validTroop(name)
		validSlot(slot)
	},
	
	newBuilding({ name: building, slot, builders }){
		validSlot(slot)
		validBuilding(building)
		positiveNumber(builders, "Builders", validator.err.invalidBuildersCount)
	},
	
	upgrade({ slot, builders }){
		validSlot(slot)
		positiveNumber(builders, "Builders", validator.err.invalidBuildersCount)
	},
	
	upgradeOccupation({ occupation }){
		validOccupation(occupation)
	},
	
	sell({ buy, sell }){
		
		if ( typeof sell !== 'object' ){
			throw {
				msg: "Invalid sell specification: " + sell,
				code: validator.err.invalidSellSpec
			}			
		}
		
		validResource(buy, "resource to buy")
		validResource(sell.resource, "resource to sell")
		positiveNumber(sell.count, "sell count", validator.err.invalidSellCount)
	},
	
	moveWorker({ occupation, workers }){
		validOccupation(occupation)
		notNegativeNumber(workers, "workers", validator.err.invalidWorkerCount)
	},
	
	research({ slot, technology }){
		validSlot(slot)
		validTechnology(technology)
	}
}

module.exports = validator