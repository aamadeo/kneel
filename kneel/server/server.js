const express = require('express')
const path = require('path')
const busboy = require('connect-busboy')
const compression = require('compression')
const favicon = require('serve-favicon')
const logger = require('morgan')
const expressSession = require('express-session')
const sharedSession = require('express-socket.io-session')
const bodyParser = require('body-parser')
const events = require('./modules/events')
const emmiter = require('./modules/events/emitter')

const routers = {
	villages: require('./modules/village/controller'),
	cells: require('./modules/map/controller'),
	missions: require('./modules/mission/controller'),
	users: require('./modules/users/controller'),
	diplomacy: require('./modules/diplomacy/controller'),
	tasks: require('./modules/tasks/controller')
}

const eventsMdw = {
	map: require('./modules/map/events'),
	village: require('./modules/village/events'),
	technology: require('./modules/technology/events'),
	diplomacy: require('./modules/diplomacy/events'),
	mission: require('./modules/mission/events'),
	task: require('./modules/tasks/events')
}

const getHandler = eventKey => {
	if ( eventKey.match('map-section') ) return eventsMdw.map.handler
	if ( eventKey.match(/(resources|village)-update/) ) return eventsMdw.village.handler
	if ( eventKey.match(/new-technology/) ) return eventsMdw.technology.handler
	if ( eventKey.match(/allegiance-(request|update|accepted|denied)/) ) return eventsMdw.diplomacy.handler
	if ( eventKey.match(/(new|update)-report/) ) return eventsMdw.mission.handler
	if ( eventKey.match(/(new|update|end)-mission/) ) return eventsMdw.mission.handler
	if ( eventKey.match(/task-(start|end)/) ) return eventsMdw.task.handler
}

const getSubscriptionValidator = eventKey => {
	if ( eventKey.match('map-section') ) return eventsMdw.map.subscriptionValidator
}

const app = express()
const fs = require('fs')
const http = require('http')
const PORT = process.env.PORT || 8080 

const server = http.createServer(app)


const io = require('socket.io')(server, { origins: "*s1.aamadeo.me:*" })
server.listen(PORT, function () {
	console.log("Listening on " + PORT)
})

app.use(compression())
app.use(function (req, res, next) {
	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', 'http://www.dev.localhost:3000')
	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	//res.setHeader('Access-Control-Allow-Credentials', true)
	// Pass to next layer of middleware
	next()
})

app.use(busboy())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: false
}))

let sessionConfig = {
	resave: false
	, saveUninitialized: true
	, secret: 'AManHasNoSecrets'
	, cookie: {
		path: '/'
		, domain: 'aamadeo.me'
		, maxAge: 1000 * 60 * 60 * 24, // 24 hours',
		secure: false
		, httpOnly: false
	}
}

if (app.get('env') === 'production') {
	app.set('trust proxy', 1) // trust first proxy
}

const session = expressSession(sessionConfig)
app.use(session)
io.use(sharedSession(session))

app.use(express.static(path.join(__dirname, 'public')));

app.use('/v2/villages', routers.villages)
app.use('/v2/cells', routers.cells)
app.use('/v2/missions', routers.missions)
app.use('/v2/users', routers.users)
app.use('/v2/diplomacy', routers.diplomacy)
app.use('/v2/tasks', routers.tasks)
app.use('/index.html', (req, res) => {
	res.redirect('/public/index.html')
	res.end()
})

/* WebSocket */
io.on('connection', socket => { 
	console.log('Vamos los pibes')
	emmiter(socket, getHandler, 1000)
	events.subscriptionsHandler( socket, getSubscriptionValidator )
	events.eventsHandler( socket )
})

